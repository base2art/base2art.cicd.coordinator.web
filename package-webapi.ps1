param ([string]$toolsPath)

if ([System.String]::IsNullOrWhitespace($toolsPath))
{
  $base = Resolve-Path "~"
  $toolsPath = "$base/.nuget/packages/base2art.webapirunner.server.runners.commandlineinterface/0.1.3.7/tools"
}

. "$($toolsPath)/Deployment.ps1"

$deploymentProcessor = New-Webapi-Deployment

$deploymentProcessor.bin('src/Base2art.CiCd.Builder.WebClient/bin/Release/netstandard2.0/')
$deploymentProcessor.bin('src/Base2art.CiCd.Publisher.WebClient/bin/Release/netstandard2.0/')
$deploymentProcessor.bin('src/Base2art.CiCd.SourceControl.WebClient/bin/Release/netstandard2.0/')
$deploymentProcessor.bin('src/Base2art.CiCd.Servers.WebClient/bin/Release/netstandard2.0/')
$deploymentProcessor.bin('src/Base2art.CiCd.Coordinator.ServerPlugins.Database/bin/Release/netstandard2.0/')
$deploymentProcessor.bin('src/Base2art.CiCd.Coordinator.Web/bin/Release/netstandard2.0/')
$deploymentProcessor.bin('src/Base2art.CiCd.Coordinator.Web.UI/bin/Release/netstandard2.0/')
$deploymentProcessor.bin('src/Base2art.Web.Razor.Forms/bin/Release/netstandard2.0/')

# PAckaaging Example
$deploymentProcessor.package("Base2art.Standard.DataStorage.Provider.SQLite", "1.0.0.1")
$deploymentProcessor.package("Base2art.Standard.WebClient.NetHttp", "0.0.1")
$deploymentProcessor.package("Base2art.Web.App.ObjectQualityManagement", "0.0.3")
$deploymentProcessor.package("Base2art.Web.App.MessageQueue", "1.0.0")

$deploymentProcessor.package("SQLitePCLRaw.bundle_green", "2.0.5")
$deploymentProcessor.package("SQLitePCLRaw.lib.e_sqlite3", "2.0.5")
$deploymentProcessor.package("SQLitePCLRaw.provider.e_sqlite3", "2.0.5")
$deploymentProcessor.package("SQLitePCLRaw.bundle_e_sqlite3", "2.0.5")
$deploymentProcessor.package("SQLitePCLRaw.core", "2.0.5")

$deploymentProcessor.package("Base2art.Web.App.Principals.Auth", "1.0.3")
$deploymentProcessor.package("Base2art.Web.App.Principals.GoogleAuth", "1.0.3")

$deploymentProcessor.package("Base2art.Web.App.MessageQueue", "1.0.0")
$deploymentProcessor.package("Base2art.Standard.MessageQueue.Management", "0.0.0.4")




# config example
#$deploymentProcessor.config("input-file.yaml", "environment", "destinationName.yaml")
$deploymentProcessor.config("config/conf.yaml", "Production", "configuration.yaml")
$deploymentProcessor.config("config/urls-ui.yaml", "Production", "urls-ui.yaml")
$deploymentProcessor.config("config/urls.yaml", "Production", "urls.yaml")

$deploymentProcessor.config("config/tasks.yaml", "Production", "tasks.yaml")

$deploymentProcessor.config("config/injection.yaml", "Production", "injection.yaml")
$deploymentProcessor.config("config/injection-artifacts.yaml", "Production", "injection-artifacts.yaml")
$deploymentProcessor.config("config/injection-builds.yaml", "Production", "injection-builds.yaml")
$deploymentProcessor.config("config/injection-publishing.yaml", "Production", "injection-publishing.yaml")
$deploymentProcessor.config("config/injection-source-control.yaml", "Production", "injection-source-control.yaml")
$deploymentProcessor.config("config/injection-versioning.yaml", "Production", "injection-versioning.yaml")
$deploymentProcessor.config("config/injection-ui.yaml", "Production", "injection-ui.yaml")

$deploymentProcessor.framework("netcoreapp3.1")


Write-Host $deploymentProcessor.deploy()


Exit 0

