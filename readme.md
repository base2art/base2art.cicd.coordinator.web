
#Main Content

#Dashboard
    * Failing Active Project Branches
    * Failing Active Servers
    * Recent Builds 
    * Project treeview

## Servers
    * Active
    * Inactive
    
    * Healthy
    * Unhealthy


## Builds
    * State
    * Version
    * When
    * Error Logs
    * OutputLogs
    * System Logs
    * Artifacts
    * Source Control Info


## Security Keys
    * Active
    * Inactive

## Projects

    * Grouping ( path Style )
    * Project Parts
    ** Source Control
    ** Build Instructions
    ** Version Style
    * Ability To See All Branches
    * Ability to Build a branch
    * Auto Building Branches
    * ability to see changes by Branches
    
    * Active
    * Inactive
    
    * ability to add a Project
    
    
    
### BUILD Script

Remove-Item -Recurse -Force bccw-2022;
git clone git@bitbucket.org:base2art/base2art.cicd.coordinator.web.git bccw-2022;
cd ./bccw-2022/
Set-Content -Path ".branch-name" -Value "master";
Set-Content -Path ".version" -Value "0.0.0.42";
pwsh -File ./bob.ps1;   
sh ./rename-file.sh; 



## TODO

- Dates / times should have hover to the original date.
- Auto Refresh?
