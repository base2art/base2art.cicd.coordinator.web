namespace Base2art.CiCd.SourceControl.WebClient
{
    using Base2art.WebClient;
    using Coordinator.Repositories;
    using Servers.WebClient;

    public class CheckoutPhaseServiceFactory : ExtensionServiceFactory<ICheckoutPhaseService>, ICheckoutPhaseServiceFactory
    {
        private readonly IWebClient client;

        public CheckoutPhaseServiceFactory(IServerReadRepository serverService, IWebClient client) : base(serverService)
            => this.client = client;

        protected override ICheckoutPhaseService Create(string serverUrl) => new SourceControlServiceClient(serverUrl, this.client);
    }
}

/*
 *
        private readonly IServerReadRepository serverService;
        public async Task<ICheckoutPhaseService> CreateClient(Guid projectType)
        {
            var baseUrl = await this.GetBaseUrlByType(projectType);
            return new SourceControlServiceClient(baseUrl);
        }

        public Task<ICheckoutPhaseService> CreateClient(string baseUrl) => Task.FromResult<ICheckoutPhaseService>(new SourceControlServiceClient(baseUrl));

        private async Task<string> GetBaseUrlByType(Guid projectSourceControlType)
        {
            var servers = await this.serverService.FindByFeature(projectSourceControlType);

            return servers?.OrderBy(x => Guid.NewGuid())
                          ?.FirstOrDefault(x => x.Enabled)
                          ?.Url
                          ?.ToString();
        }
 * 
 */