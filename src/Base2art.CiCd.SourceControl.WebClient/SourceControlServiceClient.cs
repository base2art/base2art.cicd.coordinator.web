﻿namespace Base2art.CiCd.SourceControl.WebClient
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Resources;
    using Base2art.WebClient;
    using Base2art.WebClient.Json;
    using Base2art.WebClient.ResponseHandlers.Json;
    using Servers;

    public class SourceControlServiceClient : ICheckoutPhaseService
    {
        private readonly string baseUrl;

        public SourceControlServiceClient(string baseUrl, IWebClient client)
        {
            this.baseUrl = baseUrl;
            this.client = client;
        }

        private readonly IWebClient client;

        public async Task<SupportedFeature[]> GetSupportedFeatures(ClaimsPrincipal principal)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("/api/v1/supported-features")
                      .ToString();

            List<SupportedFeature> returnValue = null;
            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Get)
                      .WithAuthorizationBearer(principal)
                      .WithResponseHandler()
                      .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<List<SupportedFeature>>(x => returnValue = x))
                      .Run();

            return returnValue?.ToArray() ?? new SupportedFeature[0];
        }

        public async Task<IsolatedBranchData[]> GetBranches(Dictionary<string, string> projectSourceControlData, ClaimsPrincipal principal)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("/api/v1/source/branches")
                      .ToString();

            var data = new List<IsolatedBranchData>();
            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Put)
                      .WithAuthorizationBearer(principal)
                      .WithJsonBody(projectSourceControlData)
                      .WithResponseHandler()
                      .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<List<IsolatedBranchData>>(x => data = x))
                      .Run();

            return data?.ToArray() ?? new IsolatedBranchData[0]; //.Select<IsolatedBranchData, IBranchData>(x => new BranchDataModel
            //                                              {
            //                                                  Name = x.Name,
            //                                                  Hash = x.Hash
            //                                              }).ToArray();
        }

        public async Task<IsolatedBranchLogData> GetLogs(RepositoryBranchData<Dictionary<string, string>> projectData, ClaimsPrincipal principal)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("/api/v1/source/logs")
                      .ToString();

            var body = new Dictionary<string, object>
                       {
                           ["repository"] = projectData.Repository,
                           ["branch"] = new IsolatedBranchData {Name = projectData.Branch.Name, Hash = projectData.Branch.Hash}
                       };

            var data = new IsolatedBranchLogData();
            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Put)
                      .WithAuthorizationBearer(principal)
                      .WithJsonBody(body)
                      .WithResponseHandler()
                      .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<IsolatedBranchLogData>(x => data = x))
                      .Run();

            return data;
//            return new BranchLogDataModel
//                   {
//                       Name = branchName,
//                       Hash = branchHash,
//                       CommitLogs = (data?.Logs ?? new IsolatedCheckoutLogData[0]).Select(y => new CommitLogDataModel
//                                                                                               {
//                                                                                                   Id = y.Id,
//                                                                                                   Author = y.Author,
//                                                                                                   Message = y.Message,
//                                                                                                   When = y.When,
//                                                                                                   FilesAdded = y.FilesAdded,
//                                                                                                   FilesRemoved = y.FilesRemoved,
//                                                                                                   FilesChanged = y.FilesChanged,
//                                                                                               })
//                                                                                  .Select<CommitLogDataModel, ICommitLogData>(y => y)
//                                                                                  .ToArray()
//                   };
        }

        public async Task<IsolatedCheckout> StartCheckout(Dictionary<string, string> projectSourceControlData, string branchOrTag,
                                                          ClaimsPrincipal principal)
//        public async Task<ICheckout> StartCheckout(Guid projectSourceControlType, object projectSourceControlData, string branchOrTag)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("/api/v1/checkout")
                      .ToString();

            var data = new IsolatedCheckout();
            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Put)
                      .WithAuthorizationBearer(principal)
                      .WithJsonBody(new CloneData
                                    {
                                        BranchOrTag = branchOrTag,
                                        Repository = projectSourceControlData
                                    })
                      .WithResponseHandler()
                      .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<IsolatedCheckout>(x => data = x))
                      .Run();

            return data;
//            return MapCheckoutModel(data);
        }

//
//        public async Task<ICheckout> GetCheckoutStatus(Guid projectSourceControlType, Guid checkoutId)
        public async Task<IsolatedCheckout> GetCheckoutStatus(Guid checkoutId, ClaimsPrincipal principal)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("/api/v1/checkout/{checkoutId}")
                      .WithUrlSegment("checkoutId", checkoutId.ToString("D"))
                      .ToString();

            var data = new IsolatedCheckout();
            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Get)
                      .WithAuthorizationBearer(principal)
                      .WithResponseHandler()
                      .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<IsolatedCheckout>(x => data = x))
                      .Run();
            return data;
//            return MapCheckoutModel(data);
        }

        public async Task<IsolatedSourceData> GetSourceWithMetaData(Guid checkoutId, ClaimsPrincipal principal)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("/api/v1/checkout/{checkoutId}/artifacts")
                      .WithUrlSegment("checkoutId", checkoutId.ToString("D"))
                      .ToString();

            IsolatedSourceData data = null;
            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Get)
                      .WithAuthorizationBearer(principal)
                      .WithResponseHandler()
                      .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<IsolatedSourceData>(x => data = x))
                      .Run();

            return data;
        }

        public async Task<byte[]> GetSource(Guid checkoutId, ClaimsPrincipal principal)
//        public async Task<byte[]> GetSource(Guid projectSourceControlType, Guid checkoutId)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("/api/v1/checkout/{checkoutId}/source")
                      .WithUrlSegment("checkoutId", checkoutId.ToString("D"))
                      .ToString();

            var data = new byte[0];
            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Get)
                      .WithAuthorizationBearer(principal)
                      .WithResponseHandler()
                      .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<byte[]>(x => data = x))
                      .Run();

            return data;
        }

        public async Task RemoveSource(Guid checkoutId, ClaimsPrincipal principal)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("/api/v1/checkout/{checkoutId}")
                      .WithUrlSegment("checkoutId", checkoutId.ToString("D"))
                      .ToString();

            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Delete)
                      .WithAuthorizationBearer(principal)
                      .WithResponseHandler()
                      .Run();
        }

//        public Task RemoveSource(Guid checkoutId, ClaimsPrincipal principal) => throw new NotImplementedException();
    }

//    private class CommitLogDataModel : ICommitLogData
//    {
//        public string Id { get; set; }
//        public DateTime When { get; set; }
//        public string Author { get; set; }
//        public string Message { get; set; }
//        public string[] FilesAdded { get; set; }
//        public string[] FilesRemoved { get; set; }
//        public string[] FilesChanged { get; set; }
//    }

//        private class CheckoutModel : ICheckout
//        {
//            public Guid Id { get; set; }
//
//            public CheckoutState State { get; set; }
//        }
//        private static CheckoutModel MapCheckoutModel(IsolatedCheckout data) => new CheckoutModel
//                                                                                {
//                                                                                    Id = data.Id,
//                                                                                    State = (CheckoutState) (int) data.State,
//                                                                                };

//        private class BranchDataModel : IBranchData
//        {
//            public string Name { get; set; }
//            public string Hash { get; set; }
//        }
//
//        private class BranchLogDataModel : IBranchLogData
//        {
//            public string Name { get; set; }
//            public string Hash { get; set; }
//            public ICommitLogData[] CommitLogs { get; set; }
//        }

//    public interface IBranchData
//    {
//    }
}

//                                                                         CommitLogs = x.Logs.Select(y => new CommitLogDataModel
//                                                                                                         {
//                                                                                                             Id = y.Id,
//                                                                                                             Author = y.Author,
//                                                                                                             Message = y.Message,
//                                                                                                             When = y.When,
//                                                                                                             FilesAdded = y.FilesAdded,
//                                                                                                             FilesRemoved = y.FilesRemoved,
//                                                                                                             FilesChanged = y.FilesChanged,
//                                                                                                         })
//                                                                                       .Select<CommitLogDataModel, ICommitLogData>(y => y)
//                                                                                       .ToArray()