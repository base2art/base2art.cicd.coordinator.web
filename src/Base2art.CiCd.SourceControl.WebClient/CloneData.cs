namespace Base2art.CiCd.SourceControl.WebClient
{
    public class CloneData
    {
        public object Repository { get; set; }
        public string BranchOrTag { get; set; }
    }
}