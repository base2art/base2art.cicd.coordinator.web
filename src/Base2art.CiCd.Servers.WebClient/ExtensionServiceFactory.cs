﻿namespace Base2art.CiCd.Servers.WebClient
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Coordinator.Models;
    using Coordinator.Repositories;

    public abstract class ExtensionServiceFactory<T> : IExtensionServiceFactory<T>
    {
        private readonly IServerReadRepository serverService;

        protected ExtensionServiceFactory(IServerReadRepository serverService) => this.serverService = serverService;

        public async Task<IExtensionServiceClient<T>> CreateClient(Guid projectType)
        {
            var (server, servers) = await this.GetBaseUrlByType(projectType);
            return this.CreateInternal(server, servers);
        }

        public Task<T> CreateClient(string baseUrl)
            => Task.FromResult(this.CreateInternal(baseUrl));

        private T CreateInternal(string serverUrl)
        {
            if (serverUrl == null)
            {
                return default;
            }

            return Create(serverUrl);
        }

        protected abstract T Create(string serverUrl);

        private IExtensionServiceClient<T> CreateInternal(IServerData server, IServerData[] alternates)
            => new ExtensionServiceClient(this.CreateInternal(server?.Url?.ToString()), alternates, server);

        private async Task<(IServerData, IServerData[])> GetBaseUrlByType(Guid projectSourceControlType)
        {
            var servers = await this.serverService.FindByFeature(projectSourceControlType);
            var items = servers.Select((x) => (x, Guid.NewGuid().ToString("N")))
                               .OrderBy(x => x.Item2)
                               .Select(x => x.Item1)
                               .Where(x => x.Enabled)
                               .Select(this.Convert)
                               .ToArray();

            return (items.FirstOrDefault(), items);
        }

        private IServerData Convert(IServer firstOrDefault)
        {
            if (firstOrDefault == null)
            {
                return null;
            }

            return new ServerData
                   {
                       Id = firstOrDefault.Id,
                       Name = firstOrDefault.Name,
                       AuthenticationKeyId = firstOrDefault.AuthenticationKeyId,
                       Url = firstOrDefault.Url,
                   };
        }

        private class ServerData : IServerData
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public Uri Url { get; set; }
            public Guid? AuthenticationKeyId { get; set; }
        }

        public class ExtensionServiceClient : IExtensionServiceClient<T>
        {
            public ExtensionServiceClient(T item, IServerData[] servers, IServerData currentServer)
            {
                this.Service = item;
                this.CurrentServer = currentServer;
                this.AlternateServers = servers;
            }

            public IServerData[] AlternateServers { get; }
            public IServerData CurrentServer { get; }
            public T Service { get; }
        }
    }
}