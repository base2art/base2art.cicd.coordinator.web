var riotApp = (function (wndw, doc, stateManager) {

    const root = {};
    root.dependencyModel = null;
    root.workingState = {};
    root.workingState.isBusy = false;
    root.workingState.counter = 0;
    var root_workingState_handlers = [];
    root.workingState.setIsBusy = function (value) {
        
        var delta = (value ?  1 : -1);
        root.workingState.counter += delta;
        
        
        root.workingState.isBusy = root.workingState.counter > 0;
        for (let i = 0; i < root_workingState_handlers.length; i++) {
            root_workingState_handlers[i]();
        }
    };
    root.workingState.registerHandler = function (value) {
        root_workingState_handlers.push(value);
    };


    var text = {};
    text.stringComparer = {};
    text.stringComparer.ordinal = function (action, invert) {

        var i = 1;
        if (invert === true) {
            i = -1;
        }

        return function (x, y) {
            var a = action;

            var ax = a(x) || "";
            var ay = a(y) || "";

            if (ax < ay) {
                return -1 * i;
            }

            if (ax > ay) {
                return 1 * i;
            }

            return 0 * i;
        }
    };

    text.stringComparer.ordinalIgnoreCase = function (action, invert) {
        return text.stringComparer.ordinal(x => (action(x) || "").toUpperCase(), invert);
    };

    var net = {};
    net.getParameterByName = function (name, url) {
        if (!url) {
            url = wndw.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    };
    net.getCookie = function (cname) {
        var name = cname + "=";
        var ca = doc.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    };
    net.primaryDomain = function () {
        return wndw.location.host.split('.').slice(-2).join(".");
    };


    const io = {};
    io.clone = function jsonCopy(src) {
        return JSON.parse(JSON.stringify(src));
    };

    const security = {};
    var __knownRoles = [];
    security.knownRoles = function () {
        return io.clone(__knownRoles);
    };
    security.addRole = function (name, requireAuthenticated, requireUser, requireGroup) {
        return __knownRoles.push({
            name: name,
            requireAuthenticated: requireAuthenticated,
            requireUser: requireUser,
            requireGroup: requireGroup
        });
    };

    const user = {};
    user.signIn = function () {

        var path = wndw.location.pathname;
        path = path.substring(1);
        path += "?" + wndw.location.search;

        var appName = "builder";


        var localSigninServer = wndw["signinServer"];
        if (localSigninServer == null) {
            localSigninServer = 'https://sso.' + net.primaryDomain() + '/';
        }

        var destLocation = localSigninServer + '?app.name=' + appName + '&returnUrl=' + encodeURIComponent((wndw.location.pathname + wndw.location.search).substr(1));
        wndw.location = destLocation;
    };

    user.isAuthenticated = function () {

        if (wndw.location.host === "localhost" || wndw.location.host.startsWith("localhost:")) {

            var dsi = net.getParameterByName("d.signed-in");
            if (dsi === false || dsi === "false" || dsi === "0" || dsi === 0) {
                return false;
            }

            return true;
        }

        var auth_jwt = net.getCookie("auth_jwt");
        var auth_info = net.getCookie("auth_info");
        if (auth_info && auth_jwt) {

            var token = JSON.parse(decodeURIComponent(auth_info));
            var exp = new Date(token.exp * 1000);
            if (exp < new Date()) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    };

    user.isAuthorized = function (role) {

        var roles = security.knownRoles().filter(x => x.name === role);
        if (roles.length === 0) {
            return false;
        }

        var isAuthenticated = user.isAuthenticated();

        for (let i = 0; i < roles.length; i++) {
            var targetRole = roles[i];
            if (targetRole.requireAuthenticated && !isAuthenticated) {
                return false;
            }
        }

        return true;
    };

    var navigation = {};
    navigation.router = {};

    navigation.addRoute = function (title, tagName, requiredRole, url, parameters, dependencyCall) {
        navigation.router[url] = {
            url: url,
            tag: tagName,
            title: title,
            requiredRole: requiredRole,
            parameters: parameters,
            dependencyCall: dependencyCall
        }
    };

    navigation.currentPage = {
        title: "Unknown",
        url: "/",
        parameters: {}
    };

    navigation.routes = function () {
        var keys = Object.keys(navigation.router);
        return keys.map(key => navigation.router[key]);
    };

    navigation.navigate = function (url, params) {
        var page = navigation.router[url];

        if (page == null) {
            console.log("cannot navigate to ", url, params);
            return "";
        }


        var navigationParameters = Object.keys(params);

        if (!page.parameters.every(val => navigationParameters.includes(val))) {
            console.log("cannot navigate to ", url, " because missing parameters for page", params);
            return "";
        }

        var qsUrl = url;
        for (var i = 0; i < page.parameters.length; i++) {
            var pageParameter = page.parameters[i];

            qsUrl = qsUrl.replace("{" + pageParameter + "}", params[pageParameter]);
        }

        navigation.currentPage.url = url;
        navigation.currentPage.title = page.title;
        navigation.currentPage.parameters = params;
        
        
        var role = page.requiredRole;
        if (!user.isAuthorized(role))
        {
            console.log("cannot navigate to ", url, " because you are not authorized", params);
            navigation.routeToUrl("/");
            return "";
        }
        
        if (page.dependencyCall == null) {
            console.log("navigating", qsUrl, params);
            history.pushState(null, null, "#" + qsUrl);
            stateManager.update();
        }
        else {
            page.dependencyCall(params, (obj) => {
                root.dependencyModel = obj;
                console.log("navigating", qsUrl, params);
                history.pushState(null, null, "#" + qsUrl);
                stateManager.update();

            })
        }
    };


    navigation.routeToUrl = function (url) {
        if (url != null) {
            var normalizeParts = function (item) {
                while (item.includes("//")) {
                    item = item.replace("//", "/");
                }

                return item.split('/').filter(x => x != null && x.length > 0
                )
                    .map(function (x) {
                        return {
                            mask: ((x.startsWith("{") && x.endsWith("}")) ? "----------------------" : x),
                            value: x
                        };
                    });
            };

            function isMatch(hashParts, urlParts) {
                if (urlParts.length !== hashParts.length) {
                    return [false, {}];
                }

                var outParams = {};

                for (var up = 0; up < urlParts.length; up++) {
                    var urlPart = urlParts[up];
                    var hashPart = hashParts[up];

                    if (urlPart.value !== hashPart.value && urlPart.value === urlPart.mask) {
                        return [false, {}];
                    }

                    if (urlPart.value !== urlPart.mask) {
                        var cleanPart = urlPart.value.substr(1, urlPart.value.length - 2);
                        outParams[cleanPart] = hashPart.value;
                    }
                }


                return [true, outParams];
            }

            function getMatch(hasParts, routes) {

                for (var u = 0; u < routes.length; u++) {
                    var route = routes[u];

                    var urlParts = normalizeParts(route.url);

                    var match = isMatch(hashParts, urlParts);
                    if (match[0]) {
                        return [route, match[1]];
                    }
                }
            }

            var hashParts = normalizeParts(url);

            var routes = navigation.routes();
            var route = getMatch(hashParts, routes);

            if (route != null) {
                navigation.navigate(route[0].url, route[1]);
                return;
            }
        }

        var keys = Object.keys(navigation.router);
        var route = keys.map(key => navigation.router[key]).filter(x => x.parameters.length === 0)[0];
        navigation.navigate(route.url, {});
    };

    var actions = {};

    actions.addAction = function (name, action) {
        actions[name] = action || function () {
        };
    };

    root.text = text;
    root.net = net;
    root.security = security;
    root.user = user;
    // root.data = data;
    root.navigation = navigation;
    root.actions = actions;
    // root.commands = commands;

    return root;

})
(window, document, riot);

