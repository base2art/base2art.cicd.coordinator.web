var apiClientFactory = (function (jquery) {

    var options = {
        callbackPre: function () {
        },
        callbackPost: function () {
        }
    };

    var __ajaxNoBody = function (url, method, callback) {

        options.callbackPre();
        jquery.ajax({
            url: url,
            context: document.body,
            dataType: "json",
            contentType: "application/json",
            method: method
        }).done(function (result) {
            options.callbackPost();
            callback(result);
        }).fail(function (x) {

            options.callbackPost();
            console.log(x);
        });
    };


    var __ajaxWithBody = function (url, method, body, callback) {
        options.callbackPre();
        jquery.ajax({
            url: url,
            context: document.body,
            dataType: "json",
            contentType: "application/json",
            method: method,
            data: JSON.stringify(body)
        }).done(function (result) {
            options.callbackPost();
            callback(result);
        }).fail(function (x) {
            options.callbackPost();
            console.log(x);
        });
    };

    var __ajaxGet = function (url, callback) {
        __ajaxNoBody(url, "GET", callback);
    };

    var __ajaxDelete = function (url, callback) {
        __ajaxNoBody(url, "GET", callback);
    };

    var __getProjects = function (callback) {
        __ajaxGet("/api/v1/projects", callback);
    };
    var __getProjectRequisitions = function (callback) {
        __ajaxGet("/api/v1/projects/requisitions", callback);
    };

    var __deleteProject = function (projectId, callback) {
        __ajaxDelete("/api/v1/projects/" + projectId, "DELETE", callback);
    };

    var __getBranchesByProjectId = function (id, callback) {
        __ajaxGet("/api/v1/projects/" + id + "/branches?allowCachedItems=true", callback);
    };

    var __getBuildsByProjectBranch = function (projectId, branchName, pageIndex, pageSize, callback) {

        var url = "/api/v1/jobs?PageIndex={pageIndex}&PageSize={pageSize}&sort=DateCreatedDescending";
        if (projectId) {
            url += "&ProjectId={projectId}";
        }

        if (branchName) {
            url += "&BranchName={branchName}";
        }

        url = url.replace("{projectId}", encodeURIComponent(projectId))
            .replace("{branchName}", encodeURIComponent(branchName))
            .replace("{pageSize}", encodeURIComponent(pageSize.toString()))
            .replace("{pageIndex}", encodeURIComponent(pageIndex.toString()));

        __ajaxGet(url, callback);
    };

    var __triggerBuild = function (projectId, branchName, callback) {
        var body = {
            "projectId": projectId,
            "branchName": branchName
        };

        __ajaxWithBody("/api/v1/jobs", "PUT", body, callback);
    };

    var __triggerBranchRefresh = function (projectId, callback) {

        var body = {"projectId": projectId};

        __ajaxWithBody("/tasks/refresh-branches", "POST", body, callback);
    };


    var __getAllSourceControlFeatures = function (callback) {
        __ajaxGet("/api/v1/servers/SourceControl/features", callback);
    };

    var __getAllCompilationFeatures = function (callback) {
        __ajaxGet("/api/v1/servers/Compilation/features", callback);
    };

    var __getServers = function (callback) {
        __ajaxGet("/api/v1/servers", callback);
    };

    var __addServer = function (name, serverUrl, enabled, serverType, callback) {

        var url = "/api/v1/servers/{serverType}";
        url = url.replace("{serverType}", encodeURIComponent(serverType));

        var body = {
            "name": name,
            "url": serverUrl,
            "enabled": enabled
        };
        __ajaxWithBody(url, "PUT", body, callback);
    };
    
    var __refreshServers = function (callback) {

        var url = "/tasks/refresh-server-features";

        var body = {};
        __ajaxWithBody(url, "POST", body, callback);
    };

    var __addSourceControl = function (projectId, sourceControlType, data, exe, exeArgs, callback) {

        var url = "/api/v1/projects/{projectId}/sourceControl";
        url = url.replace("{projectId}", encodeURIComponent(projectId));

        var body = {
            "sourceControlType": sourceControlType,
            "sourceControlData": JSON.parse(data),
            "buildInstructions": [
                {
                    "executableFile": exe,
                    "executableArguments": exeArgs || []
                }
            ]
        };
        __ajaxWithBody(url, "PUT", body, callback);
    };

    var __removeSourceControl = function (projectId, sourceControlId, callback) {

        var url = "/api/v1/projects/{projectId}/sourceControl/{sourceControlId}";
        url = url.replace("{projectId}", encodeURIComponent(projectId))
            .replace("{sourceControlId}", encodeURIComponent(sourceControlId));

        __ajaxNoBody(url, "DELETE", callback);
    };

    var __addCompilationFeature = function (projectId, featureId, callback) {

        var url = "/api/v1/projects/{projectId}/features";
        url = url.replace("{projectId}", encodeURIComponent(projectId));

        var body = {
            "id": featureId
        };
        __ajaxWithBody(url, "PUT", body, callback);
    };

    var __removeCompilationFeature = function (projectId, featureId, callback) {

        var url = "/api/v1/projects/{projectId}/features/{featureId}";
        url = url.replace("{projectId}", encodeURIComponent(projectId))
            .replace("{featureId}", encodeURIComponent(featureId));

        __ajaxNoBody(url, "DELETE", callback);
    };

    var __addProject = function (data, callback) {

        __ajaxWithBody("/api/v1/projects", "PUT", data, callback);
    };


    var __getBuild = function (buildId, callback) {
        __ajaxGet("/api/v1/jobs/" + buildId, callback);
    };


    var methods = {
        projectRequisitions: __getProjectRequisitions,
        projects: __getProjects,
        addSourceControl: __addSourceControl,
        removeSourceControl: __removeSourceControl,
        getBranchesByProjectId: __getBranchesByProjectId,
        getBuildsByProjectBranch: __getBuildsByProjectBranch,
        triggerBuild: __triggerBuild,
        triggerBranchRefresh: __triggerBranchRefresh,
        getAllSourceControlFeatures: __getAllSourceControlFeatures,
        getAllCompilationFeatures: __getAllCompilationFeatures,
        getServers: __getServers,
        addServer: __addServer,
        refreshServers: __refreshServers,
        addProject: __addProject,
        addCompilationFeature: __addCompilationFeature,
        removeCompilationFeature: __removeCompilationFeature,
        getBuild: __getBuild,
        deleteProject: __deleteProject
    };

    return {
        apiClient: function (callbackPre, callbackPost) {
            options.callbackPre = callbackPre;
            options.callbackPost = callbackPost;
            return methods;
        }
    };
})($);
