$(function() {

    $('.include-container').each(function(i, x) {

        var inc = $(x).attr("data-include");
        $.get(inc).done(function(z) {
            $(x).html(z)
        });

    });

});