function showHideFormChange(el, config, selector, $form) {

    // console.log(config.InputName);
    let selectedTarget = $(selector, $form);
    if ((selectedTarget.attr("type") || "").toUpperCase() === "RADIO") {
        selectedTarget = $(selector + ":checked", $form);
    }
    var val = selectedTarget.val();

    for (var i = 0; i < config.ShowHides.length; i++) {
        var showHide = config.ShowHides[i];


        var target = $('*[name="' + showHide.Control + '"]', $form).closest(".form-group");
        if (showHide.Show) {
            if (val == showHide.Value) {

                target.show();
            } else {
                target.hide();
            }
        } else {
            if (val == showHide.Value) {
                target.hide();
            } else {
                target.show();
            }
        }
    }

}

function showHideForm(el, config) {

    $form = $(el).closest("form");

    var selector = '*[name="' + config.InputName + '"]';

    $(selector, $form).each(function (i, x) {

        $(x).change(function (event) {
            showHideFormChange(el, config, selector, $form);
        });

        showHideFormChange(el, config, selector, $form);

    });
}

// var b2a_forms = {
//     xwwwfurlenc: function (srcjson, prefix) {
//         prefix = prefix || "";
//         if (typeof srcjson !== "object") if (typeof console !== "undefined") {
//             console.log("\"srcjson\" is not a JSON object");
//             return null;
//         }
//         u = encodeURIComponent;
//         var urljson = "";
//         var keys = Object.keys(srcjson);
//         for (var i = 0; i < keys.length; i++) {
//             urljson += (prefix + u(keys[i])) + "=" + u(srcjson[keys[i]]);
//             if (i < (keys.length - 1)) urljson += "&";
//         }
//         return urljson;
//     },
//
//     dexwwwfurlenc: function (urljson) {
//         var dstjson = {};
//         var ret;
//         var reg = /(?:^|&)(\w+)=(\w+)/g;
//         while ((ret = reg.exec(urljson)) !== null) {
//             dstjson[ret[1]] = ret[2];
//         }
//         return dstjson;
//     }
//
// };