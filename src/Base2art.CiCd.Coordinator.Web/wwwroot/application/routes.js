let ApplicationRoutes = (function () {

    //<div class="row">
    //                     {opts.model.length} Running Builds
    //                 </div>
    let messages = [];

    var configure = function (app) {
        app.navigation.panel("header", "application-header")
            .modelProvider((params, callback) => {
                callback(messages)
            })
            .add();

        app.navigation.panel("notifications", "debug")
            .modelProvider((params, callback) => {
                let url = "/api/v1/jobs?PageIndex={pageIndex}&PageSize={pageSize}&sort=DateCreatedDescending&State=Working";
                let pageSize = 10;
                let pageIndex = 0;
                url = url.replace("{pageSize}", encodeURIComponent(pageSize.toString()))
                    .replace("{pageIndex}", encodeURIComponent(pageIndex.toString()));

                app.webClient.rest("GET", url).do(function (body) {


                    for (let j = messages.length; j > 0; j--) {

                        var idx = j - 1;
                        if (messages[idx].id === "dae86806f9d347428ec43b95eba88a96") {

                            messages.splice(idx, 1);
                        }
                    }

                    if (body.length) {
                        messages.push({text: body.length + " Running Builds", id: "dae86806f9d347428ec43b95eba88a96"});

                        for (var j = 0; j < body.length; j++) {
                            messages.push({
                                text: " - " + body[j].projectName + " Running",
                                id: "dae86806f9d347428ec43b95eba88a96"
                            });
                        }

                    }
                    // callback(messages);
                });
            })
            .refresh(1000 * 10)
            .add();


        // app.navigation.panel("rightNav", "builds-panel")
        //     .titled("Recent Builds")
        //     .modelProvider((params, callback) => {
        //         let url = "/api/v1/jobs?PageIndex={pageIndex}&PageSize={pageSize}&sort=DateCreatedDescending";
        //         let pageSize = 5;
        //         let pageIndex = 0;
        //         url = url.replace("{pageSize}", encodeURIComponent(pageSize.toString()))
        //             .replace("{pageIndex}", encodeURIComponent(pageIndex.toString()));
        //
        //         app.webClient.rest("GET", url).do(body => callback(body));
        //     })
        //     // .refresh(1000*20)
        //     .add();


        app.navigation.addRoute("Dashboard", "/dashboard", [])
            .panel("leftNav", "builds-panel")
            .titled("Failing Projects")
            .modelProvider((params, callback) => {
                app.webClient.rest("GET", "/api/v1/projects/requisitions")
                    .do(x => {
                        let list = [];
                        for (let j = 0; j < x.length; j++) {
                            let branches = x[j].knownBranches;
                            for (let k = 0; k < branches.length; k++) {
                                let branch = branches[k] || {buildData: {state: null}};

                                let buildData = branch.buildData || {state: null};

                                if (buildData.state === "CompletedFail") {
                                    list.push(buildData)
                                }
                            }
                        }

                        callback(list);

                    });
            })
            .refresh(1000 * 20)
            .add()
            // 
            .panel("main", "builds-panel")
            .titled("Recent Builds")
            .modelProvider((params, callback) => {
                let url = "/api/v1/jobs?PageIndex={pageIndex}&PageSize={pageSize}&sort=DateCreatedDescending";
                let pageSize = 5;
                let pageIndex = 0;
                url = url.replace("{pageSize}", encodeURIComponent(pageSize.toString()))
                    .replace("{pageIndex}", encodeURIComponent(pageIndex.toString()));

                app.webClient.rest("GET", url).do(body => callback(body));
            })
            .refresh(1000 * 20)
            .add()
            // 
            .panel("leftNav", "builds-panel")
            .titled("Failed Builds")
            .modelProvider((params, callback) => {
                let url = "/api/v1/jobs?State=CompletedFail&PageSize=5";
                app.webClient.rest("GET", url).do(body => callback(body));
            })
            .refresh(1000 * 20)
            .add()
            //
            .panel("rightNav", "servers-panel")
            .titled("Source Control Servers")
            .requireRole("Server:Viewer")
            .modelProvider((params, callback) => {
                let url = "/api/v1/servers/SourceControl";
                app.webClient.rest("GET", url).do(body => callback(body));
            })
            .refresh(1000 * 20)
            .add()
            //
            .panel("rightNav", "servers-panel")
            .requireRole("Server:Viewer")
            .titled("Compilation Servers")
            .modelProvider((params, callback) => {
                let url = "/api/v1/servers/Compilation";
                app.webClient.rest("GET", url).do(body => callback(body));
            })
            .refresh(1000 * 20)
            .add();


        app.navigation.panel("footer", "application-footer").modelProvider((params, callback) => {
            callback({companyName: "Base2art", companyUrl: "//base2art.com/"});
        }).add();

        app.navigation.addRoute("Projects", "/projects", [])
            .panel("mainAbove", "dashboard-projects")
            // .titled("")
            .modelProvider((params, callback) => {
                app.webClient.rest("GET", "/api/v1/projects/requisitions")
                    .do(x => callback(x));
            }).add();



        // VIEW PROJECT ROUTE
        app.navigation.addRoute("New Project", "/projects/new", [], null, false)// ["confirm"]
            .panel("main", "project-edit")
            .titled("Add Project")
            .requireRole("Project:Create")
            .modelProvider((params, callback) => {

                callback({})
                // app.webClient.rest("GET", url).do(body => callback(body));
            })
            // .refresh(1000 * 20)
            .add();


        // VIEW BUILD ROUTE

        app.navigation.addRoute("Builds", "/projects/{projectId}/builds/{buildId}", ["projectId", "buildId"], "Build:Viewer")
            .panel("main", "build-details")
            .titled("Build")
            .requireRole("Build:Viewer")
            .modelProvider((params, callback) => {

                let url = "/api/v1/jobs/{jobId}";
                url = url.replace("{jobId}", encodeURIComponent(params.buildId));

                app.webClient.rest("GET", url).do(body => callback(body));
            })
            .refresh(1000 * 20)
            .add();

        // VIEW PROJECT ROUTE
        app.navigation.addRoute("Builds By Project", "/projects/{projectId}", ["projectId"])
            .panel("main", "builds-panel")
            .titled("All Builds")
            .requireRole("Build:Lister")
            .modelProvider((params, callback) => {
                let url = "/api/v1/jobs?PageIndex={pageIndex}&PageSize={pageSize}&sort=DateCreatedDescending&ProjectId={projectId}";
                let pageSize = 100;
                let pageIndex = 0;
                url = url.replace("{pageSize}", encodeURIComponent(pageSize.toString()))
                    .replace("{pageIndex}", encodeURIComponent(pageIndex.toString()))
                    .replace("{projectId}", encodeURIComponent(params.projectId));

                app.webClient.rest("GET", url).do(body => callback(body));
            })
            .refresh(1000 * 20)
            .add()
            //
            .panel("rightNav", "servers-panel")
            .titled("Eligible Compilation Servers")
            .requireRole("Server:Viewer")
            .modelProvider((params, callback) => {
                let url = "/api/v1/projects/{projectId}/servers/Compilation"
                    .replace("{projectId}", encodeURIComponent(params.projectId));
                app.webClient.rest("GET", url).do(body => callback(body));
            }).add()

        //
            .panel("rightNav", "servers-panel")
            .titled("Eligible Checkout Servers")
            .requireRole("Server:Viewer")
            .modelProvider((params, callback) => {
                let url = "/api/v1/projects/{projectId}/servers/SourceControl"
                    .replace("{projectId}", encodeURIComponent(params.projectId));
                app.webClient.rest("GET", url).do(body => callback(body));
            }).add();

        // VIEW PROJECT ROUTE
        app.navigation.addRoute("Edit Project", "/projects/{projectId}/edit", ["projectId"])
            .panel("main", "project-edit")
            .titled("Edit Project")
            .requireRole("Project:Edit")
            .modelProvider((params, callback) => {

                let loaded1 = null;
                let loaded3 = null;
                let loaded2 = null;

                let go = function() {
                    if (loaded1 && loaded2 && loaded3) {
                        callback({source: loaded1, compile: loaded3, project: loaded2});
                    }
                };
                

                let url = "/api/v1/projects/{projectId}";
                url = url.replace("{projectId}", encodeURIComponent(params.projectId));

                app.webClient.rest("GET", "/api/v1/servers/SourceControl/features").do(function(body) { loaded1 = body; go(); });
                app.webClient.rest("GET", url).do(function(body) { loaded2 = body; go(); });
                app.webClient.rest("GET", "/api/v1/servers/Compilation/features").do(function(body) { loaded3 = body; go(); });
                
                
                // app.webClient.rest("GET", url).do(body => callback(body));
            })
            // .refresh(1000 * 20)
            .add();
            //
            // .panel("rightNav", "servers-panel")
            // .titled("Eligible Compilation Servers")
            // .requireRole("Server:Viewer")
            // .modelProvider((params, callback) => {
            //     let url = "/api/v1/projects/{projectId}/servers/Compilation"
            //         .replace("{projectId}", encodeURIComponent(params.projectId));
            //     app.webClient.rest("GET", url).do(body => callback(body));
            // }).add()

        //
        //     .panel("rightNav", "servers-panel")
        //     .titled("Eligible Checkout Servers")
        //     .requireRole("Server:Viewer")
        //     .modelProvider((params, callback) => {
        //         let url = "/api/v1/projects/{projectId}/servers/SourceControl"
        //             .replace("{projectId}", encodeURIComponent(params.projectId));
        //         app.webClient.rest("GET", url).do(body => callback(body));
        //     }).add();


        // BUILDS ROUTE
        app.navigation.addRoute("Builds", "/builds", [])
            .panel("main", "build-list")
            .titled("All Builds")
            .requireRole("Build:Lister")
            .modelProvider((params, callback) => {
                let url = "/api/v1/jobs?PageIndex={pageIndex}&PageSize={pageSize}&sort=DateCreatedDescending";
                let pageSize = 100;
                let pageIndex = 0;
                url = url.replace("{pageSize}", encodeURIComponent(pageSize.toString()))
                    .replace("{pageIndex}", encodeURIComponent(pageIndex.toString()));

                app.webClient.rest("GET", url).do(body => callback(body));
            })
            .refresh(1000 * 30).add();


        app.navigation.addRoute("Servers", "/servers", [], "Server:Viewer")
            .panel("main", "server-list")
            .titled("Servers")
            .requireRole("Server:Viewer")
            .modelProvider((params, callback) => {
                let url = "/api/v1/servers";
                app.webClient.rest("GET", url).do(body => callback(body));
            }).add()
            .panel("main", "user-form").titled("Register Server")
            .modelProvider((params, callback) => {
                callback({
                    title: function (value) {

                        if (value == null || value.server == null) {
                            return "Server: Creating...";
                        }

                        return "Server: " + value.server.name;
                    },
                    handleSubmit: function (value) {

                        app.actions.addServer(value.serverName, value.url, value.serverType, app.actions.reload);
                    },
                    handleValidate: function (value) {

                        let errors = [];
                        if ((value.serverName || "").length === 0) {
                            errors.push("Server Name Required");
                        }

                        if ((value.url || "").length === 0) {
                            errors.push("Url Required");
                        }
                        else {
                            let parser = document.createElement('a');
                            parser.href = value.url;
                            if (parser.href !== value.url) {
                                errors.push("Url improperly formatted, try: `" + parser.href + "`");
                            }
                        }

                        return errors;
                    },
                    fields: [
                        {
                            type: "text",
                            placeholder: "Build Server #1",
                            label: "Server Name",
                            key: "serverName"
                        },
                        {
                            type: "text",
                            placeholder: "https://www.buildserver.com/",
                            label: "Url",
                            key: "url"
                        },
                        // {
                        //     type: "chooser",
                        //     options: [
                        //         {
                        //             label: "Enabled",
                        //             value: true,
                        //             chosen: true
                        //         }
                        //     ],
                        //     minimum: null,
                        //     maximum: null,
                        //     key: "enabled"
                        // },
                        {
                            type: "chooser",
                            label: "Server Type",
                            options: [
                                {
                                    label: "Compilation",
                                    value: "Compilation",
                                    chosen: false
                                },
                                {
                                    label: "Source Control",
                                    value: "SourceControl",
                                    chosen: true
                                }

                            ],
                            minimum: 1,
                            maximum: 1,
                            key: "serverType"
                        }

                    ],
                    value: {}
                });
            }).add();


        app.navigation.addRoute("Swagger", "/swagger/", [])
            .panel("main", null)
            .modelProvider((params, callback) => {
                window.location = "/swagger/"
            })
            .add();

        // app.navigation.addRoute("Dashboard", "dashboard-projects", "Dashboard:View", "/dashboard", [], (params, callback) => {
        //     apiClient.projectRequisitions(x => callback(x));
        // });

    };

    return {
        configure: configure
    };


})();


/*



    app.navigation.addRoute(
        "View Build",
        "card-view-build",
        "BuildItems:View",
        "/projects/{projectId}/builds/{buildId}",
        ["projectId", "buildId"],
        (params, callback) => {
            apiClient.getBuild(params.buildId, (y) => {
                // app.repository.upsert(x => x.builds, y);
                // app.repository.upsert(x => x.jobs, y);
                callback(y);
            })
        });

    app.navigation.addRoute(
        "View Build",
        "card-jobs",
        "Builds:View",
        "/projects/{projectId}/builds",
        ["projectId"],
        (params, callback) => {

            apiClient.getBuildsByProjectBranch(params.projectId, null, 0, 100, function (jobs) {
                callback(jobs);
            });
        });

    app.navigation.addRoute(
        "Edit Project",
        "edit-project",
        "Projects:Edit",
        "/projects/{projectId}/edit",
        ["projectId"],

        (params, callback) => {

            var loaded1 = null;
            var loaded3 = null;
            var loaded2 = null;

            apiClient.getAllSourceControlFeatures(function (features) {
                loaded1 = features;
                if (loaded1 && loaded2 && loaded3) {
                    callback({source: loaded1, compile: loaded3, project: loaded2});
                }
            });

            apiClient.getAllCompilationFeatures(function (features) {
                loaded3 = features;
                if (loaded1 && loaded2 && loaded3) {
                    callback({source: loaded1, compile: loaded3, project: loaded2});
                }
            });

            apiClient.projects(function (projs) {
                var proj = projs.filter(x => x.id == params.projectId)[0];

                loaded2 = proj;
                if (loaded1 && loaded2 && loaded3) {
                    callback({source: loaded1, compile: loaded3, project: loaded2});
                }
            });
        });

    
    
    app.navigation.addRoute("Dashboard", "dashboard-projects", "Dashboard:View", "/dashboard", [], (params, callback) => {
        apiClient.projectRequisitions(x => callback(x));
    });

    app.navigation.addRoute("Builds", "card-jobs", "Builds:View", "/builds", [], (params, callback) => {
        apiClient.getBuildsByProjectBranch(null, null, 0, 100, function (jobs) {
            callback(jobs);
        });
    });

    app.navigation.addRoute("Servers", "page-servers", "Servers:View", "/servers", [], (params, callback) => {

        apiClient.getServers(function (servers) {
            callback(servers);
        });
    });

    app.navigation.addRoute("Projects", "card-projects", "Projects:View", "/projects", [], (params, callback) => {
        apiClient.projects(function (projects) {
            callback(projects);
        });
    });

    app.navigation.addRoute("Swagger", null, "Swagger:View", "/swagger", [], (params, callback) => {
        window.location = "/swagger/"
    });


* 
* 
* */