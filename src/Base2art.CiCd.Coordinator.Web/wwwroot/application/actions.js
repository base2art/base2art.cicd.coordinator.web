var ApplicationActions = (function (window1, document1) {

    let wndw = window1;
    let doc = document1;
    var configure = function (app) {

        if (window.location.hostname === "localhost") {

            app.security.setAuthenticator((function (userName) {
                let userName1 = userName;

                return {
                    id: function () {
                        return userName1;
                    },
                    isAuthenticated: function () {
                        return userName1 != null && userName1.length > 0;
                    },
                    hasRole: function (role) {
                        return true;
                    },
                    canSignIn: function () {
                        return true;
                    },
                    signIn: function () {

                    }
                };
            })("Local Development"));
            // ""
        } else {

            app.security.setAuthenticator((function () {

                let getCookie = function (cname) {
                    let name = cname + "=";
                    let decodedCookie = decodeURIComponent(document.cookie);
                    let ca = decodedCookie.split(';');
                    for (let i = 0; i < ca.length; i++) {
                        let c = ca[i];
                        while (c.charAt(0) === ' ') {
                            c = c.substring(1);
                        }

                        if (c.indexOf(name) === 0) {
                            return c.substring(name.length, c.length);
                        }
                    }

                    return "";
                };

                let userName1 = "";
                let isSignedIn = false;

                let authInfo = getCookie("auth_info");
                if (authInfo.length > 0) {
                    let token = JSON.parse(authInfo);

                    userName1 = token.sub;

                    if (token.exp > (Date.now() / 1000)) {
                        isSignedIn = true;
                    }
                }

                return {
                    id: function () {

                        return userName1;
                    },
                    isAuthenticated: function () {
                        return isSignedIn && userName1 != null && userName1.length > 0;
                    },
                    hasRole: function (role) {
                        return userName1 != null && userName1.length > 0;
                    },
                    canSignIn: function () {
                        return true;
                    },
                    signIn: function () {

                        let primaryDomain = function () {
                            return wndw.location.host.split('.').slice(-2).join(".");
                        };

                        let path = wndw.location.pathname;
                        path = path.substring(1);
                        path += "?" + wndw.location.search;

                        let appName = "builder";


                        let localSigninServer = wndw["signinServer"];
                        if (localSigninServer == null) {
                            localSigninServer = 'https://sso.' + primaryDomain() + '/';
                        }

                        let destLocation = localSigninServer + '?app.name=' + appName + '&returnUrl=' + encodeURIComponent((wndw.location.pathname + wndw.location.search).substr(1));
                        wndw.location = destLocation;
                    }
                };
            })());
        }


        app.addAction("navigate", function (path, params) {
            app.navigation.navigate(path, params);

        });


        app.addAction("addServer", function (name, serverUrl, serverType, callback) {

            app.module("webClient")
                .rest("PUT", "/api/v1/servers/{serverType}", {"serverType": serverType})
                .withBody({"name": name, "url": serverUrl, "enabled": true})
                .do(body => (callback || function (x) {
                })(body));

        });

        app.addAction("build", function (projectId, branchName) {

            app.module("webClient")
                .rest("PUT", '/api/v1/jobs')
                .withBody({"projectId": projectId, "branchName": branchName})
                .do(body => (console.log(body)));
        });


        app.addAction("refreshBranches", function (projectId) {

            app.module("webClient")
                .rest("POST", '/tasks/refresh-branches')
                .withBody({"projectId": projectId})
                .do(body => (console.log(body)));
        });

        app.addAction("reload", function () {

            // let tag = doc.getElementById("myBabaBuiApp")._tag;
            // tag.update();

        });

        app.addAction("setSourceControl", function (
            projectId, 
            sourceControlType, 
            data, 
            executableFile, 
            executableArguments,
            callback) {


            app.module("webClient")
                .rest("POST", '/api/v1/projects/{projectId}/sourcecontrol', {"projectId": projectId})
                .withBody([
                    {
                        "sourceControlType": sourceControlType,
                        "sourceControlData": data,
                        "buildInstructions": [
                            {
                                "executableFile": executableFile,
                                "executableArguments": executableArguments
                            }
                        ]
                    }])
                .do(body => callback(body));

        });

        app.addAction("setFeatures", function (projectId, featureIds, callback) {

            let features = featureIds.map(function (x) {
                return {id: x};
            });
            app.module("webClient")
                .rest("POST", '/api/v1/projects/{projectId}/features', {"projectId": projectId})
                .withBody(features)
                .do(body => callback(body));
        });

        app.addAction("addProject", function (value, callback) {

            app.module("webClient")
                .rest("PUT", '/api/v1/projects')
                .withBody(value)
                .do(body => callback(body));
        });
    };

    return {
        configure: configure
    };


})(window, document);
