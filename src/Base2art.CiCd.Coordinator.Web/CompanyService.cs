namespace Base2art.CiCd.Coordinator.Web
{
    using System;

    public class CompanyService : ICompanyService
    {
        private readonly string companyName;
        private readonly int escapeDefaultCompanyName;

        public CompanyService(string companyName, string escapeDefaultCompanyName)
        {
            this.companyName = companyName;

            var esc = 2;
            if (!string.IsNullOrWhiteSpace(escapeDefaultCompanyName) && !escapeDefaultCompanyName.StartsWith("#{"))
            {
                try
                {

                    esc = bool.Parse(escapeDefaultCompanyName) ? 2 : 0;
                }
                catch
                {
                    esc = int.Parse(escapeDefaultCompanyName);
                }
            }

            this.escapeDefaultCompanyName = esc;
        }

        public string CompanyName() => this.companyName;

        public int EscapeDefaultCompanyName() => this.escapeDefaultCompanyName;
    }
}