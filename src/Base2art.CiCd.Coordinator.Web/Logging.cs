namespace Base2art.CiCd.Coordinator.Web
{
    using System;
    using System.Threading.Tasks;
    using Public.MessageHandlers.PhaseHandlers.Logging;

    public static class Logging
    {
        public static Task AddMessage(this IJobStoreService service,
                                      Guid buildId,
                                      string projectName,
                                      string message,
                                      LogEntry entry)
            => service.AddMessage(buildId, projectName, entry.Phase, message, entry.Message());

        // public static async Task Log(Exception exception)
        // {
        //     using (var sr = CreateFile())
        //     {
        //         await sr.WriteLineAsync(exception.GetType().FullName);
        //         await sr.WriteLineAsync(exception.Message);
        //         await sr.WriteLineAsync(exception.StackTrace);
        //         await sr.FlushAsync();
        //     }
        // }

        // public static TextWriter CreateFile()
        // {
        //     var dir = Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "logs"));
        //
        //     var combine = Path.Combine(dir.FullName, $"{DateTime.UtcNow.Ticks:X}.app_log");
        //     var writer = new FileStream(combine, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite);
        //     return new StreamWriter(writer);
        // }
    }
}