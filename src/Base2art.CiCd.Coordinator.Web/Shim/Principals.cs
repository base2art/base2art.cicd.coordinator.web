namespace Base2art.CiCd.Coordinator.Web
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Cryptography;
    using Microsoft.IdentityModel.Tokens;
    using Models;

    public static class Principals
    {
        public static ClaimsPrincipal CreatePrincipal(IKey[] keys, Guid? serverAuthenticationKeyId, ServerType pluginType)
        {
            if (serverAuthenticationKeyId == null)
            {
                return new ClaimsPrincipal(new ClaimsIdentity());
            }

            var key = keys.FirstOrDefault(x => x.Id == serverAuthenticationKeyId);
            if (key == null)
            {
                return new ClaimsPrincipal(new ClaimsIdentity());
            }

            var parameters = new RSAParameters
                             {
                                 Exponent = key.Exponent,
                                 Modulus = key.Modulus,
                                 D = key.D,
                                 DP = key.DP,
                                 DQ = key.DQ,
                                 P = key.P,
                                 Q = key.Q,
                                 InverseQ = key.InverseQ
                             };
            
            string token = GenerateToken(parameters, pluginType);

            #if DEBUG
            // Console.WriteLine(token);
            #endif
            
            var claim = new Claim(ClaimTypes.Authentication, token, ClaimValueTypes.String);

            var claimsIdentity = new ClaimsIdentity(new[] {claim}, "Jwt-Client", ClaimTypes.Authentication, ClaimTypes.Role);
            return new ClaimsPrincipal(claimsIdentity);
        }

        public static string GenerateToken(RSAParameters parameters, ServerType serverType)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            var nbf = DateTime.UtcNow.AddMinutes(-10);
            var exp = DateTime.UtcNow.AddMinutes(300);
            var iss = "http://build-master.base2art.com/";
            var aud = GetAudience(serverType);

//            var securityKey = new RsaSecurityKey(parameters);
            //securityKey.KeySize = 2048;
            var rsa = new RSACryptoServiceProvider(2048);
            rsa.ImportParameters(parameters);
            var key = new RsaSecurityKey(rsa);
            key.KeyId = "version1";
            var tokenDescriptor = new SecurityTokenDescriptor
                                  {
                                      Subject = new ClaimsIdentity(new[]
                                                                   {
                                                                       new Claim(ClaimTypes.Name, Environment.MachineName),
                                                                       new Claim(ClaimTypes.Version, "1"),
                                                                   }),
                                      Expires = exp,
                                      Audience = aud,
                                      Issuer = iss,
                                      NotBefore = nbf,

                                      SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.RsaSha512)
                                  };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);

            return token;
        }

        private static readonly Dictionary<ServerType, string> lookup = new Dictionary<ServerType, string>
                                                                        {
                                                                            {ServerType.SourceControl, "http://source-node.base2art.com/"},
                                                                            {ServerType.Compilation, "http://build-node.base2art.com/"},
                                                                            {ServerType.Publish, "http://publish-node.base2art.com/"},
                                                                        };

        private static string GetAudience(ServerType serverType)
        {
            if (lookup.ContainsKey(serverType))
            {
                return lookup[serverType];
            }

            return "http://node.base2art.com/";
        }
    }
}