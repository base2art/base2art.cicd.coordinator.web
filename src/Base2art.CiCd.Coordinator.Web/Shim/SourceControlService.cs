namespace Base2art.CiCd.Coordinator.Web
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using CiCd.Servers;
    using Models;
    using Serialization;
    using SourceControl;
    using SourceControl.Resources;

    public class SourceControlService
    {
        private readonly ICheckoutPhaseServiceFactory checkoutPhaseServiceFactory;

        public SourceControlService(ICheckoutPhaseServiceFactory checkoutPhaseServiceFactory)
        {
            this.checkoutPhaseServiceFactory = checkoutPhaseServiceFactory;
        }

        public async Task<IsolatedBranchData[]> GetBranches(IKey[] keys, Guid projectSourceControlType, object projectSourceControlData)
        {
            var clientWrapper = await this.checkoutPhaseServiceFactory.CreateClient(projectSourceControlType);
            var client = clientWrapper.Service;

            var jsonSerializer = new SimpleJsonSerializer();
            var content = jsonSerializer.Serialize(projectSourceControlData);
            var obj = jsonSerializer.Deserialize<Dictionary<string, string>>(content);

            var data =
                await client.GetBranches(obj, Principals.CreatePrincipal(keys, clientWrapper.CurrentServer.AuthenticationKeyId, ServerType.SourceControl));
            return data;
        }

        public async Task<IsolatedBranchLogData> GetLogs(
            IKey[] keys,
            Guid sourceControlType,
            object projectSourceControlData,
            string branchName,
            string branchHash)
        {
            var clientWrapper = await this.checkoutPhaseServiceFactory.CreateClient(sourceControlType);
            var client = clientWrapper.Service;
            var jsonSerializer = new SimpleJsonSerializer();
            var content = jsonSerializer.Serialize(projectSourceControlData);
            var obj = jsonSerializer.Deserialize<Dictionary<string, string>>(content);

            var data = await client.GetLogs(
                                            new RepositoryBranchData<Dictionary<string, string>>
                                            {
                                                Branch = new IsolatedBranchData
                                                         {
                                                             Hash = branchHash,
                                                             Name = branchName
                                                         },
                                                Repository = obj
                                            },
                                            Principals.CreatePrincipal(keys, clientWrapper.CurrentServer.AuthenticationKeyId, ServerType.SourceControl));
            return data;
        }
        
        
        public async Task<IsolatedCheckout> StartCheckout(
            IServerData currentServer,
            ICheckoutPhaseService service,
            IKey[] keys,
            object projectSourceControlData,
            string branchOrTag)
        {
            var client = service;
            var jsonSerializer = new SimpleJsonSerializer();
            var content = jsonSerializer.Serialize(projectSourceControlData);
            var obj = jsonSerializer.Deserialize<Dictionary<string, string>>(content);

            var data = await client.StartCheckout(
                                                  obj,
                                                  branchOrTag,
                                                  Principals.CreatePrincipal(keys, currentServer.AuthenticationKeyId, ServerType.SourceControl));
            return data;
        }

        // public async Task<(IServerData CurrentServer, IsolatedCheckout data)> StartCheckout(
        //     IKey[] keys, Guid projectSourceControlType, object projectSourceControlData, string branchOrTag)
        // {
        //     var clientWrapper = await this.checkoutPhaseServiceFactory.CreateClient(projectSourceControlType);
        //     var client = clientWrapper.Service;
        //     var jsonSerializer = new SimpleJsonSerializer();
        //     var content = jsonSerializer.Serialize(projectSourceControlData);
        //     var obj = jsonSerializer.Deserialize<Dictionary<string, string>>(content);
        //
        //     var data = await client.StartCheckout(
        //                                           obj,
        //                                           branchOrTag,
        //                                           Principals.CreatePrincipal(keys, clientWrapper.CurrentServer.AuthenticationKeyId, ServerType.SourceControl));
        //     return (clientWrapper.CurrentServer, data);
        // }

        public async Task<IsolatedCheckout> GetCheckoutStatus(
            IKey[] keys, IServerData serverData, Guid checkoutId)
        {
            var client = await this.checkoutPhaseServiceFactory.CreateClient(serverData.Url.ToString());
            var data = await client.GetCheckoutStatus(
                                                      checkoutId,
                                                      Principals.CreatePrincipal(
                                                                                 keys,
                                                                                 serverData.AuthenticationKeyId,
                                                                                 ServerType.SourceControl));
            return data;
        }

        public async Task<IsolatedSourceData> GetSource(IKey[] keys, IServerData serverData, Guid checkoutId)
        {
            var client = await this.checkoutPhaseServiceFactory.CreateClient(serverData.Url.ToString());
            var data = await client.GetSourceWithMetaData(
                                                          checkoutId,
                                                          Principals.CreatePrincipal(
                                                                                     keys,
                                                                                     serverData.AuthenticationKeyId,
                                                                                     ServerType.SourceControl));
            return data;
        }

        public async Task RemoveSource(IKey[] keys, IServerData serverData, Guid checkoutId)
        {
            var client = await this.checkoutPhaseServiceFactory.CreateClient(serverData.Url.ToString());

            await client.RemoveSource(checkoutId, Principals.CreatePrincipal(
                                                                             keys,
                                                                             serverData.AuthenticationKeyId,
                                                                             ServerType.SourceControl));
        }
    }
}