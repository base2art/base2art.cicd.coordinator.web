namespace Base2art.CiCd.Coordinator.Web.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.CodeAnalysis.CSharp.Syntax;

    public static class Enumerables
    {
        public static bool In<T>(this T item, params T[] items)
            where T : struct, IComparable
        {
            return (items ?? new T[0]).Any(y => item.CompareTo(y) == 0);
        }

        public static IEnumerable<T> TakeAbout<T>(this IReadOnlyList<T> items, int numToTake)
        {
            return TakeAbout(items, numToTake, (int) (numToTake * 1.5));
        }

        public static IEnumerable<T> TakeAbout<T>(this IReadOnlyList<T> items, int numToTake, int max)
            => items.Count > max ? items.Take(numToTake) : items;

        public static IEnumerable<T> Randomize<T>(this IEnumerable<T> values)
        {
            return values.Select((x) => (x, Guid.NewGuid().ToString("N")))
                         .OrderBy(x => x.Item2)
                         .Select(x => x.x);
        }

        public static KeyValuePair<T1, T2> Pair<T1, T2>(this (T1, T2) value)
        {
            return new KeyValuePair<T1, T2>(value.Item1, value.Item2);
        }
    }
}