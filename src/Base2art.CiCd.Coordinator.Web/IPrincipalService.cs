namespace Base2art.CiCd.Coordinator.Web
{
    using System.Security.Claims;
    using System.Security.Principal;

    public interface IPrincipalService
    {
        void RequireAuthenticated(IPrincipal principal);

        string GetUserName(ClaimsPrincipal principal);
    }
}