namespace Base2art.CiCd.Coordinator.Web
{
    public interface ICompanyService
    {
        string CompanyName();
        int EscapeDefaultCompanyName();
    }
}