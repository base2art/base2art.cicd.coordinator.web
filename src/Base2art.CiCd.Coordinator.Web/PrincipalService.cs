namespace Base2art.CiCd.Coordinator.Web
{
    using System.Security.Claims;
    using System.Security.Principal;
    using Base2art.Web.Exceptions;

    public class PrincipalService : IPrincipalService
    {
        private readonly bool skipCheck;

        public PrincipalService(bool skipCheck)
        {
            this.skipCheck = skipCheck;
        }

        public void RequireAuthenticated(IPrincipal principal)
        {
            if (this.skipCheck)
            {
                return;
            }

            var isAuthenticated = principal?.Identity?.IsAuthenticated;

            if (!isAuthenticated.GetValueOrDefault())
            {
                throw new NotAuthenticatedException();
            }
        }

        public string GetUserName(ClaimsPrincipal principal)
        {
            if (principal != null)
            {
                var email = principal.FindFirst(ClaimTypes.Email);
                var item = email?.Value;
                if (!string.IsNullOrWhiteSpace(item))
                {
                    return item;
                }
            }

            return (this.skipCheck ? "Local Development" : "(Unknown)");
        }
    }
}