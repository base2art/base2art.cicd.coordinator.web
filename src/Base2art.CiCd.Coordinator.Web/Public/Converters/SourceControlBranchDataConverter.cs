namespace Base2art.CiCd.Coordinator.Web.Public.Converters
{
    using System.Linq;
    using Models;
    using SourceControl.Resources;

    public static class SourceControlBranchDataConverter
    {
        public static SourceControlBranchData ConvertToInput(this IsolatedBranchData data)
            => new SourceControlBranchData
               {
                   Name = data.Name,
                   Hash = data.Hash
               };

        public static SourceControlBranchData ConvertToInput(this IBranchLogData data)
            => new SourceControlBranchData
               {
                   Name = data.Name,
                   Hash = data.Hash,
               };

        public static SourceControlBranchData[] ConvertToInput(this IBranchLogData[] data)
            => data.Select(x => x.ConvertToInput()).ToArray();

        public static SourceControlBranchData[] ConvertToInput(this IsolatedBranchData[] data)
            => data.Select(x => x.ConvertToInput()).ToArray();
    }
}
