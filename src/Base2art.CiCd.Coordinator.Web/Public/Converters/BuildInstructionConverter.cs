namespace Base2art.CiCd.Coordinator.Web.Public.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Models;

    public static class BuildInstructionConverter
    {
        public static Dictionary<Guid, BuildInstructionData[]> ConvertToInput(
            this Dictionary<Guid, IBuildInstruction[]> buildInstructionLookup)
        {
            var lookup = new Dictionary<Guid, BuildInstructionData[]>();

            foreach (var instructionPair in buildInstructionLookup)
            {
                lookup[instructionPair.Key] = instructionPair.Value.Select(ConvertToInput).ToArray();
            }

            return lookup;
        }

        public static BuildInstructionData ConvertToInput(this IBuildInstruction x) => new BuildInstructionData
                                                                                       {
                                                                                           ExecutableArguments = x.ExecutableArguments,
                                                                                           ExecutableFile = x.ExecutableFile
                                                                                       };
    }
}