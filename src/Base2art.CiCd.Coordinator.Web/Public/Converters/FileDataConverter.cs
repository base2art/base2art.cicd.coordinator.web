namespace Base2art.CiCd.Coordinator.Web.Public.Converters
{
    using Models;
    using Publisher.Resources;

    public static class FileDataConverter
    {
        public static FileData ConvertToInput(this IFileData data) => new FileData
                                                                      {
                                                                          Name = data.Name,
                                                                          Content = data.Content,
                                                                          RelativeName = data.RelativeName
                                                                      };
    }
}