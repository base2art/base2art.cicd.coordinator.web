namespace Base2art.CiCd.Coordinator.Web.Public.Messages
{
    using System;
    using System.Collections.Generic;
    using MessageQueue;
    using Models;

    public class BuildProjectBranchMessage : MessageBase
    {
        public Guid ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string UserName { get; set; }
        public Dictionary<Guid, SourceControlBranchData> BranchInfo { get; set; }
    }
}