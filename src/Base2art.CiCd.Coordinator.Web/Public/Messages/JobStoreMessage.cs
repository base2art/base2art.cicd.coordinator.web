namespace Base2art.CiCd.Coordinator.Web.Public.Messages
{
    using System;
    using System.Collections.Generic;
    using MessageQueue;
    using Models;

    public class JobStoreMessage : MessageBase
    {
        public Guid BuildId { get; set; }
        public string ProjectName { get; set; }
        public JobPhase Phase { get; set; }
        public string Message { get; set; }
        public Dictionary<string, object> Data { get; set; }
    }
}