namespace Base2art.CiCd.Coordinator.Web.Public.Messages
{
    using System;
    using System.Collections.Generic;
    using MessageQueue;

    public class SourceLookupMessage : MessageBase
    {
        public Guid SourceControlType { get; set; }
        public Dictionary<string, string> SourceControlData { get; set; }
        public string BranchName { get; set; }
        public string BranchHash { get; set; }
        public Guid SourceControlId { get; set; }

        public override string ToString()
            => $"{this.SourceControlType}: {this.SourceControlId} -> {this.BranchName}:{this.BranchHash}";
    }
}