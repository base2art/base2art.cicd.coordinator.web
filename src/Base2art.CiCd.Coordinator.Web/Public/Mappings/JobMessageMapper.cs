namespace Base2art.CiCd.Coordinator.Web.Public.Mappings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Models;
    using Resources;
    using Serialization;

    public static class JobMessageMapper
    {
        public static JobMessage MapToConcrete(this IJobMessage jobMessage, Func<string, string> lookup)
        {
            var yaml = new YamlDotNet.Serialization.Serializer();
            var json = new SimpleJsonSerializer();

            string MapValue(object value)
            {
                try
                {
                    return yaml.Serialize(value);
                }
                catch (Exception)
                {
                    return "";
                }
            }

            var next = jobMessage.Data ?? new Dictionary<string, object>();

            return new JobMessage
                   {
                       Data = next.ToDictionary(
                                                x => x.Key,
                                                x => x.Value),
                       DataText = lookup(MapValue(next)),
                       Message = lookup(jobMessage.Message),
                       Phase =  lookup(jobMessage.Phase)
                   };
        }
    }
}