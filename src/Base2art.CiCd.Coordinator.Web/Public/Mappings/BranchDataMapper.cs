namespace Base2art.CiCd.Coordinator.Web.Public.Mappings
{
    using System;
    using System.Collections.Generic;
    using Models;
    using Resources;
    using SourceControl.Resources;

    public static class BranchDataMapper
    {
        public static Dictionary<Guid, Branch> MapToConcrete(this IReadOnlyDictionary<Guid, IsolatedBranchData> old)
        {
            var lookup = new Dictionary<Guid, Branch>();

            foreach (var instructionPair in old)
            {
                lookup[instructionPair.Key] = MapToConcrete(instructionPair.Value);
            }

            return lookup;
        }

        public static Branch MapToConcrete(this IsolatedBranchData instructionPairValue) => new Branch
                                                                                {
                                                                                    Name = instructionPairValue.Name,
                                                                                    Hash = instructionPairValue.Hash
                                                                                };
    }
}