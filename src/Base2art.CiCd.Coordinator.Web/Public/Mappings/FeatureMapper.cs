namespace Base2art.CiCd.Coordinator.Web.Public.Mappings
{
    using System.Linq;
    using Models;
    using Resources;

    public static class FeatureMapper
    {
        public static Feature MapToConcrete(this IServerFeature x)
        {
            return new Feature
                   {
                       Id = x.Id,
                       DefaultData = x.DefaultData.ToDictionary(y => y.Key, y => y.Value),
                       SimpleName = x.SimpleName,
                       FullName = x.FullName
                   };
        }
    }
}