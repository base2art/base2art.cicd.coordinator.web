//namespace Base2art.CiCd.Coordinator.Web.Public.Mappings
//{
//    using System;
//    using System.Collections.Generic;
//    using Models;
//    using Resources;
//
//    public static class BuildInstructionMapper
//    {
//        public static Dictionary<Guid, BuildInstruction> MapToConcrete(this IReadOnlyDictionary<Guid, IBuildInstruction> old)
//        {
//            var lookup = new Dictionary<Guid, Branch>();
//
//            foreach (var instructionPair in old)
//            {
//                lookup[instructionPair.Key] = MapToConcrete(instructionPair.Value);
//            }
//
//            return lookup;
//        }
//
//        public static BuildInstruction MapToConcrete(IBuildInstruction instruction)
//        {
//            return new BuildInstruction
//                   {
//                       ExecutableFile = instruction.ExecutableFile,
//                       ExecutableArguments = instruction.ExecutableArguments
//                   };
//        }
//    }
//}

