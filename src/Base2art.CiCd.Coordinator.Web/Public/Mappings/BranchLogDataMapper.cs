namespace Base2art.CiCd.Coordinator.Web.Public.Mappings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Models;
    using Resources;

    public static class BranchLogDataMapper
    {
        public static Dictionary<Guid, Branch> MapToConcrete(this IReadOnlyDictionary<Guid, IBranchLogData> old)
        {
            var lookup = new Dictionary<Guid, Branch>();

            foreach (var instructionPair in old)
            {
                lookup[instructionPair.Key] = MapToConcrete(instructionPair.Value);
            }

            return lookup;
        }

        public static Branch MapToConcrete(this IBranchLogData instructionPairValue) => new Branch
                                                                                        {
                                                                                            Name = instructionPairValue.Name,
                                                                                            Hash = instructionPairValue.Hash,
                                                                                            CommitLogs = instructionPairValue.CommitLogs
                                                                                                                             .Select(x => Create(x))
                                                                                                                             .ToArray()
                                                                                        };

        private static CommitLog Create(ICommitLogData commitLogData)
        {
            return new CommitLog
                   {
                       Id = commitLogData.Id,
                       Author = commitLogData.Author,
                       Message = commitLogData.Message,
                       When = commitLogData.When,
                       FilesAdded = commitLogData.FilesAdded,
                       FilesRemoved = commitLogData.FilesRemoved,
                       FilesChanged = commitLogData.FilesChanged,
                   };
        }
    }
}