namespace Base2art.CiCd.Coordinator.Web.Public.Mappings
{
    using Models;
    using Resources;

    public static class FileDataMapper
    {
        public static File MapToConcrete(this IFileData data) => new File
                                                                 {
                                                                     Name = data.Name,
                                                                     Content = data.Content,
                                                                     RelativeName = data.RelativeName
                                                                 };
    }
}