namespace Base2art.CiCd.Coordinator.Web.Public.Mappings
{
    using System.Linq;
    using Models;
    using Resources;

    public static class ServerMapper
    {
        public static Server MapToConcrete(this IServer o)
        {
            return new Server
                   {
                       Name = o.Name,
                       Id = o.Id,
                       Enabled = o.Enabled,
                       ServerType = o.ServerType,
                       Features = (o.Features ?? new IServerFeature[0])
                                   .OrderBy(x => x.SimpleName)
                                   .Select(x => x.MapToConcrete())
                                   .ToArray(),
                       Url = o.Url,
                       AuthenticationKeyId = o.AuthenticationKeyId
                   };
        }
    }
}