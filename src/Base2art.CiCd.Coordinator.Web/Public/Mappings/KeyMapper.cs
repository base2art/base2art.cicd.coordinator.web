namespace Base2art.CiCd.Coordinator.Web.Public.Mappings
{
    using Models;
    using Resources;

    public static class KeyMapper
    {
        public static Key MapToConcrete(this IKey data, bool includePrivateParts)
        {
            if (includePrivateParts)
            {
                return new Key
                       {
                           Id = data.Id,
                           Name = data.Name,
                           IsActive = data.IsActive,
                           Exponent = data.Exponent,
                           Modulus = data.Modulus,
                           D = data.D,
                           P = data.P,
                           Q = data.Q,
                           DP = data.DP,
                           DQ = data.DQ,
                           InverseQ = data.InverseQ,
                       };
            }
            
            return new Key
                   {
                       Id = data.Id,
                       Name = data.Name,
                       IsActive = data.IsActive,
                       Exponent = data.Exponent,
                       Modulus = data.Modulus,
//                       D = data.D,
//                       P = data.P,
//                       Q = data.Q,
//                       DP = data.DP,
//                       DQ = data.DQ,
//                       InverseQ = data.InverseQ,
                   };
        }
    }
}