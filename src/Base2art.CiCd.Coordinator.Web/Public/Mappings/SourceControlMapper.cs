namespace Base2art.CiCd.Coordinator.Web.Public.Mappings
{
    using System.Collections.Generic;
    using System.Linq;
    using Models;
    using Resources;

    public static class SourceControlMapper
    {
        public static SourceControl MapToConcrete(this ISourceControlInfo projectSourceControl)
        {
            var next = projectSourceControl.SourceControlData ??
                       new Dictionary<string, string>();

            return new SourceControl
                   {
                       Id = projectSourceControl.Id,
                       SourceControlType = projectSourceControl.SourceControlType,
                       SourceControlData = next.ToDictionary(x => x.Key, x => x.Value),
                       BuildInstructions = projectSourceControl.BuildInstructions.Select(y => new BuildInstruction
                                                                                              {
                                                                                                  ExecutableFile = y.ExecutableFile,
                                                                                                  ExecutableArguments =
                                                                                                      y.ExecutableArguments ?? new string[0]
                                                                                              }).ToArray()
                   };
        }
    }
}