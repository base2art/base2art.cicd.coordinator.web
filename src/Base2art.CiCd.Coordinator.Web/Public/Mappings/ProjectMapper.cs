namespace Base2art.CiCd.Coordinator.Web.Public.Mappings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography.X509Certificates;
    using Models;
    using Resources;

    public static class ProjectMapper
    {
        public static Project MapToConcrete(this IProject project, IServerFeature[] allFeatures)
        {
            var dict = new Dictionary<Guid, IServerFeature>();

            foreach (var feature in allFeatures)
            {
                dict[feature.Id] = feature;
            }

            allFeatures = dict.Values.ToArray();

            return new Project
                   {
                       Name = project.Name,
                       Id = project.Id,
                       IsActive = project.IsActive,
                       RequiredFeatures = allFeatures.Where(x => project.RequiredFeatures.Contains(x.Id))
                                                     .Select(x => x.MapToConcrete())
                                                     .ToArray(),
                       RequiredFeatureIds = allFeatures.Where(x => project.RequiredFeatures.Contains(x.Id))
                                                       .Select(x => x.MapToConcrete())
                                                       .Select(x => x.Id)
                                                       .ToArray(),
                       SourceControl = project.SourceControl.Select(x => x.MapToConcrete()).ToArray(),
                       Versioning = new Versioning
                                    {
                                        Strategy = project.Versioning.Strategy,
                                        Data = project.Versioning.DefaultData.ToDictionary(x => x.Key, x => x.Value),
                                    }
                   };
        }
    }
}