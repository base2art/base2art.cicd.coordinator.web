namespace Base2art.CiCd.Coordinator.Web.Public.Mappings
{
    using System.Linq;
    using Models;

    public static class VersioningMapper
    {
        public static VersioningData MapToConcrete(this IVersioning value)
        {
            return new VersioningData
                   {
                       Strategy = value.Strategy,
                       DefaultData = value.DefaultData.ToDictionary(x => x.Key, x => x.Value)
                   };
        }
    }
}