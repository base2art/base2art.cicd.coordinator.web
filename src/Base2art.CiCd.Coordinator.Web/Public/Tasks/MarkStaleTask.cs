namespace Base2art.CiCd.Coordinator.Web.Public.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;

    public class MarkStaleTask
    {
        private readonly IJobStoreService jobStoreService;

        public MarkStaleTask(IJobStoreService jobStoreService) => this.jobStoreService = jobStoreService;

        public async Task ExecuteAsync()
        {
            await this.MarkStale(JobState.Working, JobState.RunningLong, DateTime.UtcNow.AddHours(-2));
            await this.MarkStale(JobState.RunningLong, JobState.CompletedFail, DateTime.UtcNow.AddDays(-1));
        }

        private async Task MarkStale(JobState fromState, JobState toState, DateTime limit)
        {
            var jobs = await this.jobStoreService.FindJobs(fromState, null, null, null, 100, 0, JobSort.DateCreatedAscending);

            foreach (var job in jobs)
            {
                if (job.DateCreated < limit)
                {
                    await this.jobStoreService.AddMessage(
                                                          job.Id,
                                                          job.ProjectId.ToString("N"),
                                                          JobPhase.SystemCleanup,
                                                          "Changing Job state",
                                                          new Dictionary<string, object>
                                                          {
                                                              {"From", fromState},
                                                              {"To", toState}
                                                          });
                    await this.jobStoreService.SetState(job.Id, toState);
                }
            }
        }
    }
}