namespace Base2art.CiCd.Coordinator.Web.Public.Tasks
{
    using System.Threading.Tasks;
    using Base2art.MessageQueue;
    using Messages;

    public class RefreshServerFeaturesTask
    {
        private readonly IMessageQueue queue;

        public RefreshServerFeaturesTask(IMessageQueue queue)
            => this.queue = queue;

        public Task ExecuteAsync()
            => this.queue.Push(new RefreshServersMessage());
    }
}