namespace Base2art.CiCd.Coordinator.Web.Public.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Base2art.MessageQueue;
    using Converters;
    using Messages;
    using Models;
    using Repositories;
    using SourceControl;

    public class BranchLookupTask
    {
        private readonly IProjectService projectService;
        private readonly IKeyRepository keys;
        private readonly SourceControlService sourceControl;
        private readonly ISourceRepository sourceData;
        private readonly IMessageQueue messageQueue;

        public BranchLookupTask(
            IProjectService projectService,
            IKeyRepository keys,
            ISourceRepository sourceData,
            IMessageQueue messageQueue,
            ICheckoutPhaseServiceFactory checkoutPhaseServiceFactory)
        {
            this.projectService = projectService;
            this.keys = keys;
            this.sourceControl = new SourceControlService(checkoutPhaseServiceFactory);
            this.sourceData = sourceData;
            this.messageQueue = messageQueue;
        }

        public async Task<string> ExecuteAsync(Dictionary<string, string> options)
        {
            Console.WriteLine($"Executing {nameof(BranchLookupTask)}...");

            var projects = await this.projectService.GetProjects(false);

            var sb = new StringBuilder();

            if (options != null && options.TryGetValue("projectId", out var projectId))
            {
                Guid.TryParse(projectId, out var g);
                projects = projects.Where(x => x.Id == g).ToArray();
            }
            else
            {
                var missingScs = await this.sourceData.GetProjectsMissingBranches();

                if (missingScs.Length > 0)
                {
                    sb.AppendLine($"Didnt Find: {missingScs.Length}");

                    var tempProjects = projects.Where(x => missingScs.Contains(x.Id)).ToArray();

                    Console.WriteLine($"Executing {nameof(BranchLookupTask)}.MissingBranches...");

                    if (tempProjects.Length == 0)
                    {
                        var scIds = projects.SelectMany(x => x.SourceControl.Select(y => y.Id));
                        projects = await this.GetProjectsNeedingUpdate(sb, projects, scIds.ToArray());
                        Console.WriteLine($"Executing {nameof(BranchLookupTask)}.ItemsNeedingUpdate...");
                    }
                    else
                    {
                        projects = tempProjects;
                    }
                }
                else
                {
                    var scIds = projects.SelectMany(x => x.SourceControl.Select(y => y.Id));
                    projects = await this.GetProjectsNeedingUpdate(sb, projects, scIds.ToArray());
                    Console.WriteLine($"Executing {nameof(BranchLookupTask)}.ItemsNeedingUpdate...");
                }

                projects = projects.OrderBy(x => Guid.NewGuid())
                                   .Take(Math.Max(Math.Min(projects.Length / 4, 8), 20))
                                   .ToArray();
            }

            var activeKeys = await this.keys.GetAllKeys();
            foreach (var project in projects)
            {
                sb.AppendLine(project.Id.ToString("B"));

                await this.GetProjectBranchesByIdWithoutCache(sb, project, activeKeys);
            }

            return sb.ToString();
        }

        private async Task<IProject[]> GetProjectsNeedingUpdate(StringBuilder sb, IProject[] projects, Guid[] scIds)
        {
            int[] counters = new[] {16, 8, 4, 2, 1};
            foreach (var counter in counters)
            {
                var expiration = DateTime.UtcNow.AddMinutes(0 - counter);
                sb.AppendLine($"Finding branches older than: {counter} {expiration}");

                var cacheBranches = await this.sourceData.GetBranches(scIds, expiration);
                if (cacheBranches.Count > 0)
                {
                    sb.AppendLine($"Found: {cacheBranches.Count}");

                    return projects.Where(x => x.SourceControl.Any(y => cacheBranches.ContainsKey(y.Id))).ToArray();
                }
            }

            return new IProject[0];
        }

        private async Task GetProjectBranchesByIdWithoutCache(StringBuilder sb, IProject project, IKey[] activeKeys)
        {
            var src = project.SourceControl ?? new ISourceControlInfo[0];

            foreach (var sourceControlInfo in src)
            {
                sb.AppendLine($"Looking up: {sourceControlInfo.Id}, {sourceControlInfo.SourceControlType}");
                var branches = await this.sourceControl.GetBranches(
                                                                    activeKeys,
                                                                    sourceControlInfo.SourceControlType,
                                                                    sourceControlInfo.SourceControlData);
                sb.AppendLine($"Found Branches: {branches.Length} {string.Join(",", branches.Select(x => x.Name + "-" + x.Hash))}");

                if (branches.Length > 0)
                {
                    var result = await this.sourceData.SetBranches(sourceControlInfo.Id, branches.Select(x => x.ConvertToInput()).ToArray());

                    if (result)
                    {
                        foreach (var branch in branches)
                        {
                            sb.AppendLine($"Searching for branch commits: {branch.Name}-{branch.Hash}");
                            await this.messageQueue.Push(new SourceLookupMessage
                                                         {
                                                             SourceControlId = sourceControlInfo.Id,
                                                             SourceControlType = sourceControlInfo.SourceControlType,
                                                             SourceControlData = sourceControlInfo.SourceControlData.ToDictionary(x => x.Key, x => x.Value),
                                                             BranchName = branch.Name,
                                                             BranchHash = branch.Hash
                                                         });
                        }
                    }
                }
            }
        }
    }
}