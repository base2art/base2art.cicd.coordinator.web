﻿namespace Base2art.CiCd.Coordinator.Web.Public.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.MessageQueue;
    using Converters;
    using Messages;
    using Models;
    using Repositories;

    public class WatcherTask
    {
        private readonly IJobStoreService jobStoreService;
        private readonly IProjectService projectService;

//        private readonly ProjectBranchBuilder projectBranchBuilder;
        private readonly IMessageQueue queue;
        private readonly bool shouldWatch;
        private readonly ISourceRepository sourceControlService;

        public WatcherTask(
                IProjectService projectService,
                ISourceRepository sourceControlService,
                IJobStoreService jobStoreService,
                IMessageQueue queue,
                string shouldWatch) //,
//            IBuildService buildService,
//            IPublisherService publisherService,
//            IServerReadRepository serverService,
//            IVersionService versionService)
        {
            this.queue = queue;
            this.shouldWatch = string.Equals(shouldWatch, "true", StringComparison.OrdinalIgnoreCase);
            this.projectService = projectService ?? throw new ArgumentNullException(nameof(projectService));
            this.sourceControlService = sourceControlService ?? throw new ArgumentNullException(nameof(sourceControlService));
            this.jobStoreService = jobStoreService ?? throw new ArgumentNullException(nameof(jobStoreService));
//            this.projectBranchBuilder = new ProjectBranchBuilder(
//                                                                 sourceControlService,
//                                                                 jobStoreService,
//                                                                 buildService ?? throw new ArgumentNullException(nameof(buildService)),
//                                                                 publisherService ?? throw new ArgumentNullException(nameof(publisherService)),
//                                                                 serverService ?? throw new ArgumentNullException(nameof(serverService)),
//                                                                 versionService);
        }

        public async Task ExecuteAsync()
        {
            if (!this.shouldWatch)
            {
                return;
            }

            var projects = await this.projectService.GetProjects(false);
            foreach (var project in projects)
            {
                var knownBranches = new Dictionary<Guid, IBranchLogData[]>();
                foreach (var sourceControl in project.SourceControl)
                {
                    var branches = await this.sourceControlService.GetBranches(sourceControl.Id);
                    knownBranches[sourceControl.Id] = branches;
                }

                var knownBranchNames = knownBranches.SelectMany(x => x.Value.Select(y => y.Name))
                                                    .Distinct(StringComparer.OrdinalIgnoreCase)
                                                    .ToArray();

                var comp = StringComparison.OrdinalIgnoreCase;
                var buildableBranches = knownBranchNames.Where(kb => knownBranches.All(x => x.Value.Any(y => string.Equals(y.Name, kb, comp))))
                                                        .Where(kb => !kb.StartsWith("legacy/"))
                                                        .ToList();

                var branchesToBuild = new Dictionary<Guid, IBranchLogData>();
                foreach (var branch in buildableBranches)
                {
                    foreach (var sourceControl in project.SourceControl)
                    {
                        branchesToBuild[sourceControl.Id] = knownBranches[sourceControl.Id].FirstOrDefault(x => x.Name == branch);
                    }
                }

                var canBuild = await this.CanBuild(branchesToBuild, project);

                if (canBuild)
                {
                    await this.queue.Push(new BuildProjectBranchMessage
                                          {
                                              ProjectId = project.Id,
                                              ProjectName = project.Name,
                                              UserName = "System:Trigger",
                                              BranchInfo = branchesToBuild.ToDictionary(x => x.Key, x => x.Value.ConvertToInput())
                                          });
//                    await this.projectBranchBuilder.Build(project, branchesToBuild);
                }
            }
        }

        private async Task<bool> CanBuild(IDictionary<Guid, IBranchLogData> branchesToBuild, IProject project)
        {
            foreach (var branch in branchesToBuild)
            {
                var builds = await this.jobStoreService.FindJobs(null,
                                                                 branch.Value.Hash,
                                                                 branch.Value.Name,
                                                                 project.Id,
                                                                 10,
                                                                 0,
                                                                 JobSort.DateCreatedDescending);

                if (builds.Length == 0)
                {
                    return true;
                }
            }

            return false;
        }
    }
}