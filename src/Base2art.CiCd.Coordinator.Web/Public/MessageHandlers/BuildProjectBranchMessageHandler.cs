namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.MessageQueue;
    using Messages;
    using Models;
    using PhaseHandlers;
    using PhaseHandlers.Logging;
    using Publisher;
    using Repositories;
    using Resources;
    using SourceControl;

    public class BuildProjectBranchMessageHandler : MessageHandlerBase<BuildProjectBranchMessage>
    {
        private readonly IBuildService buildService;
        private readonly IJobStoreService jobStoreService;
        private readonly IProjectService projectService;
        private readonly IPublishPhaseServiceFactory publishFactory;
        private readonly IServerReadRepository servers;
        private readonly IVersionService versions;
        private readonly ICheckoutPhaseServiceFactory checkoutPhaseServiceFactory;
        private readonly SourceControlService sourceControlService;
        private readonly IKeyRepository keys;

        public BuildProjectBranchMessageHandler(
            IProjectService projectService,
            IPublishPhaseServiceFactory publishFactory,
            IJobStoreService jobStoreService,
            IBuildService buildService,
            IServerReadRepository servers,
            IVersionService versions,
            ICheckoutPhaseServiceFactory checkoutPhaseServiceFactory,
            IKeyRepository keys)
        {
            this.projectService = projectService;
            this.publishFactory = publishFactory;
            this.sourceControlService = new SourceControlService(checkoutPhaseServiceFactory);
            this.jobStoreService = jobStoreService;
            this.buildService = buildService;
            this.servers = servers;
            this.versions = versions;
            this.checkoutPhaseServiceFactory = checkoutPhaseServiceFactory;
            this.keys = keys;
        }

        protected override async Task HandleMessage(BuildProjectBranchMessage message)
        {
            var branches = message.BranchInfo;
            var projectId = message.ProjectId;
            var projectName = message.ProjectName;
            var userName = message.UserName;
            // Console.WriteLine($"Building branch: {string.Join(",", branches.Select(x => x.Value))}");

            var project = await this.projectService.GetProjectById(projectId);

            var buildMessageStore = await this.jobStoreService.CreateJob(
                                                                         projectId,
                                                                         projectName,
                                                                         userName,
                                                                         branches);

            var loggableBranches = this.Clean(branches);

            var logEntry = new LogEntry(JobPhase.Creation);
            logEntry.Log(project);
            logEntry.Log(loggableBranches);

            await this.jobStoreService.AddMessage(buildMessageStore.Id, projectName, "Starting Build...", logEntry);

            this.BuildInternal(project, branches, buildMessageStore, loggableBranches)
                .RunAway(ex => this.LogError(ex, buildMessageStore, project, loggableBranches));
        }

        private IReadOnlyDictionary<Guid, ProjectBranch> Clean(Dictionary<Guid, SourceControlBranchData> branches)
        {
            var logBranches = new Dictionary<Guid, ProjectBranch>();
            foreach (var branch in branches)
            {
                logBranches[branch.Key] = new ProjectBranch
                                          {
                                              Hash = branch.Value.Hash,
                                              Name = branch.Value.Name,
                                              SourceControlId = branch.Key
                                          };
            }

            return logBranches;
        }

        private async Task LogError(
            Exception e,
            IJobInfo buildMessageStore,
            IProject project,
            IReadOnlyDictionary<Guid, ProjectBranch> branches)
        {
            var logEntry = new LogEntry(JobPhase.Completion);
            logEntry.Log(project);
            logEntry.Log(branches);
            logEntry.Log(e);

            await this.jobStoreService.AddMessage(
                                                  buildMessageStore.Id,
                                                  project.Name,
                                                  "Exception",
                                                  logEntry
                                                 );

            await this.jobStoreService.SetState(buildMessageStore.Id, JobState.CompletedFail);
        }

        private async Task BuildInternal(IProject project,
                                         IReadOnlyDictionary<Guid, SourceControlBranchData> theBranches,
                                         IJobInfo buildMessageStore,
                                         IReadOnlyDictionary<Guid, ProjectBranch> loggableBranches)
        {
            IKey[] activeKeys = await this.keys.GetAllKeys();
            var projectName = project.Name;
            await this.jobStoreService.SetState(buildMessageStore.Id, JobState.Working);

            var factories = new IHandlerFactory[]
                            {
                                new CheckoutHandlerFactory(this.checkoutPhaseServiceFactory),
                                new BuildHandlerFactory(this.versions, this.buildService),
                                new PublishHandlerFactory(this.publishFactory)
                            };

            var interchangeData = new BuildInterchangeData(theBranches);

            try
            {
                foreach (var factory in factories)
                {
                    var checkoutHandler = factory.Create(this.jobStoreService, this.sourceControlService, activeKeys, project, buildMessageStore, this.servers);

                    try
                    {
                        await checkoutHandler.LogStart();
                        var state = await checkoutHandler.Execute(interchangeData);
                        if (state != JobState.CompletedSuccess)
                        {
                            await checkoutHandler.LogFailure();
                            // break;
                            throw new BuildFailureException();
                        }

                        await checkoutHandler.LogEnd();
                    }
                    catch (BuildFailureException)
                    {
                        throw;
                    }
                    catch (Exception e)
                    {
                        await checkoutHandler.LogError(e);
                        throw;
                    }
                }

                await this.jobStoreService.SetState(buildMessageStore.Id, JobState.CompletedSuccess);

                var logEntry = new LogEntry(JobPhase.Completion);
                logEntry.Log(project);
                logEntry.Log(loggableBranches);

                await this.jobStoreService.AddMessage(
                                                      buildMessageStore.Id,
                                                      projectName,
                                                      "Success",
                                                      logEntry);
            }
            catch (BuildFailureException)
            {
                await this.jobStoreService.SetState(buildMessageStore.Id, JobState.CompletedFail);
            }
            catch (Exception e)
            {
                await this.LogError(e, buildMessageStore, project, loggableBranches);
            }
        }
    }
}

/*
 *
 *
 * 
            // var checkoutHandlerFactory = new CheckoutHandlerFactory();
            // var buildHandlerFactory = new BuildHandlerFactory(this.versions, this.buildService);
            // var publishHandlerFactory = new PublishHandlerFactory(this.publishFactory);

                var buildHandler = buildHandlerFactory.Create(this.jobStoreService, this.sourceControlService, activeKeys, project, buildMessageStore,
                                                              this.servers);

                try
                {
                    await buildHandler.LogStart();
                    // await buildHandler.Execute();
                    await buildHandler.LogEnd();
                }
                catch (Exception e)
                {
                    await buildHandler.LogFailure(e);
                    throw;
                }

                var publishHandler = publishHandlerFactory.Create(this.jobStoreService, this.sourceControlService, activeKeys, project, buildMessageStore,
                                                                  this.servers);

                try
                {
                    await publishHandler.LogStart();
                    // await publishHandler.Execute();
                    await publishHandler.LogEnd();
                }
                catch (Exception e)
                {
                    await publishHandler.LogFailure(e);
                    throw;
                }
 * 
 */