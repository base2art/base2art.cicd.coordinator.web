namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Cryptography;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Threading.Tasks;
    using Base2art.MessageQueue;
    using Builder;
    using CiCd.Servers;
    using Collections;
    using Messages;
    using Microsoft.IdentityModel.Tokens;
    using Models;
    using Publisher;
    using Repositories;
    using Serialization;
    using SourceControl;

    public class RefreshServersMessageHandler : MessageHandlerBase<RefreshServersMessage>
    {
        private readonly IServerReadRepository serverReader;
        private readonly IServerWriteRepository serverWriter;
        private readonly IBuildPhaseServiceFactory buildPhaseServiceFactory;
        private readonly ICheckoutPhaseServiceFactory checkoutPhaseServiceFactory;
        private readonly IPublishPhaseServiceFactory publishPhaseServiceFactory;
        private readonly IKeyRepository keyServiceRepository;
        private readonly SimpleJsonSerializer serializer;

        public RefreshServersMessageHandler(
            IServerReadRepository serverReader,
            IServerWriteRepository serverWriter,
            IBuildPhaseServiceFactory buildPhaseServiceFactory,
            ICheckoutPhaseServiceFactory checkoutPhaseServiceFactory,
            IPublishPhaseServiceFactory publishPhaseServiceFactory,
            IKeyRepository keyServiceRepository)
        {
            this.serverReader = serverReader;
            this.serverWriter = serverWriter;
            this.buildPhaseServiceFactory = buildPhaseServiceFactory;
            this.checkoutPhaseServiceFactory = checkoutPhaseServiceFactory;
            this.publishPhaseServiceFactory = publishPhaseServiceFactory;
            this.keyServiceRepository = keyServiceRepository;
            this.serializer = new SimpleJsonSerializer();
        }

        protected override async Task HandleMessage(RefreshServersMessage message)
        {
            var keys = await this.keyServiceRepository.GetAllKeys();

            try
            {
                await this.HandleServer(keys, ServerType.Compilation, async (id) => await this.buildPhaseServiceFactory.CreateClient(id.ToString()));
            }
            catch (Exception)
            {
            }

            try
            {
                await this.HandleServer(keys, ServerType.SourceControl, async (id) => await this.checkoutPhaseServiceFactory.CreateClient(id.ToString()));
            }
            catch (Exception)
            {
            }

            try
            {
                await this.HandleServer(keys, ServerType.Publish, async (id) => await this.publishPhaseServiceFactory.CreateClient(id.ToString()));
            }
            catch (Exception)
            {
                Console.WriteLine("Error Getting Publish Servers...");
            }
        }

        private async Task HandleServer(IKey[] keys, ServerType pluginType, Func<Uri, Task<ISupportedFeatureService>> provider)
        {
            var servers = await this.serverReader.GetServers(pluginType);
            foreach (var server in servers.Randomize())
            {
                bool isUp = true;

                var webClient = await provider(server.Url);

                SupportedFeature[] features = null;
                try
                {
                    features = await webClient.GetSupportedFeatures(Principals.CreatePrincipal(keys, server.AuthenticationKeyId, pluginType));
                }
                catch (Exception)
                {
                    isUp = false;
                }

                if (isUp)
                {
                    if (features != null)
                    {
                        await this.serverWriter.SetFeatures(
                                                            server.Id,
                                                            features.Select(x => new ServerFeature
                                                                                 {
                                                                                     Id = x.Id,
                                                                                     DefaultData = this.Convert(x.DefaultData),
                                                                                     SimpleName = x.Name,
                                                                                     FullName = x.FullName
                                                                                 })
                                                                    .ToArray());
                    }

                    if (!server.Enabled)
                    {
                        await this.serverWriter.SetStatus(server.Id, true);
                    }
                }
                else
                {
                    if (server.Enabled)
                    {
                        await this.serverWriter.SetStatus(server.Id, false);
                    }
                }
            }
        }

        private int Epoch(DateTime mins)
            => (int) (mins - new DateTime(1970, 1, 1)).TotalSeconds;

        private Dictionary<string, object> Convert(object obj)
        {
            if (obj == null)
            {
                return new Dictionary<string, object>();
            }

            return this.serializer.Deserialize<Dictionary<string, object>>(this.serializer.Serialize(obj));
        }
    }
}