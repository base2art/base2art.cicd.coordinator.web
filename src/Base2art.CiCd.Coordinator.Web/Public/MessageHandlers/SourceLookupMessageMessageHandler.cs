namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.MessageQueue;
    using Messages;
    using Models;
    using Repositories;
    using SourceControl;

    public class SourceLookupMessageMessageHandler : MessageHandlerBase<SourceLookupMessage>
    {
        private readonly SourceControlService sourceControl;
        private readonly ISourceRepository sourceData;
        private readonly IKeyRepository keys;

        public SourceLookupMessageMessageHandler(
            ISourceRepository sourceData,
            IKeyRepository keys,
            ICheckoutPhaseServiceFactory checkoutPhaseServiceFactory)
        {
            this.sourceControl = new SourceControlService(checkoutPhaseServiceFactory);
            this.sourceData = sourceData;
            this.keys = keys;
        }

        protected override async Task HandleMessage(SourceLookupMessage message)
        {
            Console.WriteLine(message);
            foreach (var item in message.SourceControlData)
            {
                Console.WriteLine($"{item.Key}: {item.Value}");
            }

            var activeKeys = await this.keys.GetAllKeys();
            var logs = await this.sourceControl.GetLogs(
                                                        activeKeys,
                                                        message.SourceControlType,
                                                        message.SourceControlData,
                                                        message.BranchName,
                                                        message.BranchHash);
            
            Console.WriteLine($"Found Logs.Count(): {logs?.Logs?.Length}");
            Console.WriteLine($"Found Logs.Authors(): {string.Join(", ", logs?.Logs?.Select(x => x?.Author) ?? new List<string>())}");

            await this.sourceData.SetCommits(
                                             message.SourceControlId,
                                             message.BranchName,
                                             message.BranchHash,
                                             logs?.Logs?.Select(y => new SourceControlBranchCommitLogData
                                                                     {
                                                                         Id = y.Id,
                                                                         Author = y.Author,
                                                                         Message = y.Message,
                                                                         When = y.When,
                                                                         FilesAdded = y.FilesAdded,
                                                                         FilesRemoved = y.FilesRemoved,
                                                                         FilesChanged = y.FilesChanged,
                                                                     })?.ToArray() ?? new SourceControlBranchCommitLogData[0]);
        }
    }
}