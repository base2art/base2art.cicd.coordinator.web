namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers.PhaseHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Collections;
    using Converters;
    using Logging;
    using Models;
    using Publisher;
    using Publisher.Resources;
    using Repositories;
    using Text;

    public class PublishHandler : HandlerBase
    {
        private readonly IServerReadRepository servers;
        private readonly IPublishPhaseServiceFactory publishFactory;

        public PublishHandler(
            IJobStoreService jobStoreService,
            IKey[] keys,
            IProject project,
            IJobInfo jobInfo,
            IServerReadRepository servers,
            IPublishPhaseServiceFactory publishFactory) : base(jobStoreService, keys, project, jobInfo, JobPhase.Publish)
        {
            this.servers = servers;
            this.publishFactory = publishFactory;
        }

        public override async Task<JobState> Execute(BuildInterchangeData interchangeData)
        {
            IReadOnlyDictionary<Guid, SourceControlBranchData> theBranches = interchangeData.Branches;
            IReadOnlyDictionary<Guid, IEnumerable<IFileData>> artifacts = interchangeData.Artifacts;
            var publishServers = await this.servers.GetServers(ServerType.Publish);
            var enabledPublishServers = publishServers.Where(x => x.Enabled).ToArray();

            var projectPublish = new Base2art.CiCd.Publisher.Resources.Project
                                 {
                                     Id = this.Project.Id,
                                     Name = this.Project.Name,
                                 };

            var types = this.Project.SourceControl.Select(x => (theBranches[x.Id], x));

            var branchData = types.Select(x => new BranchData
                                               {
                                                   Id = x.x.Id.ToString(),
                                                   Name = x.Item1.Name,
                                                   Hash = x.Item1.Hash,
                                                   TypeId = x.x.SourceControlType
                                               })
                                  .ToArray();

            var entry = new LogEntry(this.Phase);
            entry.Log(this.Project);
            entry.Log(enabledPublishServers);
            entry.Log(artifacts);

            await this.JobStoreService.AddMessage(
                                                  this.JobInfo.Id,
                                                  this.Project.Name,
                                                  "Publish Started...",
                                                  entry);

            var matches = GetMatches(enabledPublishServers, artifacts);

            var tasks = matches.Select(handler => this.GetTaskFor(projectPublish, branchData, handler));
            var items = await Task.WhenAll(tasks);

            while (items.Any(x => x.Item1.State.In(PublishPhaseState.Pending, PublishPhaseState.Claimed, PublishPhaseState.Working)))
            {
                await Task.Delay(TimeSpan.FromSeconds(30));

                var tasks1 = items.Select(handler => this.GetTaskForStatus(handler.Item1, handler.Item2));
                items = await Task.WhenAll(tasks1);
            }

            foreach (var (item, server) in items)
            {
                var logEntry = new LogEntry(this.Phase);
                logEntry.Log(this.Project);
                logEntry.Log(item);
                logEntry.Log(server);
                await this.JobStoreService.AddMessage(
                                                      this.JobInfo.Id,
                                                      this.Project.Name,
                                                      "Publish Pushed...",
                                                      logEntry);
            }

            return items.Any(x => x.Item1.State == PublishPhaseState.CompletedFail)
                       ? JobState.CompletedFail
                       : JobState.CompletedSuccess;
        }

        private async Task<(ProjectPublish, IServer)> GetTaskForStatus(ProjectPublish handler, IServer server)
        {
            if (!handler.State.In(PublishPhaseState.Pending, PublishPhaseState.Claimed, PublishPhaseState.Working))
            {
                return (handler, server);
            }

            var client = await this.publishFactory.CreateClient(server.Url?.ToString());
            // Console.WriteLine($"Client: {client}");
            // Console.WriteLine($"server: {server}");
            // Console.WriteLine($"handler: {handler}");
            // Console.WriteLine($"Keys: {this.Keys}");

            var claimsPrincipal = Principals.CreatePrincipal(this.Keys, server.AuthenticationKeyId, ServerType.Publish);
            var publishPhaseService = client;
            
            // Console.WriteLine($"{nameof(publishPhaseService)}: {publishPhaseService}");
            var data = await publishPhaseService.GetPublishStatus(handler.Id, claimsPrincipal);

            return (data, server);
        }

        private async Task<(ProjectPublish, IServer)> GetTaskFor(
            Project project,
            BranchData[] branchData,
            (Guid handlerId, IServer server, IFileData[] data) handler)
        {
            var client = await this.publishFactory.CreateClient(handler.server.Url.ToString());
            var @event = new PublishEvent
                         {
                             JobId = this.JobInfo.Id,
                             Project = project,
                             Branches = branchData,
                             Artifacts = handler.data.Select(x => x.ConvertToInput()).ToArray()
                         };

            var data = await client.StartPublish(
                                                 handler.handlerId,
                                                 @event,
                                                 Principals.CreatePrincipal(this.Keys, handler.server.AuthenticationKeyId, ServerType.Publish));

            var logEntry = new LogEntry(this.Phase);
            logEntry.Log(this.Project);
            logEntry.Log(data);
            logEntry.Log(handler.server);
            await this.JobStoreService.AddMessage(
                                                  this.JobInfo.Id,
                                                  this.Project.Name,
                                                  "Publish Pushed...",
                                                  logEntry);

            return (data, handler.server);
        }

        private bool IsMatch(IServerFeature feature, IFileData fileData)
        {
            if (!feature.DefaultData.ContainsKey("supportedExtension"))
            {
                return false;
            }

            var supportedExtension = feature.DefaultData["supportedExtension"] as string;
            if (string.IsNullOrWhiteSpace(supportedExtension))
            {
                // its a wildcard
                return false;
            }

            return supportedExtension.IsWildCardMatch(fileData.RelativeName, true) ||
                   supportedExtension.IsWildCardMatch(fileData.Name, true);
        }

        private (Guid handlerId, IServer server, IFileData[] data)[] GetMatches(
            IServer[] enabledPublishServers,
            IReadOnlyDictionary<Guid, IEnumerable<IFileData>> artifacts)
            => this.GetMatchesEnumerable(enabledPublishServers, artifacts).ToArray();

        private IEnumerable<(Guid handlerId, IServer server, IFileData[] data)> GetMatchesEnumerable(
            IServer[] enabledPublishServers,
            IReadOnlyDictionary<Guid, IEnumerable<IFileData>> artifacts)
        {
            foreach (var enabledPublishServer in enabledPublishServers)
            {
                foreach (var feature in enabledPublishServer.Features ?? new IServerFeature[0])
                {
                    var packages = artifacts.SelectMany(x => x.Value)
                                            .Where(x => IsMatch(feature, x))
                                            .ToArray();

                    if (packages.Length > 0)
                    {
                        yield return (feature.Id, enabledPublishServer, packages);
                    }
                }
            }
        }
    }
}

/*

        private class CustomComparer : IEqualityComparer<IServer>
        {
            public bool Equals(IServer x, IServer y)
            {
                if (ReferenceEquals(x, y) || ReferenceEquals(x, null) || ReferenceEquals(y, null))
                {
                    return false;
                }

                return x.GetType() == y.GetType() && x.Id.Equals(y.Id);
            }

            public int GetHashCode(IServer obj) => obj.Id.GetHashCode();
        }


 */