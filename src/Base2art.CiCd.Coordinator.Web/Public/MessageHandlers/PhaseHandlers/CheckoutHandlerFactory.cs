namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers.PhaseHandlers
{
    using Models;
    using Repositories;
    using SourceControl;

    public class CheckoutHandlerFactory : IHandlerFactory //<CheckoutHandler>
    {
        private readonly ICheckoutPhaseServiceFactory checkoutService;

        public CheckoutHandlerFactory(ICheckoutPhaseServiceFactory checkoutService) 
            => this.checkoutService = checkoutService;

        public CheckoutHandler Create(IJobStoreService jobStoreService,
                                      SourceControlService sourceControlService,
                                      IKey[] activeKeys,
                                      IProject project,
                                      IJobInfo buildMessageStore, IServerReadRepository servers)
            => new CheckoutHandler(jobStoreService, activeKeys, project, buildMessageStore, sourceControlService, checkoutService);

        IHandler IHandlerFactory.Create(
            IJobStoreService jobStoreService,
            SourceControlService sourceControlService,
            IKey[] activeKeys,
            IProject project,
            IJobInfo buildMessageStore,
            IServerReadRepository servers)
            => this.Create(jobStoreService, sourceControlService, activeKeys, project, buildMessageStore, servers);
    }
}