namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers.PhaseHandlers.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CiCd.Servers;
    using Models;
    using Publisher.Resources;
    using Resources;

    public class LogEntry
    {
        public JobPhase Phase { get; }
        private readonly Dictionary<string, object> message = new Dictionary<string, object>();

        public LogEntry(JobPhase phase) => this.Phase = phase;

        public Dictionary<string, object> Message()
            => this.message;

        public void Log(IProject project, bool includeFeatures = false)
        {
            this.message["projectId"] = project.Id;
            this.message["project"] = project.Name;

            if (includeFeatures)
            {
                this.message["requiredFeatures"] = project.RequiredFeatures;
            }
        }

        public void Log(IReadOnlyDictionary<Guid, SourceControlBranchData> branches)
        {
            this.message["branches"] = branches;
        }

        public void Log(IReadOnlyDictionary<Guid, ProjectBranch> branches)
        {
            this.message["branches"] = branches;
        }

        public void Log(IServer buildServer)
        {
            this.message[$"{this.Phase.ToString("G").ToLowerInvariant()}Server"] = Map(buildServer);
        }

        public void Log(IServerData buildServer)
        {
            var item = new Dictionary<string, object>
                       {
                           {"id", buildServer.Id},
                           {"name", buildServer.Name},
                           {"url", buildServer.Url},
                       };

            this.message[$"{this.Phase.ToString("G").ToLowerInvariant()}Server"] = item;
        }

        public void Log(ProjectPublish publish)
        {
            this.message["publishData"] = publish;
        }

        public void Log(Exception e)
        {
            this.message["messageType"] = e.GetType().FullName;
            this.message["message"] = e.Message;
            this.message["stackTrace"] = e.StackTrace;
        }

        public void Log(IBuild build)
        {
            this.message["buildId"] = build.Id;
            this.message["buildState"] = build.State.ToString("G");
        }

        public void LogVersion(string newVersion)
        {
            this.message["version"] = newVersion;
        }

        public void Log(IReadOnlyDictionary<Guid, IEnumerable<IFileData>> artifacts)
        {
            var artifactNames = artifacts.SelectMany(y => y.Value.Select(x => x.RelativeName)).ToArray();
            this.message["artifacts"] = artifactNames;
        }

        public void Log(SourceControlBranchData branch)
        {
            this.message["branchName"] = branch.Name;
            this.message["branchVersion"] = branch.Hash;
        }

        public void Log(ISourceControlInfo sourceControlInfo)
        {
            this.message["sourceControlType"] = sourceControlInfo.SourceControlType;
        }

        public void Log(IServer[] servers)
        {
            var loggableServers = servers.Select(buildServer => Map(buildServer)).ToArray();

            this.message["servers"] = loggableServers;
        }

        private static Dictionary<string, object> Map(IServer buildServer)
        {
            return new Dictionary<string, object>
                   {
                       {"id", buildServer.Id},
                       {"name", buildServer.Name},
                       {"url", buildServer.Url},
                       {"serverType", buildServer.ServerType},
                       {"enabled", buildServer.Enabled},
                       {"modified", buildServer.Modified},
                       {"features", buildServer.Features.Select(x => x.SimpleName).OrderBy(x => x).ToArray()},
                   };
        }
    }
}