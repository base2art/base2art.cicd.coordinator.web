namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers.PhaseHandlers
{
    using System;
    using System.Collections.Generic;
    using Models;
    using SourceControl.Resources;

    public class BuildInterchangeData
    {
        public BuildInterchangeData(IReadOnlyDictionary<Guid, SourceControlBranchData> theBranches)
            => this.Branches = theBranches;

        public IReadOnlyDictionary<Guid, SourceControlBranchData> Branches { get; }

        public Dictionary<Guid, IsolatedSourceData> ContentLookup { get; } = new Dictionary<Guid, IsolatedSourceData>();

        public Dictionary<Guid, SourceControlBranchData> SourceControlInfoLookup { get; } = new Dictionary<Guid, SourceControlBranchData>();

        public Dictionary<Guid, IBuildInstruction[]> BuildInstructionLookup { get; } = new Dictionary<Guid, IBuildInstruction[]>();
        
        public Dictionary<Guid, IEnumerable<IFileData>> Artifacts { get; } = new Dictionary<Guid, IEnumerable<IFileData>>();
    }
}