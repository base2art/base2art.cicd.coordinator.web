namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers.PhaseHandlers
{
    using System;
    using System.Threading.Tasks;
    using Models;

    public interface IHandler
    {
        Task LogStart();
        Task<JobState> Execute(BuildInterchangeData interchangeData);
        Task LogEnd();

        Task LogFailure();
        Task LogError(Exception exception);
    }
}