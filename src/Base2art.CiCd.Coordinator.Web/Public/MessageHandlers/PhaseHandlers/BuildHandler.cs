namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers.PhaseHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Collections;
    using Converters;
    using Logging;
    using Mappings;
    using Models;
    using Repositories;
    using SourceControl.Resources;

    public class BuildHandler : HandlerBase
    {
        private readonly IServerReadRepository servers;
        private readonly IVersionService versions;
        private readonly IBuildService buildService;

        public BuildHandler(
            IJobStoreService jobStoreService,
            IKey[] keys,
            IProject project,
            IJobInfo jobInfo,
            IServerReadRepository servers,
            IVersionService versions,
            IBuildService buildService)
            : base(jobStoreService, keys, project, jobInfo, JobPhase.Build)
        {
            this.servers = servers;
            this.versions = versions;
            this.buildService = buildService;
        }

        public override async Task<JobState> Execute(BuildInterchangeData interchangeData)
        {
            IReadOnlyDictionary<Guid, SourceControlBranchData> loggableBranches = interchangeData.Branches;
            Dictionary<Guid, SourceControlBranchData> sourceControlInfoLookup = interchangeData.SourceControlInfoLookup;
            Dictionary<Guid, IsolatedSourceData> contentLookup = interchangeData.ContentLookup;
            Dictionary<Guid, IBuildInstruction[]> buildInstructionLookup = interchangeData.BuildInstructionLookup;

            var buildServer = await this.servers.GetBuildServerByFeatures(ServerType.Compilation, this.Project.RequiredFeatures);

            if (buildServer == null || buildServer.Id == Guid.Empty)
            {
                var entry = new LogEntry(this.Phase);
                entry.Log(this.Project, true);
                entry.Log(loggableBranches);
                await this.JobStoreService.AddMessage(
                                                      this.JobInfo.Id,
                                                      this.Project.Name,
                                                      "Cannot Build (No Server Found)",
                                                      entry);
                // new Dictionary<string, object>
                // {
                //     // {"projectId", this.Project.Id},
                //     // {"project", this.Project.Name},
                //     {"branches", loggableBranches},
                // });
                await this.JobStoreService.SetState(this.JobInfo.Id, JobState.CompletedFail);
                return JobState.CompletedFail;
                //, new Dictionary<Guid, IEnumerable<IFileData>>());
            }

            var logEntry = new LogEntry(this.Phase);
            logEntry.Log(this.Project);
            logEntry.Log(loggableBranches);
            logEntry.Log(buildServer);

            await this.JobStoreService.AddMessage(
                                                  this.JobInfo.Id,
                                                  this.Project.Name,
                                                  "Building...",
                                                  logEntry);

            var newVersion = await this.versions.GetNextVersion(
                                                                this.Project.Id,
                                                                this.JobInfo.Id,
                                                                this.Project.Versioning.MapToConcrete(),
                                                                sourceControlInfoLookup);

            var build = await this.buildService.StartBuild(
                                                           buildServer.Id,
                                                           newVersion,
                                                           buildInstructionLookup.ConvertToInput(),
                                                           contentLookup.ToDictionary(x => x.Key, x => x.Value.Content),
                                                           sourceControlInfoLookup);

            var logEntryRunning = new LogEntry(this.Phase);
            logEntryRunning.Log(this.Project);
            logEntryRunning.Log(loggableBranches);
            logEntryRunning.Log(buildServer);
            logEntryRunning.Log(build);
            logEntryRunning.LogVersion(newVersion);
            await this.JobStoreService.AddMessage(
                                                  this.JobInfo.Id,
                                                  this.Project.Name,
                                                  "Build Running...",
                                                  logEntry);

            await this.JobStoreService.SetBuildInfo(this.JobInfo.Id, build.ServerId, build.Id);

            while (build.State.In(BuildState.Pending, BuildState.Claimed, BuildState.Working))
            {
                await Task.Delay(TimeSpan.FromSeconds(30));
                build = await this.buildService.GetBuildStatus(build.ServerId,
                                                               build.Id);
            }

            var artifacts = build.State == BuildState.CompletedSuccess
                                ? await this.buildService.GetArtifacts(
                                                                       buildServer.Id,
                                                                       build.Id,
                                                                       this.Project.SourceControl.Select(x => x.Id).ToArray())
                                : new Dictionary<Guid, IEnumerable<IFileData>>();

            var logEntryCompleted = new LogEntry(this.Phase);
            logEntryCompleted.Log(this.Project);
            logEntryCompleted.Log(loggableBranches);
            logEntryCompleted.Log(build);
            logEntryCompleted.Log(buildServer);
            logEntryCompleted.Log(artifacts);

            await this.JobStoreService.AddMessage(
                                                  this.JobInfo.Id,
                                                  this.Project.Name,
                                                  "Build Completed...",
                                                  logEntry);

            await this.buildService.RemoveBuild(buildServer.Id, build.Id);

            foreach (var pair in artifacts)
            {
                interchangeData.Artifacts.Add(pair.Key, pair.Value);
            }

            return Map(build.State);
        }

        private JobState Map(BuildState buildState)
        {
            switch (buildState)
            {
                case BuildState.Unknown:
                    return JobState.Unknown;
                case BuildState.Pending:
                    return JobState.Pending;
                case BuildState.Claimed:
                    return JobState.Claimed;
                case BuildState.Working:
                    return JobState.Working;
                case BuildState.CompletedSuccess:
                    return JobState.CompletedSuccess;
                case BuildState.CompletedFail:
                    return JobState.CompletedFail;
                default:
                    throw new ArgumentOutOfRangeException(nameof(buildState), buildState, null);
            }
        }
    }
}