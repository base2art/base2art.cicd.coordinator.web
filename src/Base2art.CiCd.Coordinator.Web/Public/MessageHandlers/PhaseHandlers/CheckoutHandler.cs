namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers.PhaseHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Collections;
    using Logging;
    using Models;
    using SourceControl;
    using SourceControl.Resources;

    public class CheckoutHandler : HandlerBase
    {
        private readonly SourceControlService sourceControlService;
        private readonly ICheckoutPhaseServiceFactory checkoutPhaseServiceFactory;

        public CheckoutHandler(
            IJobStoreService jobStoreService,
            IKey[] keys,
            IProject project,
            IJobInfo jobInfo,
            SourceControlService sourceControlService,
            ICheckoutPhaseServiceFactory checkoutPhaseServiceFactory)
            : base(jobStoreService, keys, project, jobInfo, JobPhase.Checkout)
        {
            this.sourceControlService = sourceControlService;
            this.checkoutPhaseServiceFactory = checkoutPhaseServiceFactory;
        }

        public override async Task<JobState> Execute(BuildInterchangeData interchangeData)
        {
            IReadOnlyDictionary<Guid, SourceControlBranchData> theBranches = interchangeData.Branches;
            Dictionary<Guid, SourceControlBranchData> sourceControlInfoLookup = interchangeData.SourceControlInfoLookup;
            Dictionary<Guid, IsolatedSourceData> contentLookup = interchangeData.ContentLookup;
            Dictionary<Guid, IBuildInstruction[]> buildInstructionLookup = interchangeData.BuildInstructionLookup;
            foreach (var sourceControlInfo in this.Project.SourceControl)
            {
                var branch = theBranches[sourceControlInfo.Id];

                var clientWrapper = await this.checkoutPhaseServiceFactory.CreateClient(sourceControlInfo.SourceControlType);

                var server = clientWrapper.CurrentServer;

                var logEntry = new LogEntry(this.Phase);
                logEntry.Log(this.Project);
                logEntry.Log(branch);
                logEntry.Log(server);
                logEntry.Log(sourceControlInfo);

                await this.JobStoreService.AddMessage(
                                                      this.JobInfo.Id,
                                                      this.Project.Name,
                                                      "Checking out...",
                                                      logEntry);

                var checkout = await this.sourceControlService.StartCheckout(
                                                                             clientWrapper.CurrentServer,
                                                                             clientWrapper.Service,
                                                                             this.Keys,
                                                                             sourceControlInfo.SourceControlData,
                                                                             branch.Name);

                sourceControlInfoLookup[sourceControlInfo.Id] = branch;

                while (checkout.State.In(IsolatedCheckoutState.Pending, IsolatedCheckoutState.Claimed, IsolatedCheckoutState.Working))
                {
                    await Task.Delay(TimeSpan.FromSeconds(30));
                    checkout = await this.sourceControlService.GetCheckoutStatus(this.Keys, server, checkout.Id);
                }

                var isolatedSourceData = await this.sourceControlService.GetSource(this.Keys, server, checkout.Id);
                contentLookup[sourceControlInfo.Id] = isolatedSourceData;

                await this.sourceControlService.RemoveSource(this.Keys, server, checkout.Id);

                buildInstructionLookup[sourceControlInfo.Id] = sourceControlInfo.BuildInstructions;
            }

            return JobState.CompletedSuccess;
        }
    }
}