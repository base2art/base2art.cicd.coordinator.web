namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers.PhaseHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CiCd.Servers;
    using Logging;
    using Models;

    public abstract class HandlerBase : IHandler
    {
        protected HandlerBase(IJobStoreService jobStoreService, IKey[] keys, IProject project, IJobInfo jobInfo, JobPhase phase)
        {
            this.JobStoreService = jobStoreService;
            this.Keys = keys;
            this.Project = project;
            this.JobInfo = jobInfo;
            this.Phase = phase;
        }

        protected JobPhase Phase { get; }

        protected IJobStoreService JobStoreService { get; }

        protected IJobInfo JobInfo { get; }

        protected IProject Project { get; }

        protected IKey[] Keys { get; }

        public Task LogStart() => Task.CompletedTask;

        public abstract Task<JobState> Execute(BuildInterchangeData interchangeData);

        public async Task LogEnd()
        {
            var logEntry = new LogEntry(this.Phase);
            logEntry.Log(this.Project);

            await this.JobStoreService.AddMessage(
                                                  this.JobInfo.Id,
                                                  this.Project.Name,
                                                  "Success",
                                                  logEntry);
        }

        public async Task LogFailure()
        {
            var logEntry = new LogEntry(this.Phase);
            logEntry.Log(this.Project);

            await this.JobStoreService.AddMessage(
                                                  this.JobInfo.Id,
                                                  this.Project.Name,
                                                  "Task Failure",
                                                  logEntry);
        }

        public async Task LogError(Exception e)
        {
            var logEntry = new LogEntry(this.Phase);
            logEntry.Log(this.Project);
            logEntry.Log(e);

            await this.JobStoreService.AddMessage(
                                                  this.JobInfo.Id,
                                                  this.Project.Name,
                                                  "Exception",
                                                  logEntry);
        }
    }
}

// protected Dictionary<string, object> LoggableServer(IServer buildServer) =>
//     new Dictionary<string, object>
//     {
//         {"id", buildServer.Id},
//         {"name", buildServer.Name},
//         {"url", buildServer.Url},
//         {"serverType", buildServer.ServerType},
//         {"enabled", buildServer.Enabled},
//         {"modified", buildServer.Modified},
//         {"features", buildServer.Features.Select(x => x.SimpleName).ToArray()},
//     };
//
// protected Dictionary<string, object> LoggableServer(IServerData buildServer) =>
//     new Dictionary<string, object>
//     {
//         {"id", buildServer.Id},
//         {"name", buildServer.Name},
//         {"url", buildServer.Url},
//     };