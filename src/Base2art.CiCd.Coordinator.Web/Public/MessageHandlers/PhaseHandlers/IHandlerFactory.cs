namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers.PhaseHandlers
{
    using Models;
    using Repositories;

    // public interface IHandlerFactory<out T> : IHandlerFactory
    //     where T : IHandler
    // {
    //     new T Create(IJobStoreService jobStoreService,
    //                  SourceControlService sourceControlService,
    //                  IKey[] activeKeys,
    //                  IProject project,
    //                  IJobInfo buildMessageStore, IServerReadRepository servers);
    // }

    public interface IHandlerFactory
    {
        IHandler Create(IJobStoreService jobStoreService,
                        SourceControlService sourceControlService,
                        IKey[] activeKeys,
                        IProject project,
                        IJobInfo buildMessageStore, IServerReadRepository servers);
    }
}