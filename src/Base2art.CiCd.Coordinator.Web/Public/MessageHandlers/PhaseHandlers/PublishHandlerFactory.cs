namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers.PhaseHandlers
{
    using Models;
    using Publisher;
    using Repositories;

    public class PublishHandlerFactory : IHandlerFactory // <PublishHandler>
    {
        private readonly IPublishPhaseServiceFactory publisher;

        public PublishHandlerFactory(IPublishPhaseServiceFactory publisher) => this.publisher = publisher;

        public PublishHandler Create(IJobStoreService jobStoreService,
                                     SourceControlService sourceControlService,
                                     IKey[] activeKeys,
                                     IProject project,
                                     IJobInfo buildMessageStore, IServerReadRepository servers)
            => new PublishHandler(jobStoreService, activeKeys, project, buildMessageStore, servers, this.publisher);

        IHandler IHandlerFactory.Create(
            IJobStoreService jobStoreService,
            SourceControlService sourceControlService,
            IKey[] activeKeys,
            IProject project,
            IJobInfo buildMessageStore,
            IServerReadRepository servers)
            => this.Create(jobStoreService, sourceControlService, activeKeys, project, buildMessageStore, servers);
    }
}