namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers.PhaseHandlers
{
    using Models;
    using Repositories;

    public class BuildHandlerFactory : IHandlerFactory
    {
        private readonly IVersionService versions;
        private readonly IBuildService buildService;

        public BuildHandlerFactory(IVersionService versions, IBuildService buildService)
        {
            this.versions = versions;
            this.buildService = buildService;
        }

        public BuildHandler Create(
            IJobStoreService jobStoreService,
            SourceControlService sourceControlService,
            IKey[] activeKeys,
            IProject project,
            IJobInfo buildMessageStore,
            IServerReadRepository servers)
            => new BuildHandler(jobStoreService, activeKeys, project, buildMessageStore, servers, this.versions, this.buildService);

        IHandler IHandlerFactory.Create(
            IJobStoreService jobStoreService,
            SourceControlService sourceControlService,
            IKey[] activeKeys,
            IProject project,
            IJobInfo buildMessageStore,
            IServerReadRepository servers)
            => this.Create(jobStoreService, sourceControlService, activeKeys, project, buildMessageStore, servers);
    }
}