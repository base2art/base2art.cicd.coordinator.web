namespace Base2art.CiCd.Coordinator.Web.Public.MessageHandlers
{
    using System;
    using System.IO;
    using System.Net;
    using System.Threading.Tasks;
    using Base2art.MessageQueue;
    using Messages;
    using Serialization;

    public class WebServiceJobStoreMessageHandler : MessageHandlerBase<JobStoreMessage>
    {
        private readonly string url;
        private readonly IJsonSerializer mySerializer;

        public WebServiceJobStoreMessageHandler(string url)
        {
            this.url = url;
            this.mySerializer = new CamelCasingIndentedSimpleJsonSerializer();
        }

        protected override async Task HandleMessage(JobStoreMessage message)
        {
            if (string.IsNullOrWhiteSpace(this.url))
            {
                return;
            }

            var httpWebRequest = WebRequest.CreateHttp(this.url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            try
            {
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var content = this.mySerializer.Serialize(message);
                    await streamWriter.WriteAsync(content);
                    await streamWriter.FlushAsync();
                }

                using (var httpResponse = await httpWebRequest.GetResponseAsync())
                {
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        streamReader.ReadToEnd();
                    }
                }
            }
            catch (Exception)
            {
            }
        }
    }
}