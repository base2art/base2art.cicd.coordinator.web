namespace Base2art.CiCd.Coordinator.Web.Public.Validators
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using FluentValidation;
    using Repositories;
    using Resources;

    public class ServerVerifier : Base2art.Web.ObjectQualityManagement.VerifierBase<Server>
    {
        private readonly IServerReadRepository readRepository;

        public ServerVerifier(IServerReadRepository readRepository)
        {
            this.readRepository = readRepository;
        }

        protected override void Configure()
        {
            this.Backing.RuleFor(x => x.Name).MustAsync(this.BeUnique).WithMessage("Server Name must be unique.");
            this.Backing.RuleFor(x => x.Url).NotEmpty();
        }

        private async Task<bool> BeUnique(Server s, string arg, CancellationToken arg3)
        {
            var servers = await this.readRepository.GetServers(null);
            var server = servers.FirstOrDefault(x => x.Id != s.Id && x.Name == arg);

            return server == null;
        }
    }
}