namespace Base2art.CiCd.Coordinator.Web.Public.Validators
{
    using System;
    using System.Threading.Tasks;
    using Base2art.Web.ObjectQualityManagement;
    using Base2art.Web.ObjectQualityManagement.Models;

    public static class QualityManagement
    {
        public static async Task<(ErrorField[], T)> Run<T>(Func<Task<T>> action)
        {
            try
            {
                var result = await action();
                return (new ErrorField[0], result);
            }
            catch (QualityManagementException e)
            {
                return (e.Errors, default);
            }
        }

//        public static async Task<(bool isValid, ErrorField[] errors)> IsValidateAndVerify<T>(this IQualityManagementLookup lookup, T data)
//        {
//            var (isValid, errors) = lookup.IsValidate<T>(data);
//
//            if (!isValid)
//            {
//                return (false, errors);
//            }
//
//            return await lookup.IsVerify<T>(data);
//        }
//
//        public static (bool isValid, ErrorField[] errors) IsValidate<T>(this IQualityManagementLookup lookup, T data)
//        {
//            var validators = lookup.GetValidators<T>();
//
//            var resultOf = validators.Select(x => ValidateItem(x, data)).ToArray();
//
//            if (!resultOf.All(x => x.Item1))
//            {
//                return (false, resultOf.SelectMany(x => x.Item2).ToArray());
//            }
//
//            return (true, new ErrorField[0]);
//        }
//
//        public static async Task<(bool isValid, ErrorField[] errors)> IsVerify<T>(this IQualityManagementLookup lookup, T data)
//        {
//            var validators = lookup.GetVerifiers<T>();
//
//            var resultOf = await Task.WhenAll(validators.Select(x => VerifyItem(x, data)).ToArray());
//
//            if (!resultOf.All(x => x.Item1))
//            {
//                return (true, resultOf.SelectMany(x => x.Item2).ToArray());
//            }
//
//            return (true, new ErrorField[0]);
//        }
//
//        private static async Task<(bool, ErrorField[])> VerifyItem<T>(IVerifier<T> verifier, T data)
//        {
//            return MapToResult<T>(await verifier.Verify(data));
//        }
//
//        private static (bool, ErrorField[]) ValidateItem<T>(IValidator<T> validator, T data)
//        {
//            return MapToResult<T>(validator.Validate(data));
//        }
//
//        private static (bool, ErrorField[]) MapToResult<T>(IQualityManagementResult result)
//        {
//            if (result == null)
//            {
//                return (true, new ErrorField[0]);
//            }
//
//            return !result.IsValid
//                       ? (false, (result.Errors ?? new IQualityManagementError[0]).Where(x => x != null).Select(Map).ToArray())
//                       : (true, new ErrorField[0]);
//        }
//
//        private static ErrorField Map(IQualityManagementError x)
//            => new ErrorField {Code = x.ErrorCode, Message = x.ErrorMessage, Path = x.PropertyName};
    }
}