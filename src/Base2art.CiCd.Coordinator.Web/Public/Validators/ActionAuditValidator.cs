namespace Base2art.CiCd.Coordinator.Web.Public.Validators
{
    using System.Security.Claims;
    using Base2art.Web.Exceptions;
    using FluentValidation;
    using Resources;

    public class ActionAuditValidator : Base2art.Web.ObjectQualityManagement.ValidatorBase<ActionAudit>
    {
        protected override void Configure()
        {
            this.Backing.RuleFor(x => x.Principal).NotNull()
                .OnAnyFailure(request => throw new NotAuthenticatedException());
            this.Backing.RuleFor(x => x.Principal).Must(BeAuthenticated)
                .OnAnyFailure(request => throw new NotAuthenticatedException());
            
//            this.Backing.RuleFor(x => x.Principal.Identity.IsAuthenticated).Equal(true);
            this.Backing.RuleFor(x => x.Type).NotNull();
            this.Backing.RuleFor(x => x.Type.Namespace).Must(StartWithAssemblyName);
//            this.Backing.RuleFor(x => x.OpType).NotNull();
        }

        private bool BeAuthenticated(ClaimsPrincipal arg) => (arg?.Identity?.IsAuthenticated).GetValueOrDefault();

        private bool StartWithAssemblyName(string arg)
            => arg.StartsWith(typeof(ActionAudit).Namespace) || arg.StartsWith(typeof(Server).Namespace);
    }
}