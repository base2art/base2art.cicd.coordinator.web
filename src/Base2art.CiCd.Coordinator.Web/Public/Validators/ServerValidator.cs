namespace Base2art.CiCd.Coordinator.Web.Public.Validators
{
    using FluentValidation;
    using Resources;

    public class ServerValidator : Base2art.Web.ObjectQualityManagement.ValidatorBase<Server>
    {
        protected override void Configure()
        {
            this.Backing.RuleFor(x => x.Name).NotEmpty();
            this.Backing.RuleFor(x => x.Url).NotEmpty();
        }
    }
}