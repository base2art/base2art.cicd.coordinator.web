namespace Base2art.CiCd.Coordinator.Web.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.MessageQueue;
    using Converters;
    using Mappings;
    using Messages;
    using Models;
    using Repositories;
    using Resources;
    using SourceControl;
    using SourceControl.Resources;

    public class JobService
    {
        private readonly IBuildService buildService;
        private readonly ICompanyService companyService;
        private readonly IJobStoreService jobStoreService;

        private readonly IPrincipalService principals;

        private readonly IMessageQueue messageQueue;
        private readonly IProjectService projectService;
        private readonly IKeyRepository keys;
        private readonly SourceControlService sourceControlService;

        public JobService(
            IPrincipalService principals,
            IMessageQueue messageQueue,
            IProjectService projectService,
            IKeyRepository keys,
            IJobStoreService jobStoreService,
            IBuildService buildService,
            ICheckoutPhaseServiceFactory checkoutPhaseServiceFactory,
            ICompanyService companyService)
        {
            this.principals = principals;
            this.messageQueue = messageQueue;
            this.projectService = projectService ?? throw new ArgumentNullException(nameof(projectService));
            this.keys = keys;
            this.sourceControlService = new SourceControlService(checkoutPhaseServiceFactory);
            this.jobStoreService = jobStoreService ?? throw new ArgumentNullException(nameof(jobStoreService));
            this.buildService = buildService;
            this.companyService = companyService;
        }

        public async Task<JobInfo[]> FindJobs(ClaimsPrincipal principal, JobSearchCriteria searchData, Pagination paginationData, JobSort sort)
        {
            var items = await this.jobStoreService.FindJobs(
                                                            searchData.State,
                                                            searchData.BranchHash,
                                                            searchData.BranchName,
                                                            searchData.ProjectId,
                                                            paginationData.PageSize,
                                                            paginationData.PageIndex,
                                                            sort);
            return items.Select(x => x.MapToConcrete()).ToArray();
        }

        public async Task<Job> GetJob(ClaimsPrincipal principal, Guid jobId)
        {
            principals.RequireAuthenticated(principal);
            var jobInfo = await this.jobStoreService.GetJobInfo(jobId);

            var messages = await this.jobStoreService.GetJobMessages(jobId);

            IBuildLog build = null;
            if (jobInfo.BuildServer.HasValue)
            {
                build = await this.buildService.GetBuildLogs(jobInfo.BuildServer.GetValueOrDefault(),
                                                             jobInfo.Build.GetValueOrDefault());
            }

            var job = jobInfo.MapToConcrete(build, messages, this.companyService.EscapeDefaultCompanyName());

            return job;
        }

        public async Task<JobInfo> Build(ClaimsPrincipal principal, SimpleJob info)
        {
            Console.WriteLine($"Handling Build: {info.BranchName} Recieved");
            principals.RequireAuthenticated(principal);

            var project = await this.projectService.GetProjectById(info.ProjectId);

            if (project == null)
            {
                Console.WriteLine($"Handling Build: {info.BranchName} Project Not Found");
                throw new ArgumentNullException(nameof(Project), "Project For Id Not Found");
            }

            if (!project.IsActive)
            {
                Console.WriteLine($"Handling Build: {info.BranchName} Project Not Active");
                throw new ArgumentNullException(nameof(Project), "Project Not Active");
            }

            var activeKeys = await this.keys.GetAllKeys();

            var has = new Dictionary<Guid, IsolatedBranchData>();
            foreach (var sourceControl in project.SourceControl)
            {
                var branches = await this.sourceControlService
                                         .GetBranches(activeKeys, sourceControl.SourceControlType, sourceControl.SourceControlData);

                var branch = branches
                    .FirstOrDefault(x => string.Equals(x.Name, info.BranchName, StringComparison.OrdinalIgnoreCase));

                if (branch == null)
                {
                    throw new ArgumentOutOfRangeException($"No branch exists with name: `{info.BranchName}`");
                }

                has[sourceControl.Id] = branch;
            }

            Console.WriteLine($"Handling Build: {info.BranchName} Pushing To Queue");
            await this.messageQueue.Push(new BuildProjectBranchMessage
                                         {
                                             ProjectId = project.Id,
                                             ProjectName = project.Name,
                                             UserName = this.principals.GetUserName(principal),
                                             BranchInfo = has.ToDictionary(x => x.Key, x => x.Value.ConvertToInput())
                                         });

            Console.WriteLine($"Handling Build: {info.BranchName} returning");
            return new JobInfo
                   {
                       ProjectId = project.Id,
                       State = JobState.Pending,
                       OriginatingUser = this.principals.GetUserName(principal),
                       SourceCode = new Dictionary<Guid, Branch>(),
                       ProjectName = project.Name,
                       DateCreated = DateTime.UtcNow
                   };
        }
    }
}