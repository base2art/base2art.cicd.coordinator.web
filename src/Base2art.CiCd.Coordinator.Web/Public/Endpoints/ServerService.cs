namespace Base2art.CiCd.Coordinator.Web.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.MessageQueue;
    using Base2art.Web.ObjectQualityManagement;
    using Base2art.Web.ObjectQualityManagement.Specialized;
    using Mappings;
    using Messages;
    using Models;
    using Repositories;
    using Resources;

    public class ServerService
    {
        private readonly IMessageQueue queue;
        private readonly IPrincipalService principals;
        private readonly IServerReadRepository serverReader;
        private readonly IServerWriteRepository serverWriter;
        private readonly IQualityManagementLookup validation;

        public ServerService(
            IPrincipalService principals,
            IServerReadRepository serverReader,
            IServerWriteRepository serverWriter,
            IQualityManagementLookup validation,
            IMessageQueue queue)
        {
            this.principals = principals;
            this.serverReader = serverReader ?? throw new ArgumentNullException(nameof(serverReader));
            this.serverWriter = serverWriter ?? throw new ArgumentNullException(nameof(serverWriter));
            this.validation = validation;
            this.queue = queue ?? throw new ArgumentNullException(nameof(queue));
        }

        public async Task<Feature[]> GetAllServerFeatures(ClaimsPrincipal principal, ServerType serverType)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Feature), Operation.List));
            var servers = await this.serverReader.GetServers(serverType);
            return servers.SelectMany(x => x.Features)
                          .Distinct(new FeatureComparer())
                          .Select(x => x.MapToConcrete())
                          .ToArray();
        }

        public async Task<Server[]> GetAllServers(ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Server), Operation.List));
            var result = await this.serverReader.GetServers(null);
            return result.Select(x => x.MapToConcrete()).ToArray();
        }

        public async Task<Server[]> GetAllServersByType(ClaimsPrincipal principal, ServerType serverType)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Server), Operation.List));
            var result = await this.serverReader.GetServers(serverType);
            return result.Select(x => x.MapToConcrete()).ToArray();
        }

        public async Task<Server> AddServer(ClaimsPrincipal principal, ServerType serverType, Server server)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Server), Operation.Add));
            await this.validation.ValidateAndVerify(server);

            var item = await this.serverWriter.AddServer(serverType, server.Name, server.Url.ToString(), server.AuthenticationKeyId);

            await this.queue.Push(new RefreshServersMessage());

            return item.MapToConcrete();
        }

        public async Task<Server> UpdateServer(ClaimsPrincipal principal, Guid serverId, Server server)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Server), Operation.Add));
            await this.validation.ValidateAndVerifyForUpdate(serverId, server, x => x.Id);

            var item = await this.serverWriter.UpdateServer(server.Id, server.Name, server.Url.ToString(), server.AuthenticationKeyId);

            await this.queue.Push(new RefreshServersMessage());

            return item.MapToConcrete();
        }

        public async Task DeleteServer(ClaimsPrincipal principal, Guid id)
        {
            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Server), ItemOperation.Delete, id));
            await this.serverWriter.DeleteServer(id);
        }

        private class FeatureComparer : IEqualityComparer<IServerFeature>
        {
            public bool Equals(IServerFeature x, IServerFeature y) => x?.Id == y?.Id;

            public int GetHashCode(IServerFeature obj) => obj.Id.GetHashCode();
        }
    }
}