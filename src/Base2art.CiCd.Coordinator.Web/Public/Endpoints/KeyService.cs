namespace Base2art.CiCd.Coordinator.Web.Public.Endpoints
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Cryptography;
    using System.Threading.Tasks;
    using Base2art.Web.ObjectQualityManagement;
    using Mappings;
    using Repositories;
    using Resources;

    public class KeyService
    {
        private readonly IPrincipalService principals;
        private readonly IKeyRepository jobStoreService;
        private readonly IQualityManagementLookup validation;

        public KeyService(IPrincipalService principals, IKeyRepository jobStoreService, IQualityManagementLookup validation)
        {
            this.principals = principals;
            this.jobStoreService = jobStoreService;
            this.validation = validation;
        }

        public async Task<Key[]> FindKeys(ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Key), Operation.List));

            var items = await this.jobStoreService.GetAllKeys();
            return items.Select(x => x.MapToConcrete(false)).ToArray();
        }

        public async Task<Key> GetKey(ClaimsPrincipal principal, Guid id)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Key), Operation.List));

            var items = await this.jobStoreService.GetKeyById(id);
            const bool includePrivatePart = false;
            return items.MapToConcrete(includePrivatePart);
        }

        public async Task<Key> AddKey(ClaimsPrincipal principal, Key key)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Key), Operation.Add));
            await this.validation.ValidateAndVerify(key);

            var rsaParameters = new RSAParameters();

            rsaParameters.D = key.D;
            rsaParameters.Exponent = key.Exponent;
            rsaParameters.Modulus = key.Modulus;
            rsaParameters.P = key.P;
            rsaParameters.Q = key.Q;
            rsaParameters.DP = key.DP;
            rsaParameters.DQ = key.DQ;
            rsaParameters.InverseQ = key.InverseQ;

            var item = await this.jobStoreService.AddKey(key.Name, rsaParameters);

            return item.MapToConcrete(false);
        }

        public async Task DeleteKey(ClaimsPrincipal principal, Guid id)
        {
            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Key), ItemOperation.Delete, id));
            await this.jobStoreService.ArchiveKey(id);
        }
    }
}