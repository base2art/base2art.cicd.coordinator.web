namespace Base2art.CiCd.Coordinator.Web.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Cryptography.X509Certificates;
    using System.Threading.Tasks;
    using Base2art.Threading.Tasks;
    using Converters;
    using Mappings;
    using Models;
    using Repositories;
    using Resources;

    public class ProjectService
    {
        private readonly IPrincipalService principals;
        private readonly IProjectService projectService;
        private readonly ISourceRepository source;
        private readonly IServerReadRepository servers;
        private readonly IJobStoreRepository jobs;

        public ProjectService(
            IPrincipalService principals,
            IProjectService projectService,
            ISourceRepository source,
            IServerReadRepository servers,
            IJobStoreRepository jobs)
        {
            this.principals = principals;
            this.projectService = projectService;
            this.source = source;
            this.servers = servers;
            this.jobs = jobs;
        }

        public async Task<Project[]> GetProjects(ClaimsPrincipal principal, bool includeInactive = false)
        {
            var result = await this.projectService.GetProjects(includeInactive);
            var allFeatures = await this.servers.AllKnownFeatures();

            return result.Select(x => x.MapToConcrete(allFeatures)).ToArray();
        }

        public async Task<ProjectRequisition[]> GetProjectsWithBuildsAndBranches(ClaimsPrincipal principal, bool includeInactive = false)
        {
            var result = await this.projectService.GetProjects(includeInactive);

            var sourceControlIds = result.SelectMany(x => x.SourceControl.Select(y => y.Id)).ToArray();

            var branches = await this.source.GetBranches(sourceControlIds);

            var goodBranches = branches.ToDictionary(x => x.Key, x => x.Value.Select(y => y.MapToConcrete()).ToArray());

            var jobs = await this.jobs.FindLatestJobs(branches.SelectMany(x => x.Value).ToArray().ConvertToInput());

            var goodBuilds = jobs.Select(x => x.MapToConcrete()).ToArray();

            var allFeatures = await this.servers.AllKnownFeatures();

            bool Comp(string a, string b) => string.Equals(a, b, StringComparison.OrdinalIgnoreCase);

            bool GetMatched(JobInfo jobInfo, Project project, Branch branch)
            {
                if (jobInfo.ProjectId != project.Id)
                {
                    return false;
                }

                var targetBranches = jobInfo.SourceCode.Values;

                return targetBranches.Any(x => Comp(x.Name, branch.Name) && Comp(x.Hash, branch.Hash));
            }

            return result.Select(x => x.MapToConcrete(allFeatures))
                         .Select(x => new ProjectRequisition
                                      {
                                          ProjectData = x,
                                          KnownBranches = GetBranches(x, goodBranches)
                                                          .Select(y => new BranchRequisition
                                                                       {
                                                                           BranchData = y,
                                                                           BuildData = goodBuilds.FirstOrDefault(z => GetMatched(z, x, y))
                                                                       })
                                                          .ToArray()
                                      }).ToArray();
        }

        public async Task<Project> AddProject(ClaimsPrincipal principal, Project project)
        {
            principals.RequireAuthenticated(principal);
            var result = await this.projectService.SaveProject(
                                                               project.Id,
                                                               project.Name,
                                                               project?.Versioning?.Strategy,
                                                               project?.Versioning?.Data ?? new Dictionary<string, string>());

            return await this.GetProjectById(principal, result.Id);
        }

        public async Task<Project> AddProjectSourceControl(ClaimsPrincipal principal, Guid projectId, SourceControl sourceControl)
        {
            principals.RequireAuthenticated(principal);
            var sc = sourceControl;
            var scn = new SourceControlData
                      {
                          SourceControlType = sc.SourceControlType,
                          Instructions = sc.BuildInstructions.Select(x => new BuildInstructionData
                                                                          {
                                                                              ExecutableFile = x.ExecutableFile,
                                                                              ExecutableArguments = x.ExecutableArguments
                                                                          }).ToArray(),
                          SourceControlParameters = sc.SourceControlData.ToDictionary(x => x.Key, x =>
                          {
                              object obj = x.Value;
                              return obj;
                          })
                      };

            await this.projectService.AddSourceControl(projectId, scn);

            return await this.GetProjectById(principal, projectId);
        }

        public async Task<Project> SetProjectSourceControls(ClaimsPrincipal principal, Guid projectId, SourceControl[] sourceControls)
        {
            principals.RequireAuthenticated(principal);

            var currentProject = await this.GetProjectById(principal, projectId);

            var itemsToAdd = new List<SourceControl>();
            var itemsToUpdate = new List<SourceControl>();
            foreach (var currentSc in currentProject.SourceControl)
            {
                if (sourceControls.All(x => x.Id != currentSc.Id))
                {
                    await this.projectService.RemoveSourceControl(projectId, currentSc.Id);
                }
            }

            foreach (var currentSc in sourceControls)
            {
                if (currentProject.SourceControl.All(x => x.Id != currentSc.Id))
                {
                    itemsToAdd.Add(currentSc);
                }
                else if (currentProject.SourceControl.Any(x => x.Id == currentSc.Id))
                {
                    itemsToUpdate.Add(currentSc);
                }
                else
                {
                    throw new ArgumentOutOfRangeException(nameof(currentSc));
                }
            }

            (Guid id, SourceControlData data) Map(SourceControl sc)
            {
                var data = new SourceControlData
                           {
                               SourceControlType = sc.SourceControlType,
                               Instructions = sc.BuildInstructions.Select(x => new BuildInstructionData
                                                                               {
                                                                                   ExecutableFile = x.ExecutableFile,
                                                                                   ExecutableArguments = x.ExecutableArguments
                                                                               }).ToArray(),
                               SourceControlParameters = sc.SourceControlData.ToDictionary(x => x.Key, x =>
                               {
                                   object obj = x.Value;
                                   return obj;
                               })
                           };
                return (sc.Id, data);
            }

            var mappedToAdd = itemsToAdd.Select(Map);
            var mappedToUpdate = itemsToUpdate.Select(Map);
            foreach (var scn in mappedToAdd)
            {
                await this.projectService.AddSourceControl(projectId, scn.data);
            }

            foreach (var scn in mappedToUpdate)
            {
                await this.projectService.UpdateSourceControl(projectId, scn.id, scn.data);
            }

            return await this.GetProjectById(principal, projectId);
        }

        public async Task<Project> RemoveProjectSourceControl(ClaimsPrincipal principal, Guid projectId, Guid sourceControlId)
        {
            principals.RequireAuthenticated(principal);
            await this.projectService.RemoveSourceControl(projectId, sourceControlId);

            return await this.GetProjectById(principal, projectId);
        }

        public async Task<Project> AddProjectFeature(ClaimsPrincipal principal, Guid projectId, Feature feature)
        {
            principals.RequireAuthenticated(principal);
            await this.projectService.AddFeature(projectId, feature.Id);

            return await this.GetProjectById(principal, projectId);
        }

        public async Task<Project> SetProjectFeatures(ClaimsPrincipal principal, Guid projectId, Feature[] feature)
        {
            principals.RequireAuthenticated(principal);

            await this.projectService.SetProjectFeatures(projectId, feature.Select(x => x.Id).ToArray());

            return await this.GetProjectById(principal, projectId);
        }

        public async Task<Project> RemoveProjectFeature(ClaimsPrincipal principal, Guid projectId, Guid featureId)
        {
            principals.RequireAuthenticated(principal);
            await this.projectService.RemoveFeature(projectId, featureId);

            return await this.GetProjectById(principal, projectId);
        }

//        public async Task<Server[]> GetAllServersByProjectId(ClaimsPrincipal principal, Guid projectId)
//        {
//            principals.RequireAuthenticated(principal);
//            var proj = await this.projectService.GetProjectById(projectId);
////            await this.serverReader.GetServers(null);
//            var result = await this.servers.GetBuildServersByFeatures(null, );
//            return result.Select(x => x.MapToConcrete()).ToArray();
//        }

        public async Task<Server[]> GetAllServersByProjectIdByType(ClaimsPrincipal principal, Guid projectId, ServerType serverType)
        {
            principals.RequireAuthenticated(principal);
            var proj = await this.projectService.GetProjectById(projectId);

            var features = proj.RequiredFeatures;
            if (serverType == ServerType.SourceControl)
            {
                features = proj.SourceControl.Select(x => x.SourceControlType).ToArray();
            }

            var result = await this.servers.GetBuildServersByFeatures(serverType, features);
            return result.Select(x => x.MapToConcrete()).ToArray();
        }

        public async Task<Project> GetProjectById(ClaimsPrincipal principal, Guid projectId)
        {
            var result = await this.projectService.GetProjectById(projectId);
            var allFeatures = await this.servers.AllKnownFeatures();

            return result.MapToConcrete(allFeatures);
        }

        public async Task<ProjectBranch[]> GetProjectBranchesById(ClaimsPrincipal principal, Guid projectId)
        {
            var result = await this.projectService.GetProjectById(projectId);
            var abc = result.SourceControl.Select(x => x.Id);

            var all = await Task.WhenAll(abc.Select(id => this.source.GetBranches(id).Then().Return(y => Tuple.Create(id, y))));
            return all.Select(x => x.Item2.Select(y => new ProjectBranch
                                                       {
                                                           Hash = y.Hash,
                                                           Name = y.Name,
                                                           SourceControlId = x.Item1
                                                       }))
                      .SelectMany(x => x)
                      .ToArray();
        }

        public Task DeleteProject(ClaimsPrincipal principal, Guid projectId)
        {
            principals.RequireAuthenticated(principal);
            return this.projectService.DeactivateProject(projectId);
        }

        private Branch[] GetBranches(Project project, Dictionary<Guid, Branch[]> goodBranches)
        {
            List<Branch> branches = new List<Branch>();
            foreach (var sc in project.SourceControl)
            {
                if (goodBranches.ContainsKey(sc.Id))
                {
                    branches.AddRange(goodBranches[sc.Id]);
                }
            }

            return branches.ToArray();
        }
    }
}