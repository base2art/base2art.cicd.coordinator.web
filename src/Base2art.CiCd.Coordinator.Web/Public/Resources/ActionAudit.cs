namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    using System;
    using System.Security.Claims;

    public class ActionAudit
    {
        public ClaimsPrincipal Principal { get; }
        public Type Type { get; }
        public Operation OpType { get; }

        public ActionAudit(ClaimsPrincipal principal, Type type, Operation opType)
        {
            this.Principal = principal;
            this.Type = type;
            this.OpType = opType;
        }
    }

    public class ActionItemAudit
    {
        public ClaimsPrincipal Principal { get; }
        public Type Type { get; }
        public ItemOperation OpType { get; }
        public Guid? ObjectId { get; }

        public ActionItemAudit(ClaimsPrincipal principal, Type type, ItemOperation opType, Guid itemId)
        {
            this.ObjectId = itemId;
            this.Principal = principal;
            this.Type = type;
            this.OpType = opType;
        }
    }
}