﻿namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    public class File
    {
        public string RelativeName { get; set; }
        public string Name { get; set; }
        public byte[] Content { get; set; }
    }
}