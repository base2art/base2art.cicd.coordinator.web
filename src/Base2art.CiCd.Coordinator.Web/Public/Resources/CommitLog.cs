﻿namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    using System;

    public class CommitLog
    {
        public string Id { get; set; }
        public DateTime When { get; set; }
        public string Author { get; set; }
        public string Message { get; set; }
        public string[] FilesAdded { get; set; }
        public string[] FilesRemoved { get; set; }
        public string[] FilesChanged { get; set; }
    }
}