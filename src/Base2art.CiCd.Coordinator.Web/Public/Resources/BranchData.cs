﻿namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    public class Branch
    {
        public string Name { get; set; }
        public string Hash { get; set; }
        public CommitLog[] CommitLogs { get; set; }
    }
}