namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    using System;
    using Models;

    public class Server
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Uri Url { get; set; }
        public Feature[] Features { get; set; }

        public ServerType ServerType { get; set; }

        public bool Enabled { get; set; }
        public Guid? AuthenticationKeyId { get; set; }
    }
}