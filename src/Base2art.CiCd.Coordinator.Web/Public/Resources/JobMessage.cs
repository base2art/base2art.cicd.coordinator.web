﻿namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    using System.Collections.Generic;

    public class JobMessage
    {
        public string Phase { get; set; }
        public string Message { get; set; }
        public Dictionary<string, object> Data { get; set; }
        public string DataText { get; set; }
    }
}