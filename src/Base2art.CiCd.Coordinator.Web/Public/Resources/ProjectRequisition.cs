namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    public class ProjectRequisition
    {
        public Project ProjectData { get; set; }
        public BranchRequisition[] KnownBranches { get; set; }
    }
}