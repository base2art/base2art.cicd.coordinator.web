namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    using System;

    public class SimpleJob
    {
        public Guid ProjectId { get; set; }
        public string BranchName { get; set; }
    }
}