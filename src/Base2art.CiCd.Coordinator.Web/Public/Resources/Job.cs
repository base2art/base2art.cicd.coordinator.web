﻿namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    using System;
    using System.Collections.Generic;
    using Models;

    public class Job
    {
        public Guid Id { get; set; }

        public Guid ProjectId { get; set; }

        public string ProjectName { get; set; }

        public JobState State { get; set; }

        public Guid? BuildServer { get; set; }

        public Guid? Build { get; set; }

        public Dictionary<Guid, Branch> SourceCode { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateFinished { get; set; }

        public JobMessage[] Messages { get; set; }
        public string Output { get; set; }
        public string Error { get; set; }
        public string OriginatingUser { get; set; }
        public string Version { get; set; }
    }
}