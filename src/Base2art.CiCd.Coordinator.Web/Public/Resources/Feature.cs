namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    using System;
    using System.Collections.Generic;

    public class Feature
    {
        public string SimpleName { get; set; }
        public string FullName { get; set; }
        public Dictionary<string, object> DefaultData { get; set; }
        public Guid Id { get; set; }
    }
}