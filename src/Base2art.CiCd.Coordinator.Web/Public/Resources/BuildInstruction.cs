namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    public class BuildInstruction
    {
        public string ExecutableFile { get; set; }
        public string[] ExecutableArguments { get; set; }
    }
}