﻿namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    using System;
    using System.Collections.Generic;

    public class SourceControl
    {
        public Guid Id { get; set; }

        public Guid SourceControlType { get; set; }
        public Dictionary<string, string> SourceControlData { get; set; }

        public BuildInstruction[] BuildInstructions { get; set; }
    }
}