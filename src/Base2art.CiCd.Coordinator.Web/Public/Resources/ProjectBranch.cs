namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    using System;

    public class ProjectBranch
    {
        public Guid SourceControlId { get; set; }
        public string Name { get; set; }
        public string Hash { get; set; }
    }
}