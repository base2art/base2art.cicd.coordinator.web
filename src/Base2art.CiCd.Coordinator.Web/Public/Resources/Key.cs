namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    using System;

    public class Key
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        
        
        public byte[] D { get; set; }

        public byte[] DP { get; set; }

        public byte[] DQ { get; set; }

        public byte[] Exponent { get; set; }

        public byte[] InverseQ { get; set; }

        public byte[] Modulus { get; set; }

        public byte[] P { get; set; }

        public byte[] Q { get; set; }
    }
}