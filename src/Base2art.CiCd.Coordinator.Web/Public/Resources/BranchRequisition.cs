namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    public class BranchRequisition
    {
        public Branch BranchData { get; set; }
        
        public string LatestVersion { get; set; }
        
        public JobInfo BuildData { get; set; }
        
        
    }
}