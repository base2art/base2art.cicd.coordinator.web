﻿namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    public class Pagination
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

        public static Pagination FirstPage(int i) => new Pagination
                                                     {
                                                         PageIndex = 0,
                                                         PageSize = i <= 0 ? 10 : i
                                                     };
    }
}