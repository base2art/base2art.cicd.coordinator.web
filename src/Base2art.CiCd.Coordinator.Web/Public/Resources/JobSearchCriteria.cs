﻿namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    using System;
    using Models;

    public class JobSearchCriteria
    {
        public Guid? ProjectId { get; set; }

        public string BranchName { get; set; }

        public JobState? State { get; set; }

        public string BranchHash { get; set; }
    }
}