﻿namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    using System.Collections.Generic;
    using Models;

    public class Versioning
    {
        public VersioningStrategy Strategy { get; set; }
        public Dictionary<string, string> Data { get; set; }
    }
}