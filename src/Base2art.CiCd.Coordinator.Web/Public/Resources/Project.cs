﻿namespace Base2art.CiCd.Coordinator.Web.Public.Resources
{
    using System;

    public class Project
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public SourceControl[] SourceControl { get; set; }

        public Feature[] RequiredFeatures { get; set; }
        
        
        public Guid[] RequiredFeatureIds { get; set; }

        public Versioning Versioning { get; set; }

        public bool IsActive { get; set; }
    }
}