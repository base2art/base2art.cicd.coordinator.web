﻿namespace Base2art.CiCd.Coordinator.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Builder;
    using Builder.Resources;
    using Collections;
    using Models;
    using Repositories;

    public class BuildService : IBuildService
    {
        private readonly IBuildPhaseServiceFactory client;
        private readonly IKeyRepository keys;
        private readonly IServerReadRepository serverService;
        private readonly IBuildRepository storage;

        public BuildService(
            IBuildRepository storage,
            IServerReadRepository serverService,
            IBuildPhaseServiceFactory client,
            IKeyRepository keys)
        {
            this.storage = storage;
            this.serverService = serverService;
            this.client = client;
            this.keys = keys;
        }

        public async Task<IBuild> StartBuild(Guid buildServerId,
                                             string version,
                                             Dictionary<Guid, BuildInstructionData[]> buildInstructionLookup,
                                             Dictionary<Guid, byte[]> contentLookup,
                                             Dictionary<Guid, SourceControlBranchData> checkoutInfoLookup)
        {
            var build = new BuildModel();
            build.Id = Guid.NewGuid();
            build.ServerId = buildServerId;
            build.State = BuildState.Claimed;
            await this.storage.SetStatus(build.Id, build.State);

            var task = this.BuildSync(buildServerId, build.Id, version, buildInstructionLookup, contentLookup, checkoutInfoLookup);
            task.RunAway(ex => this.storage.SetStatus(build.Id, BuildState.CompletedFail));

            return build;
        }

        public async Task<IBuild> GetBuildStatus(Guid serverId, Guid id)
            => new BuildModel
               {
                   Id = id,
                   ServerId = serverId,
                   State = await this.storage.GetStatus(id)
               };

        public async Task<IReadOnlyDictionary<Guid, IEnumerable<IFileData>>> GetArtifacts(
            Guid serverId,
            Guid id,
            Guid[] sourceControlIds)
        {
            var server = await this.serverService.GetServer(serverId);

            var allKeys = await this.keys.GetAllKeys();
            ClaimsPrincipal serverPrincipal = Principals.CreatePrincipal(allKeys.ToArray(), server.AuthenticationKeyId, ServerType.Compilation);
            var result = await (await this.client.CreateClient(server.Url.ToString())).GetArtifacts(id, serverPrincipal);
            return result.ToDictionary(x => x.Key, y => y.Value.Select<Artifact, IFileData>(x => new FileDataImpl
                                                                                                 {
                                                                                                     Content = x.Content,
                                                                                                     RelativeName = x.RelativeName,
                                                                                                     Name = x.Name,
                                                                                                 }));
        }

        public async Task RemoveBuild(Guid serverId, Guid id)
        {
            var server = await this.serverService.GetServer(serverId);
            var allKeys = await this.keys.GetAllKeys();
            ClaimsPrincipal serverPrincipal = Principals.CreatePrincipal(allKeys.ToArray(), server.AuthenticationKeyId, ServerType.Compilation);
            await (await this.client.CreateClient(server.Url.ToString())).Remove(id, serverPrincipal);
        }

        public async Task<IBuildLog> GetBuildLogs(Guid serverId, Guid buildId)
        {
            if (buildId == Guid.Empty || serverId == Guid.Empty)
            {
                return new BuildLogModel
                       {
                           Error = string.Empty,
                           Output = string.Empty
                       };
            }

            IServer server =  await this.serverService.GetServer(serverId);

            if (server == null)
            {
                return new BuildLogModel
                       {
                           Error = $"Server Not found: {serverId}",
                           Output = $"Build Id: {buildId}"
                       };
            }

            var allKeys = await this.keys.GetAllKeys();
            ClaimsPrincipal serverPrincipal = Principals.CreatePrincipal(allKeys.ToArray(), server.AuthenticationKeyId, ServerType.Compilation);
            var buildPhaseService = await this.client.CreateClient(server.Url.ToString());
            ProjectBuild build = null;

            try
            {
                build = await buildPhaseService.GetBuild(buildId, serverPrincipal);
            }
            catch (Exception)
            {
            }

            var buildOutput = build?.Output;
            var buildError = build?.Error;
           
            return new BuildLogModel
                   {
                       Output = buildOutput,
                       Error = buildError
                   };
        }

        private async Task BuildSync(
            Guid serverId,
            Guid id,
            string version,
            IDictionary<Guid, BuildInstructionData[]> buildInstructionLookup,
            IDictionary<Guid, byte[]> contentLookup,
            IDictionary<Guid, SourceControlBranchData> checkoutInfoLookup)
        {
            var server = await this.serverService.GetServer(serverId);

            var webClient = await this.client.CreateClient(server.Url.ToString());

            var projectBuild = contentLookup.Select(x => x.Key)
                                            .Select(x => new ProjectBuildContent
                                                         {
                                                             SourceControlId = x,
                                                             Files = contentLookup[x],
                                                             BranchName = checkoutInfoLookup[x].Name,
                                                             BranchHash = checkoutInfoLookup[x].Hash,
                                                             BuildInstructions = buildInstructionLookup[x]
                                                                                 .Select(y => new BuildInstruction
                                                                                              {
                                                                                                  Arguments = y.ExecutableArguments,
                                                                                                  Executable = y.ExecutableFile
                                                                                              })
                                                                                 .ToArray()
                                                         })
                                            .ToList();

            var allKeys = await this.keys.GetAllKeys();
            ClaimsPrincipal serverPrincipal = Principals.CreatePrincipal(allKeys.ToArray(), server.AuthenticationKeyId, ServerType.Compilation);
            var pb = await webClient.StartBuild(
                                                id,
                                                version,
                                                projectBuild,
                                                serverPrincipal);

            while (!pb.State.In(BuildPhaseState.CompletedFail, BuildPhaseState.CompletedSuccess))
            {
                var delay = Task.Delay(TimeSpan.FromSeconds(5));
                await this.storage.SetStatus(id, (BuildState) (int) pb.State);
                await delay;
                pb = await webClient.GetBuild(id, serverPrincipal);
            }

            await this.storage.SetStatus(id, (BuildState) (int) pb.State);
        }

        private class BuildModel : IBuild
        {
            public Guid Id { get; set; }
            public Guid ServerId { get; set; }
            public BuildState State { get; set; }
        }

        private class BuildLogModel : IBuildLog
        {
            public string Output { get; set; }
            public string Error { get; set; }
        }

        public class FileDataImpl : IFileData
        {
            public string RelativeName { get; set; }
            public string Name { get; set; }
            public byte[] Content { get; set; }
        }
    }
}