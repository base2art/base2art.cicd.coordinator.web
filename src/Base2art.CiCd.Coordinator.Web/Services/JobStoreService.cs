namespace Base2art.CiCd.Coordinator.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using MessageQueue;
    using Models;
    using Public.Messages;
    using Repositories;

    public class JobStoreService : IJobStoreService
    {
        private readonly IJobStoreRepository repo;
        private readonly IMessageQueue queue;

        public JobStoreService(IJobStoreRepository repo, IMessageQueue queue)
        {
            this.repo = repo;
            this.queue = queue;
        }

        public async Task<IJobInfo> SetState(Guid jobId, JobState state)
        {
            await this.repo.SetState(jobId, state);
            return await this.GetJobInfo(jobId);
        }

        public async Task<IJobInfo> SetBuildInfo(Guid jobId, Guid buildServer, Guid buildId)
        {
            await this.repo.SetBuildInfo(jobId, buildServer, buildId);
            return await this.GetJobInfo(jobId);
        }

        public async Task AddMessage(
            Guid buildId, 
            string projectName, 
            JobPhase phase, 
            string message, 
            Dictionary<string, object> data)
        {
            await this.repo.AddMessage(buildId, phase, message, data);

            await this.queue.Push(new JobStoreMessage
                                  {
                                      BuildId = buildId,
                                      ProjectName = projectName,
                                      Phase = phase,
                                      Message = message,
                                      Data = data
                                  });
        }

        public Task<IJobInfo> GetJobInfo(Guid jobId)
        {
            return this.repo.GetJobInfo(jobId);
        }

        public Task<IJobMessage[]> GetJobMessages(Guid jobId)
        {
            return this.repo.GetJobMessages(jobId);
        }

        public Task<IJobInfo[]> FindJobs(
            JobState? searchDataState,
            string searchDataBranchHash,
            string searchDataBranchName,
            Guid? searchDataProjectId,
            int pageSize,
            int pageIndex,
            JobSort sort)
        {
            return this.repo.FindJobs(
                                      searchDataState,
                                      searchDataBranchHash,
                                      searchDataBranchName,
                                      searchDataProjectId,
                                      pageSize,
                                      pageIndex,
                                      sort);
        }

        public Task<IJobInfo[]> FindMostRecentJobsByProjectAndBranch(JobState? searchDataState, int pageSize, int pageIndex, JobSort sort)
        {
            return this.repo.FindMostRecentJobsByProjectAndBranch(searchDataState, pageSize, pageIndex, sort);
        }

        public Task<IJobInfo[]> FindLatestJobs(SourceControlBranchData[] branchData)
        {
            return this.repo.FindLatestJobs(branchData ?? new SourceControlBranchData[0]);
        }

        public async Task<IJobInfo> CreateJob(
            Guid projectId,
            string projectName,
            string userName,
            Dictionary<Guid, SourceControlBranchData> sourceControl)
        {
            if (projectId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(projectId));
            }

            if (sourceControl == null || sourceControl.Count == 0)
            {
                throw new ArgumentNullException(nameof(sourceControl));
            }

            var id = Guid.NewGuid();
            await this.repo.CreateJob(id, projectId, projectName, userName, sourceControl);

            return await this.GetJobInfo(id);
        }
    }
}