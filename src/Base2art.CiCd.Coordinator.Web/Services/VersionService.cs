﻿namespace Base2art.CiCd.Coordinator.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Models;
    using Repositories;

    public class VersionService : IVersionService
    {
        private readonly IVersionRepository repository;
        private readonly string defaultVersion;

        public VersionService(IVersionRepository repository, string defaultVersion)
        {
            this.repository = repository;
            this.defaultVersion = defaultVersion;
        }

        public Task<string> GetLastVersion(Guid projectId, Dictionary<Guid, SourceControlBranchData> sourceControlInfoLookup) =>
            this.GetLastVersion(projectId, BranchIdentifier(sourceControlInfoLookup));

        public async Task<string> GetNextVersion(
            Guid projectId,
            Guid jobId,
            VersioningData versioningData,
            Dictionary<Guid, SourceControlBranchData> sourceControlInfoLookup)
        {
            var branchIdentifier = BranchIdentifier(sourceControlInfoLookup);

            var last = await this.GetLastVersion(projectId, branchIdentifier);

            var version = this.RevVersion(versioningData, last);
            await this.SetLastVersion(projectId, jobId, branchIdentifier, version);
            return version;
        }

        private string RevVersion(VersioningData data, string last)
        {
            var strategy = (data?.Strategy).GetValueOrDefault(VersioningStrategy.Semantic);

            switch (strategy)
            {
                case VersioningStrategy.DateStrategy1:
                    return this.DateStrategy1(last, data?.DefaultData ?? new Dictionary<string, string>());
                case VersioningStrategy.Semantic:
                    return this.Semantic(last, data?.DefaultData ?? new Dictionary<string, string>());
                default:
                    throw new ArgumentOutOfRangeException(nameof(strategy), strategy, "Case not found in switch");
            }
        }

        protected Task<string> GetLastVersion(Guid projectId, string branchIdentifier)
        {
            return this.repository.GetLastVersion(projectId, branchIdentifier);
        }

        protected Task SetLastVersion(Guid projectId, Guid jobId, string branchIdentifier, string version)
        {
            return this.repository.SetLastVersion(projectId, jobId, branchIdentifier, version);
        }

        private static string BranchIdentifier(Dictionary<Guid, SourceControlBranchData> sourceControlInfoLookup)
        {
            return string.Join("--", sourceControlInfoLookup.Select(x => x.Value.Name));
        }

        private string DateStrategy1(string last, Dictionary<string, string> data)
        {
            var format = "";
            if (data.ContainsKey("format"))
            {
                format = data["format"];
            }

            if (string.IsNullOrWhiteSpace(format))
            {
                format = "yy.MMdd.HHmm.ss";
            }

            return DateTime.UtcNow.ToString(format);
        }

        private string Semantic(string last, Dictionary<string, string> data)
        {
            if (string.IsNullOrWhiteSpace(last) || !Version.TryParse(last, out var old))
            {
                return data.ContainsKey("startingVersion")
                           ? data["startingVersion"]
                           : "0.0.0.1";
            }

            var oldMajor = old.Major;
            var oldMinor = old.Minor;
            var oldBuild = old.Build;
            var revision = old.Revision + 1;

            if (revision > 1000)
            {
                oldBuild += 1;
                revision = 0;
            }

            if (oldBuild > 1000)
            {
                oldMinor += 1;
                oldBuild = 0;
            }

            if (oldMinor > 1000)
            {
                oldMajor += 1;
                oldMinor = 0;
            }

            return $"{oldMajor}.{oldMinor}.{oldBuild}.{revision}";
        }
    }
}