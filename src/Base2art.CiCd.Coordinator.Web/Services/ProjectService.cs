namespace Base2art.CiCd.Coordinator.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;
    using Repositories;

    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository repository;

        public ProjectService(IProjectRepository repository)
        {
            this.repository = repository;
        }

        public Task<IProject[]> GetProjects(bool includeInactive)
        {
            return includeInactive
                       ? this.repository.GetAll()
                       : this.repository.GetAllActive();
        }

        public Task<IProject> GetProjectById(Guid projectId)
        {
            return this.repository.GetProjectById(projectId);
        }

        public async Task<IProject> DeactivateProject(Guid projectId)
        {
            await this.repository.DeactivateProject(projectId);

            return await this.GetProjectById(projectId);
        }

        public async Task<IProject> SaveProject(
            Guid projectId,
            string name,
//            string[] features,
            VersioningStrategy? versioningStrategy,
            Dictionary<string, string> versionData)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException(nameof(name));
            }

//            features = features ?? new string[0];

            var isAdd = this.IsAdd(projectId);

            if (isAdd)
            {
                projectId = Guid.NewGuid();
                await this.repository.CreateProject(
                                                    projectId,
                                                    name,
//                                                    features,
                                                    versioningStrategy.GetValueOrDefault(VersioningStrategy.Semantic),
                                                    versionData ?? new Dictionary<string, string>());
            }
            else
            {
                await this.repository.UpdateProject(
                                                    projectId,
                                                    name,
//                                                    features,
                                                    versioningStrategy.GetValueOrDefault(VersioningStrategy.Semantic),
                                                    versionData ?? new Dictionary<string, string>());
            }

            return await this.GetProjectById(projectId);
        }

        public Task AddSourceControl(Guid projectId, SourceControlData sourceControlData) 
            => this.repository.AddSourceControl(
                                             projectId,
                                             sourceControlData.SourceControlType,
                                             sourceControlData.SourceControlParameters,
                                             sourceControlData.Instructions);

        public Task UpdateSourceControl(Guid projectId, Guid sourceControlId, SourceControlData sourceControlData)
            => this.repository.UpdateSourceControl(
                                                projectId,
                                                sourceControlId,
                                                sourceControlData.SourceControlType,
                                                sourceControlData.SourceControlParameters,
                                                sourceControlData.Instructions);

        public async Task RemoveSourceControl(Guid projectId, Guid sourceControlId)
        {
            await this.repository.RemoveSourceControl(projectId, sourceControlId);
        }

        public Task AddFeature(Guid projectId, Guid featureId)
        {
            return this.repository.AddFeature(projectId, featureId);
        }

        public Task SetProjectFeatures(Guid projectId, Guid[] features)
        {
            return this.repository.SetFeatures(projectId, features);
        }

        public Task RemoveFeature(Guid projectId, Guid featureId)
        {
            return this.repository.RemoveFeature(projectId, featureId);
        }

        private bool IsAdd(Guid projectId)
        {
            if (projectId == Guid.Empty)
            {
                return true;
            }

            var proj = this.GetProjectById(projectId);
            return proj == null;
        }
    }
}