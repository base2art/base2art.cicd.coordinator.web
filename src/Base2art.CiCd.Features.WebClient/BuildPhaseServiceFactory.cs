//namespace Base2art.CiCd.Features.WebClient
//{
//    using System;
//    using System.Linq;
//    using System.Security.Claims;
//    using System.Threading.Tasks;
//    using Builder.WebClient;
//    using Coordinator.Repositories;
//    using Servers;
//
//    public class FeaturesServiceFactory : 
//    {
//        private readonly IServerReadRepository serverService;
//
//        public FeaturesServiceFactory(IServerReadRepository serverService) => this.serverService = serverService;
//
//        public async Task<ISupportedFeatureService> CreateClient(Guid projectType)
//        {
//            var baseUrl = await this.GetBaseUrlByType(projectType);
//            return new BuildServerServiceClient(baseUrl);
//        }
//
//        public Task<IBuildPhaseService> CreateClient(string baseUrl) => Task.FromResult<IBuildPhaseService>(new BuildServerServiceClient(baseUrl));
//
//        private async Task<string> GetBaseUrlByType(Guid projectSourceControlType)
//        {
//            var servers = await this.serverService.FindByFeature(projectSourceControlType);
//
//            return servers?.OrderBy(x => Guid.NewGuid())
//                          ?.FirstOrDefault(x => x.Enabled)
//                          ?.Url
//                          ?.ToString();
//        }
//
//        public Task<SupportedFeature[]> GetSupportedFeatures(ClaimsPrincipal principal) => throw new NotImplementedException();
//    }
//}