//namespace Base2art.CiCd.Features.WebClient
//{
//    using System.Security.Claims;
//
//    public static class Principals
//    {
//        public static IWebClientRequestBuilder WithAuthorizationBearer(this IWebClientRequestBuilder builder, ClaimsPrincipal principal)
//        {
//            var findFirst = principal?.FindFirst(ClaimTypes.Authentication);
//            if (string.IsNullOrWhiteSpace(findFirst?.Value))
//            {
//                return builder;
//            }
//
//            return builder.WithHeader("Authorization", $"Bearer {findFirst.Value}");
//        }
//    }
//}