namespace Base2art.CiCd.Publisher.WebClient
{
    using Base2art.WebClient;
    using Coordinator.Repositories;
    using Publisher;
    using Servers.WebClient;

    public class PublishPhaseServiceClientFactory : ExtensionServiceFactory<IPublishPhaseService>, IPublishPhaseServiceFactory
    {
        private readonly IWebClient client;

        public PublishPhaseServiceClientFactory(IServerReadRepository serverService, IWebClient client) : base(serverService)
            => this.client = client;

        protected override IPublishPhaseService Create(string serverUrl) => new PublishPhaseServiceClient(serverUrl, this.client);
    }
}