﻿namespace Base2art.CiCd.Publisher.WebClient
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.WebClient;
    using Base2art.WebClient.Json;
    using Base2art.WebClient.ResponseHandlers;
    using Base2art.WebClient.ResponseHandlers.Json;
    using Resources;
    using Servers;

    public class PublishPhaseServiceClient : IPublishPhaseService
    {
        private readonly string baseUrl;

        public PublishPhaseServiceClient(string baseUrl, IWebClient client)
        {
            this.baseUrl = baseUrl;
            this.client = client;
        }

        private readonly IWebClient client;

        public async Task<SupportedFeature[]> GetSupportedFeatures(ClaimsPrincipal principal)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("/api/v1/supported-features")
                      .ToString();

            List<SupportedFeature> returnValue = null;
            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Get)
                      .WithAuthorizationBearer(principal)
                      .WithResponseHandler()
                      .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<List<SupportedFeature>>(x => returnValue = x))
                      .WithStatusHandler(HttpStatusCode.Unauthorized, new ExceptionHandler())
                      .Run();

            return returnValue?.ToArray() ?? new SupportedFeature[0];
        }

        public async Task<ProjectPublish> StartPublish(Guid handlerId, PublishEvent @event, ClaimsPrincipal principal)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("/api/v1/artifacts/{handlerId}")
                      .WithUrlSegment("handlerId", handlerId.ToString("D"))
                      .ToString();

            Console.WriteLine(url);
            // @event.Artifacts = new FileData[0];
            var data = new ProjectPublish();
            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Put)
                      .WithAuthorizationBearer(principal)
                      .WithJsonBody(@event)
                      .WithTimeout(TimeSpan.FromMinutes(20))
                      .WithResponseHandler()
                      .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<ProjectPublish>(x => data = x))
                      .WithStatusHandler(HttpStatusCode.InternalServerError, new StringHandler(Console.WriteLine))
                      .Run();

            Console.WriteLine("Publish Id: " + data.Id);
            Console.WriteLine("Publish State: " + data.State);
            return data;
        }

        public async Task<ProjectPublish> GetPublishStatus(Guid publishEventId, ClaimsPrincipal principal)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("/api/v1/artifacts/{publishEventId}")
                      .WithUrlSegment("publishEventId", publishEventId.ToString("D"))
                      .ToString();

            Console.WriteLine(url);
            var data = new ProjectPublish();
            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Get)
                      .WithAuthorizationBearer(principal)
                      .WithTimeout(TimeSpan.FromMinutes(20))
                      .WithResponseHandler()
                      .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<ProjectPublish>(x => data = x))
                      .WithStatusHandler(HttpStatusCode.InternalServerError, new StringHandler(Console.WriteLine))
                      .Run();

            Console.WriteLine("Publish Id: " + data.Id);
            Console.WriteLine("Publish State: " + data.State);
            return data;
        }

        private class ExceptionHandler : IResponseHandler
        {
            public Task Invoke(string value) => throw new WebException("401: Unauthorized");
            public Task Invoke(byte[] value) => throw new WebException("401: Unauthorized");
        }
    }
}