namespace Base2art.CiCd.Coordinator.Web.UI
{
    public enum Role
    {
        Anonymous,
        User,
        Admin
    }
}