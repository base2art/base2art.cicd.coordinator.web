namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Keys
{
    using System;
    using System.Linq;
    using Models;
    using Public.Resources;

    public class IndexModel
    {
        public Key[] Keys { get; set; }
        public (PermissionMode mode, Guid[] objectIds) DeletionPermissions { get; set; }

        public bool UserCanDelete(Guid serverId)
        {
            return this.DeletionPermissions.mode == PermissionMode.Full || this.DeletionPermissions.objectIds.Any(x => x == serverId);
        }
    }
}