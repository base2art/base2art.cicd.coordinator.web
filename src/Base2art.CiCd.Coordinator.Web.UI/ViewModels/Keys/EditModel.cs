namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Keys
{
    using System;
    using System.Collections;
    using System.Linq.Expressions;
    using Base2art.Web.ObjectQualityManagement.Models;
    using Public.Resources;

    public class EditModel
    {
//        public Server[] Servers { get; set; }
        public Key Data { get; set; }
        public ErrorField[] Errors { get; set; }
    }
}