namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Partials
{
    public class HeaderModel
    {
        public Role Role { get; set; }

        public MessageData[] Messages { get; set; } = new MessageData[]
                                                      {
                                                          new MessageData {Text = "Building 1 item:"}
                                                      };
    }
}