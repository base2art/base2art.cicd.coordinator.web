namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Partials
{
    public class FooterModel
    {
        public string CompanyName { get; set; }
        public string CompanyUrl { get; set; }
    }
}