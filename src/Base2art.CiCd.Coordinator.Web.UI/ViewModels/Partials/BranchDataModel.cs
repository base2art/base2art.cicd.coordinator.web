namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Partials
{
    using System.Security.Claims;
    using Public.Resources;

    public class BranchDataModel
    {
        public Project proj { get; set; }
        public Base2art.CiCd.Coordinator.Web.Public.Resources.BranchRequisition[] KnownBranches { get; set; }
        public ClaimsPrincipal Principal { get; set; }

        public bool CanCreateBuilds()
        {
            return (this.Principal?.Identity?.IsAuthenticated).GetValueOrDefault();
        }
    }
}