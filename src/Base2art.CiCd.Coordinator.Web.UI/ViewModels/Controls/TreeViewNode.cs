namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Controls
{
    public class TreeViewNode
    {
        public string Name { get; set; }
        public string Href { get; set; }
        public TreeViewNode[] Children { get; set; } = new TreeViewNode[0];
    }
}