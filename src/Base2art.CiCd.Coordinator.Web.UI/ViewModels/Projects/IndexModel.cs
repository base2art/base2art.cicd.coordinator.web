namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Projects
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using Controls;
    using Models;
    using Public.Resources;

    public class IndexModel
    {
        public ProjectRequisition[] Projects { get; set; }
        public (PermissionMode mode, Guid[] objectIds) DeletionPermissions { get; set; }
        public (PermissionMode mode, Guid[] objectIds) EditPermissions { get; set; }

        public bool UserCanDelete(Guid serverId)
        {
            return this.DeletionPermissions.mode == PermissionMode.Full || this.DeletionPermissions.objectIds.Any(x => x == serverId);
        }

        public bool CanCreateBuilds()
            => (this.Principal?.Identity?.IsAuthenticated).GetValueOrDefault();

        public ClaimsPrincipal Principal { get; set; }

        public (string, string[]) GroupName(ProjectRequisition project)
        {
            var name = (project?.ProjectData?.Name ?? "").Split("/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            var parts = name.Reverse().Skip(1).Reverse().Select(x => x?.Trim()).ToArray();
            
            return (parts.Length == 0 ? "Default Group" : string.Join(" / ", parts), parts);
        }

        public string CleanName(ProjectRequisition project)
        {
            var name = (project?.ProjectData?.Name ?? "").Split("/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            return string.Join(" / ", name.Reverse().Take(1));
        }

        public JobState GetProjectState(BranchRequisition[] projKnownBranches)
        {
            if (projKnownBranches.All(x => x?.BuildData?.State == JobState.CompletedSuccess))
            {
                return JobState.CompletedSuccess;
            }

            if (projKnownBranches.Any(x => x?.BuildData?.State == JobState.CompletedFail))
            {
                return JobState.CompletedFail;
            }

            var items = projKnownBranches.Select(x => x?.BuildData?.State)
                                         .Where(x => !(x == JobState.CompletedFail || x == JobState.CompletedSuccess))
                                         .ToArray();
            return items.FirstOrDefault() ?? JobState.Unknown;
        }

        public IEnumerable<TreeViewNode> Groups()
        {
            List<TreeViewNode> items = new List<TreeViewNode>();
            var names = this.Projects.Select(this.GroupName);

            var root = new TreeViewNode();
            BuildByGroup(0, root, new string[0], names.ToArray());
            return root.Children;
        }

        private void BuildByGroup(int i, TreeViewNode root, string[] strings, (string, string[])[] names)
        {
            var groups = new HashSet<string>();
            
            
            foreach (var name in names)
            {
                var nameParts = name.Item2;
                // var nameParts = name.Split(new char[] {'/'}, StringSplitOptions.RemoveEmptyEntries);
                if (nameParts.Length <= i)
                {
                    // skip
                }
                else
                {
                    var current = nameParts[i];
                    if (AreEqual(strings, nameParts.Take(i).ToArray()))
                    {
                        groups.Add(current.Trim());
                    }
                }
            }

            foreach (var group in groups)
            {
                // var current = nameParts[i];
                // if (AreEqual(strings, nameParts.Take(i).ToArray()) && string.Equals(current, @group, StringComparison.OrdinalIgnoreCase))
                // {
                    var newItem = new TreeViewNode
                                  {
                                      Name = group,
                                      Href = BuildHref(strings, group)
                                      
                                  };
                    BuildByGroup(i + 1, newItem, strings.Concat(new[] {group}).ToArray(), names);
                    
                    root.Children = root.Children.Concat(new[] {newItem}).ToArray();
                // }
            }

            // foreach (var name in names)
            // {
            //     var nameParts = name.Split(new char[] {'/'}, StringSplitOptions.RemoveEmptyEntries);
            //     if (nameParts.Length <= i)
            //     {
            //         // skip
            //     }
            //     else
            //     {
            //     }
            // }

        }

        private string BuildHref(string[] strings, string @group)
        {
            return $"#{string.Join("_", strings.Concat(new[] {@group}))}";
        }

        private bool AreEqual(string[] strings, string[] take)
        {
            if (strings.Length != take.Length)
            {
                return false;
            }

            for (int i = 0; i < strings.Length; i++)
            {
                if (!string.Equals(strings[i].Trim(), take[i].Trim(), StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }
            }

            return true;
        }
    }
}