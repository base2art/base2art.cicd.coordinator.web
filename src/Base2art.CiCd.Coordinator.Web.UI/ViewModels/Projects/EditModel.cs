namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Projects
{
    using System.Collections.Generic;
    using Base2art.Web.Exceptions.Models;
    using Models;
    using Public.Resources;

    public class EditModel
    {
        public Project Data { get; set; }
        public ErrorField[] Errors { get; set; }
        public KeyValuePair<string, string>[] SourceControlTypes { get; set; }
        public IServerFeature[] AvailableFeatures { get; set; }
    }
}