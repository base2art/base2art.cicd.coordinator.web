namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Projects
{
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Razor.Forms.Generation;
    using Models;
    using Public.Resources;

    public class ViewBuildsModel
    {
        public Project Project { get; set; }
        public JobInfo[] Jobs { get; set; }
        public ClaimsPrincipal Principal { get; set; }
        public BranchRequisition[] KnownBranches { get; set; }

//        public JobState? GetProjectState(BranchRequisition[] knownBranches)
//        {
//            throw new System.NotImplementedException();
//        }
        public bool CanCreateBuild()
        {
            return (this.Principal?.Identity?.IsAuthenticated).GetValueOrDefault();
        }

        public bool CanViewBuild()
        {
            return (this.Principal?.Identity?.IsAuthenticated).GetValueOrDefault();
        }
    }
}