namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Projects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base2art.Web.Exceptions.Models;
    using Models;
    using Public.Resources;

    public class SourceEditModel
    {
        public Guid? SourceControlId { get; set; }
        public Project Data { get; set; }
        public ErrorField[] Errors { get; set; }
        public Dictionary<string, IServerFeature> SourceControlTypes { get; set; }
        public IServerFeature[] AvailableFeatures { get; set; }

        public KeyValuePair<string, string>[] SourceControlTypesValues()
        {
            return this.SourceControlTypes.Select(x => new KeyValuePair<string, string>(x.Key, x.Value.SimpleName)).ToArray();
        }
    }
}