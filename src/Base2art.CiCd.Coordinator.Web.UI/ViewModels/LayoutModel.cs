namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels
{
    using System.Threading.Tasks;
    using Base2art.Web.Pages;
    using Partials;

    public class LayoutModel
    {
        public string Title { get; set; }

        public Task<WebPage> MainPanel { get; set; }
        
        public Task<WebPage>[] LeftPanels { get; set; } = new Task<WebPage>[0]; 
        public Task<WebPage>[] RightPanels { get; set; } = new Task<WebPage>[0]; 
        
//        public Task<WebPage> MainPanel { get; set; }

        public FooterModel FooterData { get; set; } = new FooterModel();

        public HeaderModel HeaderData { get; set; }

//        public LayoutMode Wide { get; set; }

        public LayoutMode Mode { get; set; }
//        public Header<o Header { get; set; }

//        public IEnumerable<Task<WebPage>> AdditionalScriptSections { get; set; }
//        public IEnumerable<Task<WebPage>> AdditionalHeadSections { get; set; }
    }
}