namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Panels
{
    using Models;
    using Public.Resources;

    public class BuildsPanelModel
    {
        public IJobInfo[] Builds { get; set; }
        public int? MaxRecords { get; }
    }
}