namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Panels
{
    using System.Collections;
    using Models;
    using Public.Resources;

    public class ServersStatusPanelModel
    {
        public Server[] Servers { get; set; }
        public bool? StatusFilter { get; set; }
        public int? MaxRecords { get; set; }
        public ServerType? ServerTypeFilter { get; set; }
    }
}