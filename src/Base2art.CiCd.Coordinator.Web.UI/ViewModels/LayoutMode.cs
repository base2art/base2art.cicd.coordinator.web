namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels
{
    public enum LayoutMode
    {
        Empty,
        Wide,
        Left,
        Center,
        Right
        
    }
}