namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Builds
{
    using System.Security.Claims;
    using Public.Resources;

    public class ViewBuildModel
    {
        public Job Job { get; set; }
        public ClaimsPrincipal Principal { get; set; }
    }
}