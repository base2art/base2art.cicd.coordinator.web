namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Servers
{
    using System;
    using System.Linq;
    using Models;
    using Public.Resources;

    public class IndexModel
    {
        public Server[] Servers { get; set; }
        public (PermissionMode mode, Guid[] objectIds) EditPermissions { get; set; }
        public (PermissionMode mode, Guid[] objectIds) DeletionPermissions { get; set; }

        public bool UserCanEdit(Guid serverId)
        {
            return this.EditPermissions.mode == PermissionMode.Full || this.EditPermissions.objectIds.Any(x => x == serverId);
        }
        
        public bool UserCanDelete(Guid serverId)
        {
            return this.DeletionPermissions.mode == PermissionMode.Full || this.DeletionPermissions.objectIds.Any(x => x == serverId);
        }
    }
}