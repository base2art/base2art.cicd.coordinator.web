namespace Base2art.CiCd.Coordinator.Web.UI.ViewModels.Servers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Base2art.Web.ObjectQualityManagement.Models;
    using Public.Resources;

    public class EditModel
    {
//        public Server[] Servers { get; set; }
        public Server Data { get; set; }
        public ErrorField[] Errors { get; set; }
        public KeyValuePair<string, string>[] Keys { get; set; }
    }
}