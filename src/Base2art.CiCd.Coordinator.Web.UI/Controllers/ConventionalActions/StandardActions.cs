namespace Base2art.CiCd.Coordinator.Web.UI.Controllers.ConventionalActions
{
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.ObjectQualityManagement;
    using Models;
    using Public.Resources;

    public class StandardActions
    {
        private readonly IQualityManagementLookup validation;
        private readonly ISecurityService security;

        public StandardActions(IQualityManagementLookup validation, ISecurityService security)
        {
            this.validation = validation;
            this.security = security;
        }

        public async Task<(TObject[] objs, (PermissionMode, Guid[] objectIds) editPermisions, (PermissionMode, Guid[] objectIds) deletePermisions)>
            ListItems<TObject>(
                ClaimsPrincipal principal,
                Func<Task<TObject[]>> lookup)
            where TObject : class
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(TObject), Operation.List));

            var servers = await lookup();

            var deletionPermissions = await this.security.Can<TObject>(user: principal, OperationType.Delete);
            var editPermissions = await this.security.Can<TObject>(user: principal, OperationType.Edit);

            return (servers, editPermissions, deletionPermissions);
        }

        public async Task<TObject> GetItem<TObject>(ClaimsPrincipal principal, Guid id, Func<Task<TObject>> lookup)
            where TObject : class
        {
            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(TObject), ItemOperation.View, id));

            var servers = await lookup();
//
//            var deletionPermissions = await this.security.Can<TObject>(user: principal, OperationType.Delete);
//            var editPermissions = await this.security.Can<TObject>(user: principal, OperationType.Edit);

            return servers;
        }

//        private async object (ClaimsPrincipal principal, Func<Task<Project>> func)
//        {
//            throw new NotImplementedException();
//        }
    }
}