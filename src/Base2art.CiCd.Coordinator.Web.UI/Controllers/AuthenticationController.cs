namespace Base2art.CiCd.Coordinator.Web.UI.Controllers
{
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Exceptions;

    public class AuthenticationController
    {
        public Task SignIn(ClaimsPrincipal principal)
        {
            if (!principal.Identity.IsAuthenticated)
            {
                throw new NotAuthenticatedException();
            }
            else
            {
                throw new RedirectException("/", false, false);
            }
        }
        
//        public Task SignOut(ClaimsPrincipal principal)
//        {
//            if (!principal.Identity.IsAuthenticated)
//            {
//                throw new NotAuthenticatedException();
//            }
//            else
//            {
//                throw new RedirectException("/", false, false);
//            }
//        }
    }
}