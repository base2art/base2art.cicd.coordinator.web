namespace Base2art.CiCd.Coordinator.Web.UI.Controllers
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Models;
    using Public.Resources;

    public static class Principals
    {
        public static async Task<(PermissionMode mode, Guid[] objectIds)> Can<T>(
            this ISecurityService security,
            ClaimsPrincipal user,
            OperationType operationType)
            where T : class
        {
            var accessRecords = await security.GetAccessRecordsByUser(user.Identity.Name);
            var recordsByType = accessRecords.Where(x => x.ObjectType == typeof(T) && x.OperationType == operationType)
                                             .ToArray();

            if (recordsByType.Any(x => x.PermissionMode == PermissionMode.None))
            {
                return (PermissionMode.None, new Guid[0]);
            }

            if (recordsByType.Any(x => x.PermissionMode == PermissionMode.Full))
            {
                return (PermissionMode.Full, new Guid[0]);
            }

            var accessRecordByType = await security.GetAccessRecordsByUserAndObjectType(user.Identity.Name, typeof(T));
            var recordIdsByType = accessRecordByType.Where(x => x.OperationType == operationType)
                                                    .ToArray();

            return (PermissionMode.Partial, recordIdsByType.Select(x => x.ObjectId).ToArray());
        }
    }
}