namespace Base2art.CiCd.Coordinator.Web.UI.Controllers.Panels
{
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Pages;
    using Models;
    using Pages.Panels;
    using Public.Endpoints;
    using Public.Resources;
    using ViewModels.Panels;

    public class BuildsPanelController
    {
        private readonly IJobStoreService service;

        public BuildsPanelController(IJobStoreService service)
        {
            this.service = service;
        }

        public async Task<WebPage> Index(ClaimsPrincipal principal)
        {
            var servers = await this.service.FindJobs(
                                                      null,
                                                      null,
                                                      null,
                                                      null,
                                                      10,
                                                      0,
                                                      JobSort.DateCreatedDescending);
            return await new BuildsPanel().AsPageAsync(new BuildsPanelModel
                                                       {
                                                           Builds = servers
                                                       });
        }

        public async Task<WebPage> Failed(ClaimsPrincipal principal)
        {
            var servers = await this.service.FindMostRecentJobsByProjectAndBranch(JobState.CompletedFail, 10, 0, JobSort.DateCreatedDescending);
            return await new BuildsPanel().AsPageAsync(new BuildsPanelModel
                                                       {
                                                           Builds = servers
                                                       });
        }

        public async Task<WebPage> FailingProjects(ClaimsPrincipal principal)
        {
            var jobs = await this.service.FindMostRecentJobsByProjectAndBranch(null, 1000, 0, JobSort.DateCreatedDescending);
            jobs = jobs.Where(x => x.State == JobState.CompletedFail).ToArray();
            return await new BuildsPanel().AsPageAsync(new BuildsPanelModel
                                                       {
                                                           Builds = jobs
                                                       });
        }
    }
}