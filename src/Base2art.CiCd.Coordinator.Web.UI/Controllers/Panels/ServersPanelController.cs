namespace Base2art.CiCd.Coordinator.Web.UI.Controllers.Panels
{
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Pages;
    using Models;
    using Pages.Panels;
    using Public.Endpoints;
    using Public.Resources;
    using ViewModels.Panels;

    public class ServersPanelController
    {
        private readonly ServerService service;

        public ServersPanelController(ServerService service)
        {
            this.service = service;
        }

        public async Task<WebPage> Index(ClaimsPrincipal principal, bool? status = null, ServerType? serverType = null)
        {
            var servers = await this.service.GetAllServers(principal);
            return await new ServersStatusPanel().AsPageAsync(new ServersStatusPanelModel
                                                              {
                                                                  Servers = servers,
                                                                  StatusFilter = status,
                                                                  ServerTypeFilter = serverType
                                                              });
        }
    }
}