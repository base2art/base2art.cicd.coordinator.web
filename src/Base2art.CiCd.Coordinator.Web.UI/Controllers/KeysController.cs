namespace Base2art.CiCd.Coordinator.Web.UI.Controllers
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Exceptions;
    using Base2art.Web.ObjectQualityManagement;
    using Base2art.Web.ObjectQualityManagement.Models;
    using Base2art.Web.Pages;
    using Models;
    using Public.Endpoints;
    using Public.Resources;
    using Public.Validators;
    using ViewModels.Keys;

    public class KeysController
    {
        private readonly KeyService service;
        private readonly IQualityManagementLookup validation;
        private readonly ISecurityService security;
        private readonly Layouts layouts;

        public KeysController(KeyService service, IQualityManagementLookup validation, ISecurityService security, Layouts layouts)
        {
            this.service = service;
            this.validation = validation;
            this.security = security;
            this.layouts = layouts;
        }

        public async Task<WebPage> Index(ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Key), Operation.List));
            var servers = await this.service.FindKeys(principal);

            var deletionPermissions = await this.security.Can<Key>(user: principal, OperationType.Delete);

            return await this.layouts.WithLayoutAsync(new Pages.Keys.Index(), new IndexModel
                                                                              {
                                                                                  Keys = servers,
                                                                                  DeletionPermissions = deletionPermissions
                                                                              }, principal);
        }

//        public async Task<WebPage> ViewItem(Guid id, ClaimsPrincipal principal)
//        {
//            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Server), ItemOperation.View, id));
//            var servers = await this.service.GetAllServers(principal);
//            return new Pages.Keys.Index().WithLayout(new IndexModel {Servers = servers}, principal);
//        }

        public async Task<WebPage> GetAddKey(ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Key), Operation.Add));
            return await Edit(new Key(), new ErrorField[0], principal);
        }

        public async Task<WebPage> AddKey(ClaimsPrincipal principal, Key key)
        {
            var (errors, result) = await QualityManagement.Run(() => this.service.AddKey(principal, key));

            if (errors.Length == 0)
            {
                throw new RedirectException("/admin/keys/view/" + result.Id, false, false);
            }

            return await Edit(key, errors, principal);
        }

        public async Task<WebPage> GetEditItem(Guid id, ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Key), ItemOperation.Edit, id));
            var servers = await this.service.FindKeys(principal);
            return await Edit(servers.First(x => x.Id == id), null, principal);
        }

//        public async Task<WebPage> EditItem(Guid id, Server server, ClaimsPrincipal principal)
//        {
//            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Server), ItemOperation.Edit, id));
//            server = await this.service.UpdateServer(principal, id, server);
//            return await Edit(server, null, principal);
//        }

        public async Task<WebPage> DeleteItem(Guid id, ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Key), ItemOperation.Delete, id));
            await this.service.DeleteKey(principal, id);

            throw new RedirectException("/admin/keys", false, false);
        }

        public Task<WebPage> Edit(Key value, ErrorField[] errors, ClaimsPrincipal principal)
        {
            return this.layouts.WithLayoutAsync(
                                                new Pages.Keys.Edit(),
                                                new EditModel {Data = value, Errors = errors ?? new ErrorField[0]},
                                                principal);
        }
    }
}