namespace Base2art.CiCd.Coordinator.Web.UI.Controllers
{
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Exceptions;
    using Base2art.Web.Pages;
    using Mappings;
    using Models;
    using Pages.Dashboard;
    using Pages.Partials;
    using Panels;
    using Public.Endpoints;
    using ViewModels.Controls;
    using ViewModels.Dashboard;

    public class DashboardController
    {
        private readonly ServerService servers;
        private readonly IJobStoreService jobstore;
        private readonly IProjectService projects;
        private readonly Layouts layouts;

        public DashboardController(ServerService servers, IJobStoreService jobstore, IProjectService projects, Layouts layouts)
        {
            this.servers = servers;
            this.jobstore = jobstore;
            this.projects = projects;
            this.layouts = layouts;
        }

//            var serversPanel = null; //
//            var recentFailedBuildsPanel = null; //
//            var recentActivityPanel = null; //
        public async Task<WebPage> Index(ClaimsPrincipal principal)
        {
            var controller = new ServersPanelController(this.servers);

            var panel1 = controller.Index(principal, false, ServerType.Compilation);
            var panel2 = controller.Index(principal, false, ServerType.Publish);
            var panel3 = controller.Index(principal, false, ServerType.SourceControl);
            var panel4 = controller.Index(principal, false);

            var controller2 = new BuildsPanelController(this.jobstore);
            var panel5 = controller2.Index(principal);
            var panel6 = controller2.FailingProjects(principal);
//            return await new Index().WithLayoutAndPanels(new IndexModel(),

            var allProjects = await this.projects.GetProjects(false);
            var treeView = allProjects.MapToConcrete();
            
            Task<WebPage> projectTreeView = Task.FromResult(WebPage.Create(new TreeViewPanel(), (treeView.ToArray(), true)));
            return await this.layouts.WithLayoutAndPanels(panel5,
                                                          new[] {panel1, panel2, panel3, panel4, projectTreeView},
                                                          new[]
                                                          {
                                                              panel6
                                                          },
                                                          principal);
        }
        //
        // private TreeViewNode[] Map(IProject[] allProjects)
        // {
        //     // allProjects.
        // }

        public Task<WebPage> Theme(int width, ClaimsPrincipal principal)
        {
            if (!principal.Identity.IsAuthenticated)
            {
                throw new NotAuthenticatedException();
            }

            if (width == 3)
            {
                return this.layouts.WithLayoutAndPanels(new Index(), new IndexModel(),
                                                        new[] {new TestPanel().AsPageAsync()},
                                                        new[] {new TestPanel().AsPageAsync(), new TestPanel().AsPageAsync()},
                                                        principal);
            }

            if (width == 2)
            {
                return this.layouts.WithLayoutAndLeftPanels(new Index(), new IndexModel(),
                                                            new[] {new TestPanel().AsPageAsync(), new TestPanel().AsPageAsync()},
                                                            principal);
            }

            if (width == 1)
            {
                return this.layouts.WithLayoutAndRightPanels(new Index(), new IndexModel(),
                                                             new[] {new TestPanel().AsPageAsync(), new TestPanel().AsPageAsync()},
                                                             principal);
            }

            return this.layouts.WithLayoutAsync(
                                                new Index(),
                                                new IndexModel(),
                                                principal);
        }
    }
}