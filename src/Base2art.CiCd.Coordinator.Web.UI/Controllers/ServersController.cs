namespace Base2art.CiCd.Coordinator.Web.UI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Exceptions;
    using Base2art.Web.ObjectQualityManagement;
    using Base2art.Web.ObjectQualityManagement.Models;
    using Base2art.Web.Pages;
    using Collections;
    using ConventionalActions;
    using Models;
    using Public.Endpoints;
    using Public.Resources;
    using Public.Validators;
    using Threading.Tasks;
    using ViewModels.Servers;

    public class ServersController
    {
        private readonly Layouts layout;
        private readonly ServerService service;
        private readonly IQualityManagementLookup validation;
        private readonly ISecurityService security;
        private KeyService keys;

        private readonly StandardActions web;

        public ServersController(Layouts layout, ServerService service, IQualityManagementLookup validation, ISecurityService security, KeyService keys)
        {
            this.layout = layout;
            this.service = service;
            this.validation = validation;
            this.security = security;
            this.keys = keys;
            this.web = new StandardActions(validation, security);
        }

        public async Task<WebPage> Index(ClaimsPrincipal principal)
        {
            var (servers, editPermissions, deletionPermissions) = await this.web.ListItems(principal, () => this.service.GetAllServers(principal));

            return await this.layout.WithLayoutAsync(new Pages.Servers.Index(),new IndexModel
                                                                               {
                                                                                   Servers = servers,
                                                                                   DeletionPermissions = deletionPermissions,
                                                                                   EditPermissions = editPermissions,
                                                                               }, principal);
        }

//        public async Task<WebPage> ViewItem(Guid id, ClaimsPrincipal principal)
//        {
//            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Server), ItemOperation.View, id));
//            var servers = await this.service.GetAllServers(principal);
//            return new Pages.Servers.Index().WithLayout(new IndexModel {Servers = servers}, principal);
//        }

        public async Task<WebPage> GetAddServer(ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Server), Operation.Add));
            return await Edit(new Server(), new ErrorField[0], principal);
        }

        public async Task<WebPage> AddServer(ClaimsPrincipal principal, Server server)
        {
            var (errors, result) = await QualityManagement.Run(() => this.service.AddServer(principal, server.ServerType, server));

            if (errors.Length == 0)
            {
                throw new RedirectException("/admin/servers/edit/" + result.Id, false, false);
            }

            return await Edit(server, errors, principal);
        }

        public async Task<WebPage> GetEditItem(Guid id, ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Server), ItemOperation.Edit, id));
            var servers = await this.service.GetAllServers(principal);
            return await Edit(servers.First(x => x.Id == id), null, principal);
        }

        public async Task<WebPage> EditItem(Guid id, Server server, ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Server), ItemOperation.Edit, id));
            server = await this.service.UpdateServer(principal, id, server);
            return await Edit(server, null, principal);
        }

        public async Task<WebPage> DeleteItem(Guid id, ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Server), ItemOperation.Delete, id));
            await this.service.DeleteServer(principal, id);

            throw new RedirectException("/admin/servers", false, false);
        }

        public async Task<WebPage> Edit(Server value, ErrorField[] errors, ClaimsPrincipal principal)
        {
            return await this.layout.WithLayoutAsync(new Pages.Servers.Edit(), new EditModel
                                                                               {
                                                                                   Data = value,
                                                                                   Errors = errors ?? new ErrorField[0],
                                                                                   Keys = await this.keys.FindKeys(principal)
                                                                                                    .Then()
                                                                                                    .Select(x => (x.Id.ToString(), x.Name).Pair())
                                                                                                    .Then()
                                                                                                    .ToArray()
                                                                                                    .Then()
                                                                                                    .Return(x => new[] {("", "( Select One )").Pair()}.Concat(x)
                                                                                                        .ToArray())
                                                                               }, principal);
        }
    }
}