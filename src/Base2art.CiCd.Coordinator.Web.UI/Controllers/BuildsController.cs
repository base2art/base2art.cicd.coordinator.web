namespace Base2art.CiCd.Coordinator.Web.UI.Controllers
{
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.ObjectQualityManagement;
    using Base2art.Web.Pages;
    using ConventionalActions;
    using Models;
    using Pages.Builds;
    using Public.Endpoints;
    using Public.Resources;
    using ViewModels.Builds;
    using ViewModels.Projects;
    using IndexModel = ViewModels.Builds.IndexModel;

    public class BuildsController
    {
        private readonly JobService service;
        private readonly StandardActions web;
        private readonly Layouts layouts;

        public BuildsController(IQualityManagementLookup validation, ISecurityService security, JobService service, Layouts layouts)
        {
            this.service = service;
            this.layouts = layouts;
            this.web = new StandardActions(validation, security);
        }

        public async Task<WebPage> Index(ClaimsPrincipal principal)
        {
            var buildsOfProject = await this.web.ListItems(principal, () => this.service.FindJobs(principal,
                                                                                                  new JobSearchCriteria(),
                                                                                                  Pagination.FirstPage(100),
                                                                                                  JobSort.DateCreatedDescending));

            return await this.layouts.WithLayoutAsync(new Pages.Projects.ViewBuilds(), new ViewBuildsModel
                                                                                       {
                                                                                           Jobs = buildsOfProject.objs,
                                                                                           Principal = principal,
                                                                                       }, principal);
        }

        public async Task<WebPage> ViewBuild(ClaimsPrincipal principal, Guid buildId)
        {
            var buildsOfProject = await this.web.GetItem(principal, buildId, () => this.service.GetJob(principal, buildId));

            return await this.layouts.WithLayoutAsync(new ViewBuild(), new ViewBuildModel
                                                                       {
                                                                           Job = buildsOfProject,
                                                                           Principal = principal,
                                                                       }, principal);
        }
    }
}