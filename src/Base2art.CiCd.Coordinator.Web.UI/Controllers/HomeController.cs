namespace Base2art.CiCd.Coordinator.Web.UI.Controllers
{
    using System.Threading.Tasks;
    using Base2art.Web.Exceptions;
    using Base2art.Web.Pages;

    public class HomeController
    {
        public Task<WebPage> Index()
        {
            throw new RedirectException("/dashboard", false, false);
        }
    }
}