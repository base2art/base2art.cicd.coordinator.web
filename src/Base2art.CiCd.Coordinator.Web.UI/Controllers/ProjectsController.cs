namespace Base2art.CiCd.Coordinator.Web.UI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Xml.Schema;
    using Base2art.Web.Exceptions;
    using Base2art.Web.Exceptions.Models;
    using Base2art.Web.ObjectQualityManagement;
    using Base2art.Web.Pages;
    using Base2art.Web.Razor.Forms.Comparison;
    using ConventionalActions;
    using Models;
    using Public.Endpoints;
    using Public.Mappings;
    using Public.Resources;
    using Repositories;
    using ViewModels.Projects;
    using ProjectService = Public.Endpoints.ProjectService;
    using JobService = Public.Endpoints.JobService;

    public class ProjectsController
    {
        private readonly ProjectService service;

        private readonly StandardActions web;

        private readonly JobService builds;
        private readonly IServerReadRepository serversService;
        private readonly IQualityManagementLookup validation;
        private readonly Layouts layouts;

        public ProjectsController(
            ProjectService service,
            JobService builds,
            IServerReadRepository serversService,
            IQualityManagementLookup validation,
            ISecurityService security,
            Layouts layouts)
        {
            this.web = new StandardActions(validation, security);
            this.service = service;
            this.builds = builds;
            this.serversService = serversService;
            this.validation = validation;
            this.layouts = layouts;
        }

        public async Task<WebPage> Index(ClaimsPrincipal principal)
        {
            var (objs, editPermissions, deletePermissions) =
                await this.web.ListItems(principal, () => this.service.GetProjectsWithBuildsAndBranches(principal, true));

            return await this.layouts.WithLayoutAsync(new Pages.Projects.Index(), new IndexModel
                                                                                  {
                                                                                      Projects = objs,
                                                                                      DeletionPermissions = deletePermissions,
                                                                                      EditPermissions = editPermissions,
                                                                                      Principal = principal
                                                                                  }, principal);
        }

        public async Task<WebPage> ViewBuilds(ClaimsPrincipal principal, Guid projectId)
        {
            var obj = await this.web.GetItem(principal, projectId, () => this.service.GetProjectById(principal, projectId));
            var objs = await this.service.GetProjectsWithBuildsAndBranches(principal);

            var buildsOfProject = await this.builds.FindJobs(principal,
                                                             new JobSearchCriteria {ProjectId = projectId},
                                                             Pagination.FirstPage(100),
                                                             JobSort.DateCreatedDescending);

            return await this.layouts.WithLayoutAsync(new Pages.Projects.ViewBuilds(), new ViewBuildsModel
                                                                                       {
                                                                                           Project = obj,
                                                                                           Jobs = buildsOfProject,
                                                                                           Principal = principal,
                                                                                           KnownBranches =
                                                                                               objs.FirstOrDefault(x => x.ProjectData.Id == projectId)
                                                                                                   ?.KnownBranches
                                                                                       }, principal);
        }

        public async Task<WebPage> GetNew(ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Project), Operation.Add));
            return await this.EditPage(new Project(), null, principal);
        }

        public async Task<WebPage> New(ClaimsPrincipal principal, Project project)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Project), Operation.Add));
            var projectNew = await this.service.AddProject(principal, project);
            throw new RedirectException($@"/admin/projects/edit/{projectNew.Id}", false, false);
        }

        public async Task<WebPage> GetEdit(Guid projectId, ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Project), ItemOperation.Edit, projectId));
            var servers = await this.service.GetProjectById(principal, projectId);
            return await this.EditPage(servers, null, principal);
        }

        public async Task<WebPage> Edit(Guid projectId, Project project, ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Project), ItemOperation.Edit, projectId));
            var oldProject = await this.service.GetProjectById(principal, projectId);
            oldProject.RequiredFeatureIds = project.RequiredFeatureIds;
            await this.service.SetProjectFeatures(principal, projectId, project.RequiredFeatureIds.Select(x => new Feature {Id = x}).ToArray());
            return await this.EditPage(oldProject, null, principal);
        }

        public async Task<WebPage> GetSourceNew(ClaimsPrincipal principal, Guid projectId)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Project), Operation.Add));
            var proj = await this.service.GetProjectById(principal, projectId);
            return await this.SourceEditPage(proj, null, null, principal);
        }

        public async Task<WebPage> SourceNew(ClaimsPrincipal principal, Guid projectId, SourceControl sourceControl)
        {
            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Project), Operation.Add));
            Clean(sourceControl);
            var proj = await this.service.GetProjectById(principal, projectId);
            var oldIds = proj.SourceControl.Select(x => x.Id).ToImmutableHashSet();
            var newProject = await this.service.AddProjectSourceControl(principal, projectId, sourceControl);
            var newIds = newProject.SourceControl.Select(x => x.Id).ToImmutableHashSet();

            foreach (var oldId in oldIds)
            {
                newIds = newIds.Remove(oldId);
            }

            if (newIds.Count == 1)
            {
                throw new RedirectException($"/admin/projects/edit/{projectId}/source/{newIds.First()}", false, false);
            }

            throw new RedirectException($"/admin/projects/edit/{projectId}", false, false);
        }

        public async Task<WebPage> GetSourceEdit(Guid projectId, Guid sourceId, ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Project), ItemOperation.Edit, projectId));
            var proj = await this.service.GetProjectById(principal, projectId);
            return await this.SourceEditPage(proj, sourceId, null, principal);
        }

        public async Task<WebPage> SourceEdit(Guid projectId, Guid sourceId, SourceControl sourceControl, ClaimsPrincipal principal)
        {
            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Project), ItemOperation.Edit, projectId));
            Clean(sourceControl);
            var oldProject = await this.service.GetProjectById(principal, projectId);

            var scToSet = oldProject.SourceControl.Where(x => x.Id != sourceId).Concat(new[] {sourceControl}).ToArray();
            var id = await this.service.SetProjectSourceControls(principal, projectId, scToSet);
            var newProject = await this.service.GetProjectById(principal, projectId);
            return await this.SourceEditPage(newProject, sourceId, null, principal);
        }

        public async Task<WebPage> SourceEditPage(Project value, Guid? sourceControlId, ErrorField[] errors, ClaimsPrincipal principal)
        {
            var sourceResult = await this.serversService.GetServers(ServerType.SourceControl);
            var buildResult = await this.serversService.GetServers(ServerType.Compilation);
            var sourceFeatures = sourceResult.SelectMany(x => x.Features)
                                             .Distinct(new StandardComparers.EqualityComparer<IServerFeature>((x, y) => x.Id.CompareTo(y.Id),
                                                           x => x.Id.GetHashCode()))
                                             .ToArray();
            var buildFeatures = buildResult.SelectMany(x => x.Features)
                                           .Distinct(new StandardComparers.EqualityComparer<IServerFeature>((x, y) => x.Id.CompareTo(y.Id),
                                                         x => x.Id.GetHashCode()))
                                           .ToArray();

            return await this.layouts.WithLayoutAsync(
                                                      new Pages.Projects.SourceEdit(),
                                                      new SourceEditModel
                                                      {
                                                          SourceControlId = sourceControlId,
                                                          Data = value,
                                                          Errors = errors ?? new ErrorField[0],
                                                          SourceControlTypes = sourceFeatures.ToDictionary(
                                                           x => x.Id.ToString("D"),
                                                           x => x),
                                                          AvailableFeatures = buildFeatures
                                                      },
                                                      principal);
        }

        public async Task<WebPage> EditPage(Project value, ErrorField[] errors, ClaimsPrincipal principal)
        {
            var sourceResult = await this.serversService.GetServers(ServerType.SourceControl);
            var buildResult = await this.serversService.GetServers(ServerType.Compilation);
            var sourceFeatures = sourceResult.SelectMany(x => x.Features)
                                             .Distinct(new StandardComparers.EqualityComparer<IServerFeature>((x, y) => x.Id.CompareTo(y.Id),
                                                           x => x.Id.GetHashCode()))
                                             .ToArray();
            var buildFeatures = buildResult.SelectMany(x => x.Features)
                                           .Distinct(new StandardComparers.EqualityComparer<IServerFeature>((x, y) => x.Id.CompareTo(y.Id),
                                                         x => x.Id.GetHashCode()))
                                           .ToArray();

            return await this.layouts.WithLayoutAsync(
                                                      new Pages.Projects.Edit(),
                                                      new EditModel
                                                      {
                                                          Data = value,
                                                          Errors = errors ?? new ErrorField[0],
                                                          SourceControlTypes = sourceFeatures
                                                                               .Select(x => new KeyValuePair<string, string>(x.Id.ToString(), x.SimpleName))
                                                                               .ToArray(),
                                                          AvailableFeatures = buildFeatures
                                                      },
                                                      principal);
        }

        private void Clean(SourceControl sourceControl)
        {
            if (sourceControl == null)
            {
                return;
            }

            foreach (var bi in sourceControl.BuildInstructions ?? new BuildInstruction[0])
            {
                bi.ExecutableArguments = (bi.ExecutableArguments ?? new string[0]).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
                if (bi.ExecutableArguments.Length == 1 && bi.ExecutableArguments[0]?.Contains("\n") == true)
                {
                    bi.ExecutableArguments = bi.ExecutableArguments[0].Split(new char[] {'\r', '\n'}, StringSplitOptions.RemoveEmptyEntries);
                }
            }

            // foreach (var bi in sourceControl.BuildInstructions ?? new BuildInstruction[0])
            // {
            //     bi.ExecutableArguments = (bi.ExecutableArguments ?? new string[0]).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            // }
        }
//        public async Task<WebPage> ViewItem(Guid id, ClaimsPrincipal principal)
//        {
//            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Server), ItemOperation.View, id));
//            var servers = await this.service.GetAllServers(principal);
//            return new Pages.Servers.Index().WithLayout(new IndexModel {Servers = servers}, principal);
//        }

//        public async Task<WebPage> GetAddServer(ClaimsPrincipal principal)
//        {
//            await this.validation.ValidateAndVerify(new ActionAudit(principal, typeof(Server), Operation.Add));
//            return await this.Edit(new Server(), new ErrorField[0], principal);
//        }
//
//        public async Task<WebPage> AddServer(ClaimsPrincipal principal, Server server)
//        {
//            var (errors, result) = await QualityManagement.Run(() => this.service.AddServer(principal, server.ServerType, server));
//
//            if (errors.Length == 0)
//            {
//                throw new RedirectException("/admin/servers/edit/" + result.Id, false, false);
//            }
//
//            return await this.Edit(server, errors, principal);
//        }
//
//
//        public async Task<WebPage> EditItem(Guid id, Server server, ClaimsPrincipal principal)
//        {
//            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Server), ItemOperation.Edit, id));
//            server = await this.service.UpdateServer(principal, id, server);
//            return await this.Edit(server, null, principal);
//        }
//
//        public async Task<WebPage> DeleteItem(Guid id, ClaimsPrincipal principal)
//        {
//            await this.validation.ValidateAndVerify(new ActionItemAudit(principal, typeof(Server), ItemOperation.Delete, id));
//            await this.service.DeleteServer(principal, id);
//
//            throw new RedirectException("/admin/servers", false, false);
//        }
//
    }
}