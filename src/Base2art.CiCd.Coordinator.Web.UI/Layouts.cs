namespace Base2art.CiCd.Coordinator.Web.UI
{
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Pages;
    using Models;
    using Pages;
    using Public.Endpoints;
    using Public.Resources;
    using ViewModels;
    using ViewModels.Partials;

    public class Layouts
    {
        private readonly JobService buildService;
        private readonly ICompanyService companyService;

        public Layouts(JobService buildService, ICompanyService companyService)
        {
            this.buildService = buildService;
            this.companyService = companyService;
        }

        public async Task<WebPage> WithLayoutAsync<T>(IWebPage<T> webPage, T model, ClaimsPrincipal principal)
        {
            return WebPage.Create(new Layout(), new LayoutModel
                                                {
                                                    Title = null,
                                                    MainPanel = Task.FromResult(WebPage.Create(webPage, model)),
                                                    Mode = LayoutMode.Wide,
                                                    HeaderData = await GetHeaderData(principal),
                                                    FooterData = new FooterModel
                                                                 {
                                                                     CompanyName = this.companyService.CompanyName()
                                                                 }
                                                });
        }

        public async Task<WebPage> WithLayoutAndPanels(
            Task<WebPage> mainPanel,
            Task<WebPage>[] leftPanels,
            Task<WebPage>[] rightPanels,
            ClaimsPrincipal principal)
        {
            return WebPage.Create(new Layout(), new LayoutModel
                                                {
                                                    Title = null,
                                                    MainPanel = mainPanel,
                                                    LeftPanels = leftPanels,
                                                    RightPanels = rightPanels,
                                                    Mode = LayoutMode.Center,
                                                    HeaderData = await GetHeaderData(principal),
                                                    FooterData = new FooterModel
                                                                 {
                                                                     CompanyName = this.companyService.CompanyName()
                                                                 }
                                                });
        }

        public Task<WebPage> WithLayoutAndPanels<T>(
            IWebPage<T> webPage,
            T model,
            Task<WebPage>[] leftPanels,
            Task<WebPage>[] rightPanels,
            ClaimsPrincipal principal)
        {
            return WithLayoutAndPanels(Task.FromResult(WebPage.Create(webPage, model)), leftPanels, rightPanels, principal);
        }

        public async Task<WebPage> WithLayoutAndLeftPanels<T>(
            IWebPage<T> webPage,
            T model,
            Task<WebPage>[] leftPanels,
            ClaimsPrincipal principal)
        {
            return WebPage.Create(new Layout(), new LayoutModel
                                                {
                                                    Title = null,
                                                    MainPanel = Task.FromResult(WebPage.Create(webPage, model)),
                                                    LeftPanels = leftPanels,
                                                    Mode = LayoutMode.Right,
                                                    HeaderData = await GetHeaderData(principal),
                                                    FooterData = new FooterModel
                                                                 {
                                                                     CompanyName = this.companyService.CompanyName()
                                                                 }
                                                });
        }

        public async Task<WebPage> WithLayoutAndRightPanels<T>(
            IWebPage<T> webPage,
            T model,
            Task<WebPage>[] rightPanels,
            ClaimsPrincipal principal)
        {
            return WebPage.Create(new Layout(), new LayoutModel
                                                {
                                                    Title = null,
                                                    MainPanel = Task.FromResult(WebPage.Create(webPage, model)),
                                                    RightPanels = rightPanels,
                                                    Mode = LayoutMode.Left,
                                                    HeaderData = await GetHeaderData(principal),
                                                    FooterData = new FooterModel
                                                                 {
                                                                     CompanyName = this.companyService.CompanyName()
                                                                 }
                                                });
        }

        private async Task<HeaderModel> GetHeaderData(ClaimsPrincipal principal)
        {
            var items = await this.buildService.FindJobs(
                                                         principal,
                                                         new JobSearchCriteria {State = JobState.Claimed | JobState.Pending | JobState.Working},
                                                         Pagination.FirstPage(20),
                                                         JobSort.DateCreatedAscending);

            return new HeaderModel
                   {
                       Role = principal.Identity.IsAuthenticated
                                  ? Role.Admin
                                  : Role.Anonymous,
                       Messages = items.Select(x => new MessageData
                                                    {
                                                        Text = x.ProjectName + " Building"
                                                    }).ToArray()
                   };
        }
    }
}

// public WebPage WithLayout<T>(IWebPage<T> webPage, T model, ClaimsPrincipal principal)
// {
//     return WebPage.Create(new Layout(), new LayoutModel
//                                         {
//                                             Title = null,
//                                             MainPanel = Task.FromResult(WebPage.Create(webPage, model)),
//                                             Mode = LayoutMode.Wide,
//                                             HeaderData = GetHeaderData(principal)
//                                         });
// }