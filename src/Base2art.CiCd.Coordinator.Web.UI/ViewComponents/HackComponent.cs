namespace Base2art.CiCd.Coordinator.Web.UI.ViewComponents
{
    using System.ComponentModel;
    using Razor.Api;

    public class HackComponent : Component
    {
        private readonly TemplateRoot helper;
        private readonly string content;
        private readonly string notes;

        public HackComponent(TemplateRoot helper, string content, string notes)
        {
            this.helper = helper;
            this.content = content;
            this.notes = notes;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                this.helper.WriteLiteral($"<!-- {this.notes} -->");
                this.helper.WriteLiteral(this.content.Trim());
            }
        }
    }
}