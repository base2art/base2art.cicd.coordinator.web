namespace Base2art.CiCd.Coordinator.Web.UI.ViewComponents
{
    using System;
    using System.ComponentModel;
    using Razor.Api;

    public class PagePanelHolder : Component
    {
        private readonly TemplateRoot helper;
        private readonly string content;

        public PagePanelHolder(TemplateRoot helper, string content)
        {
            this.helper = helper;
            this.content = content;
        }

        public IDisposable Header()
        {
            this.helper.WriteLiteral("<div style='grid-row: 1;'>");

            return new HackComponent(this.helper, "</div>", nameof(Header));
        }

        public IDisposable Body()
        {
            this.helper.WriteLiteral("<div style='grid-row: 2;'>");

            return new HackComponent(this.helper, "</div>", nameof(Body));
        }
        
        public IDisposable Footer()
        {
            this.helper.WriteLiteral("<div style='grid-row: 3;'>");

            return new HackComponent(this.helper, "</div>", nameof(this.Footer));
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                this.helper.WriteLiteral($"<!-- {nameof(PagePanelHolder)} -->");
                this.helper.WriteLiteral(this.content.Trim());
            }
        }
    }
}