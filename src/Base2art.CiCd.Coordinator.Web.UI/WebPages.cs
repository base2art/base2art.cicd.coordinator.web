namespace Base2art.CiCd.Coordinator.Web.UI
{
    using System.Threading.Tasks;
    using Base2art.Web.Pages;

    public static class WebPages
    {
        public static Task<WebPage> AsPageAsync(this IWebPage webPage)
        {
            return Task.FromResult(WebPage.Create(webPage));
        }

        public static Task<WebPage> AsPageAsync<T>(this IWebPage<T> webPage, T model)
        {
            return Task.FromResult(WebPage.Create(webPage, model));
        }

        public static WebPage AsPage(this IWebPage webPage)
        {
            return WebPage.Create(webPage);
        }

        public static WebPage AsPage<T>(this IWebPage<T> webPage, T model)
        {
            return WebPage.Create(webPage, model);
        }
    }
}