namespace Base2art.CiCd.Coordinator.Web.UI.Mappings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Models;
    using Pages.Controls;
    using Public.Resources;
    using ViewModels.Controls;

    public static class ProjectToTreeViewMapper
    {
        public static IEnumerable<TreeViewNode> MapToConcrete(this IReadOnlyList<IProject> projects)
        {
            List<TreeViewNode> items = new List<TreeViewNode>();
            var names = projects.Select(GroupName);

            var root = new TreeViewNode();
            BuildByGroup(0, root, new string[0], names.ToArray(), projects);
            return root.Children;
        }

        private static (string, string[], string) GroupName(IProject project)
        {
            var name = (project?.Name ?? "").Split("/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            var parts = name.Reverse().Skip(1).Reverse().Select(x => x?.Trim()).ToArray();
            var lastPart = name.Reverse().FirstOrDefault()?.Trim();

            return (parts.Length == 0 ? "Default Group" : string.Join(" / ", parts), parts, lastPart);
        }

        private static void BuildByGroup(int i, TreeViewNode parent, string[] strings, (string, string[], string)[] names, IReadOnlyList<IProject> projects)
        {
            var groups = new HashSet<string>();
            // var childProjects = new List<IProject>();

            foreach (var name in names)
            {
                var nameParts = name.Item2;
                // var nameParts = name.Split(new char[] {'/'}, StringSplitOptions.RemoveEmptyEntries);
                if (nameParts.Length <= i)
                {
                    // skip
                }
                else
                {
                    var current = nameParts[i];
                    if (AreEqual(strings, nameParts.Take(i).ToArray()))
                    {
                        groups.Add(current.Trim());
                    }
                }
            }

            foreach (var group in groups)
            {
                var newItem = new TreeViewNode
                              {
                                  Name = group,
                                  Href = BuildHref(strings, group)
                              };
                BuildByGroup(i + 1, newItem, strings.Concat(new[] {@group}).ToArray(), names, projects);

                parent.Children = parent.Children.Concat(new[] {newItem}).ToArray();
            }

            foreach (var project in projects)
            {
                var (main, parts, suffix) = GroupName(project);
                if (AreEqual(strings, parts))
                {
                    parent.Children = parent.Children
                                            .Concat(new TreeViewNode[]
                                                    {
                                                        new TreeViewNode
                                                        {
                                                            Name = suffix,
                                                            Href = $"/projects/{project.Id}/builds"
                                                        }
                                                    })
                                            .ToArray();
                }
            }
        }

        private static bool AreEqual(string[] strings, string[] take)
        {
            if (strings.Length != take.Length)
            {
                return false;
            }

            for (int i = 0; i < strings.Length; i++)
            {
                if (!string.Equals(strings[i].Trim(), take[i].Trim(), StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }
            }

            return true;
        }

        private static string BuildHref(string[] strings, string @group)
        {
            return $"#{string.Join("_", strings.Concat(new[] {@group}))}";
        }
    }
}