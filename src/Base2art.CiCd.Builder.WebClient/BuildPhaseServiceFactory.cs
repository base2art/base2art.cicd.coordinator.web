namespace Base2art.CiCd.Builder.WebClient
{
    using Base2art.WebClient;
    using Builder;
    using Coordinator.Repositories;
    using Servers.WebClient;

    public class BuildPhaseServiceFactory : ExtensionServiceFactory<IBuildPhaseService>, IBuildPhaseServiceFactory
    {
        private readonly IWebClient client;

        public BuildPhaseServiceFactory(IServerReadRepository serverService, IWebClient client) : base(serverService) 
            => this.client = client;

        protected override IBuildPhaseService Create(string serverUrl)
        {
            return new BuildServerServiceClient(serverUrl, this.client);
        }
    }
}