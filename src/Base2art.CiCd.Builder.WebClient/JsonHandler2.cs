namespace Base2art.CiCd.Builder.WebClient
{
    using System;
    using System.Threading.Tasks;
    using Base2art.WebClient.ResponseHandlers;

    public class JsonHandler2<T> : TextHandler
    {
        private readonly Func<T, Task> func;

        public JsonHandler2(Action<T> act) : this(FuncItAsync(act))
        {
        }

        public JsonHandler2(Func<T, Task> func) => this.func = func;

        public override Task Invoke(string value)
            => this.func(System.Text.Json.JsonSerializer.Deserialize<T>(
                                                                        value,
                                                                        new System.Text.Json.JsonSerializerOptions {PropertyNameCaseInsensitive = true}));

        public static Func<T1, Task> FuncItAsync<T1>(Action<T1> action)
        {
            return x =>
            {
                action(x);
                return Task.CompletedTask;
            };
        }
    }
}