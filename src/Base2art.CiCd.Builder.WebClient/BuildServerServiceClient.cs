﻿namespace Base2art.CiCd.Builder.WebClient
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.WebClient;
    using Base2art.WebClient.Json;
    using Base2art.WebClient.ResponseHandlers;
    using Base2art.WebClient.ResponseHandlers.Json;
    using Builder;
    using Resources;
    using Servers;

    public class BuildServerServiceClient : IBuildPhaseService
    {
        private readonly string baseUrl;

        private readonly IWebClient client;

        public BuildServerServiceClient(string baseUrl, IWebClient client)
        {
            this.baseUrl = baseUrl;
            this.client = client;
        }

        public async Task<SupportedFeature[]> GetSupportedFeatures(ClaimsPrincipal principal)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("/api/v1/supported-features")
                      .ToString();

            Console.WriteLine(url);
            List<SupportedFeature> returnValue = null;
            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Get)
                      .WithAuthorizationBearer(principal)
                      .WithResponseHandler()
                      .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<List<SupportedFeature>>(x => returnValue = x))
                      .Run();

            return returnValue?.ToArray() ?? new SupportedFeature[0];
        }

        public async Task<ProjectBuild> StartBuild(Guid buildId, string version, List<ProjectBuildContent> buildData, ClaimsPrincipal principal)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("api/v1/builds/{id}/{version}")
                      .WithUrlSegment("id", buildId.ToString("D"))
                      .WithUrlSegment("version", version ?? string.Empty)
                      .ToString();

            Console.WriteLine(url);
            ProjectBuild pb = new ProjectBuild();
            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Put)
                      .WithAuthorizationBearer(principal)
                      .WithJsonBody(buildData)
                      .WithResponseHandler()
                      .WithStatusHandler(HttpStatusCode.InternalServerError, new StringHandler(x => Console.WriteLine(x)))
                      .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<ProjectBuild>(build => pb = build))
                      //new JsonHandler<ProjectBuild>(x => pb = x))
                      .Run();

            return pb;
        }

        public async Task<ProjectBuild> GetBuild(Guid buildId, ClaimsPrincipal principal)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("api/v1/builds/{id}")
                      .WithUrlSegment("id", buildId.ToString("D"))
                      .ToString();

            Console.WriteLine(url);
            ProjectBuild pb = null;
            try
            {
                await this.client.CreateRequest(url)
                          .WithMethod(HttpMethod.Get)
                          .WithAuthorizationBearer(principal)
                          .WithResponseHandler()
                          .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<ProjectBuild>(x => pb = x))
                          .Run();
            }
            catch (HttpRequestException)
            {
                return null;
            }

            return pb;
        }

        public async Task<Dictionary<Guid, Artifact[]>> GetArtifacts(Guid buildId, ClaimsPrincipal principal)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("api/v1/builds/{id}/artifacts")
                      .WithUrlSegment("id", buildId.ToString("D"))
                      .ToString();

            Console.WriteLine(url);
            Dictionary<Guid, Artifact[]> pb = null;
            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Get)
                      .WithAuthorizationBearer(principal)
                      .WithResponseHandler()
                      .WithStatusHandler(HttpStatusCode.OK, new JsonHandler2<Dictionary<Guid, Artifact[]>>(x => pb = x))
                      .WithStatusHandler(HttpStatusCode.InternalServerError, new StringHandler(x => Console.WriteLine(x)))
                      .Run();

            return pb ?? new Dictionary<Guid, Artifact[]>();
            //pb.ToDictionary(x => x.Key, x => x.Value.Select<FileData, IFileData>(y => y)));
        }

//        public async Task Remove(Uri serverBaseUrl, Guid id)
        public async Task Remove(Guid buildId, ClaimsPrincipal principal)
        {
            var url = new WebClientUri(this.baseUrl)
                      .WithResourcePath("api/v1/builds/{id}")
                      .WithUrlSegment("id", buildId.ToString("D"))
                      .ToString();

            Console.WriteLine(url);
            await this.client.CreateRequest(url)
                      .WithMethod(HttpMethod.Delete)
                      .WithAuthorizationBearer(principal)
                      .WithResponseHandler()
                      .Run();
        }
    }
}

//            var projectBuild = contentLookup.Select(x => x.Key)
//                                            .Select(x => new ProjectBuildContent
//                                                         {
//                                                             SourceControlId = x,
//                                                             Files = contentLookup[x],
//                                                             BranchName = checkoutInfoLookup[x].Name,
//                                                             BranchHash = checkoutInfoLookup[x].Hash,
//                                                             BuildInstructions = buildInstructionLookup[x]
//                                                                                 .Select(y => new BuildInstruction
//                                                                                              {
//                                                                                                  Arguments = y.ExecutableArguments,
//                                                                                                  Executable = y.ExecutableFile
//                                                                                              })
//                                                                                 .ToArray()
//                                                         })
//                                            .ToList();
//        public async Task<IProjectBuild> Build(
//            Guid id,
//            string version,
//            IDictionary<Guid, BuildInstructionData[]> buildInstructionLookup,
//            IDictionary<Guid, byte[]> contentLookup,
//            IDictionary<Guid, SourceControlBranchData> checkoutInfoLookup)
//        {
//
////            while (!pb.State.In(BuildPhaseState.CompletedFail, BuildPhaseState.CompletedSuccess))
////            {
////                await Task.Delay(TimeSpan.FromSeconds(5));
////                pb = await this.GetBuild(id, p);
////            }
////
////            return new ProjectBuildImpl(pb);
//        }

//        async Task<IProjectBuild> IBuildServerServiceClient.GetBuild(Uri serverBaseUrl, Guid id)
//            => new ProjectBuildImpl(await this.GetBuild(serverBaseUrl, id));
//        private class ProjectBuildImpl : IProjectBuild
//        {
//            private readonly ProjectBuild pb;
//
//            public ProjectBuildImpl(ProjectBuild pb)
//            {
//                this.pb = pb;
//            }
//
//            public string Output => this.pb?.Output;
//            public string Error => this.pb?.Error;
//            public int State => (int) (this.pb?.State).GetValueOrDefault();
//        }

//        private class FileData : IFileData
//        {
//            public string RelativeName { get; set; }
//            public string Name { get; set; }
//            public byte[] Content { get; set; }
//        }

//
//        public Task<SupportedFeature[]> GetSupportedFeatures(ClaimsPrincipal principal) => throw new NotImplementedException();
//
//        public Task<Dictionary<Guid, Artifact[]>> GetArtifacts(Guid buildId, ClaimsPrincipal principal) => throw new NotImplementedException();
//
//        public Task<ProjectBuild> GetBuild(Guid buildId, ClaimsPrincipal principal) => throw new NotImplementedException();
//
//        public Task<ProjectBuild> StartBuild(Guid buildId, string version, List<ProjectBuildContent> buildData, ClaimsPrincipal principal) => throw new NotImplementedException();
//
//        public Task Remove(Guid buildId, ClaimsPrincipal principal) => throw new NotImplementedException();

//
//        public async Task<BuildLog> GetBuildLogs(string serverBaseUrl, Guid buildId)
//        {
//            var url = new WebClientUri(serverBaseUrl)
//                      .WithResourcePath("api/v1/builds/{id}/logs")
//                      .WithUrlSegment("id", buildId.ToString("D"))
//                      .ToString();
//
//            BuildLog logs = null;
//            await this.client.CreateRequest(url)
//                      .WithMethod(HttpMethod.Get)
//                      .WithResponseHandler()
//                      .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<BuildLog>(x => logs = x))
//                      .Run();
//
//            return logs;
//        }