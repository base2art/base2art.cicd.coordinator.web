﻿namespace Base2art.CiCd.Coordinator.ServerPlugins.Database
{
    using System;
    using System.Threading.Tasks;
    using Data;
    using DataStorage;
    using Models;
    using Repositories;

    public class BuildRepository : IBuildRepository
    {
        private readonly IDataStore store;

        public BuildRepository(IDataStoreFactory factory) //: base()
            => this.store = factory.Create("builds");

        public Task SetStatus(Guid buildId, BuildState buildState)
        {
            return this.store.Insert<build_state_v1>()
                       .Record(rs => rs.Field(x => x.id, buildId)
                                       .Field(x => x.state, (int) buildState)
                                       .Field(x => x.ticks, DateTime.UtcNow))
                       .Execute();
        }

        public async Task<BuildState> GetStatus(Guid buildId)
        {
            var item = await this.store.SelectSingle<build_state_v1>()
                                 .Where(rs => rs.Field(x => x.id, buildId, (x, y) => x == y))
                                 .OrderBy(rs => rs.Field(x => x.ticks, ListSortDirection.Descending))
                                 .Execute();

            if (item == null)
            {
                return BuildState.Unknown;
            }

            return (BuildState) item.state;
        }
    }
}