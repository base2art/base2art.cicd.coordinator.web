namespace Base2art.CiCd.Coordinator.ServerPlugins.Database
{
    using System;
    using System.Security.Cryptography;
    using System.Threading.Tasks;
    using Data;
    using DataStorage;
    using Models;
    using Repositories;
    using Threading.Tasks;

    public class KeyRepository : IKeyRepository
    {
        private readonly IDataStore store;

        public KeyRepository(IDataStoreFactory factory)
            => this.store = factory.Create("builds");

//        public async Task<string> GetLastVersion(Guid projectId, string branchIdentifier)
//        {
//            var items = await this.store.SelectSingle<version_v1>()
//                                  .Where(rs => rs.Field(x => x.project_id, projectId, (x, y) => x == y))
//                                  .OrderBy(rs => rs.Field(x => x.when, ListSortDirection.Descending))
//                                  .Execute();
//
//            return items?.version;
//        }
//
//        public Task SetLastVersion(Guid projectId, Guid jobId, string branchIdentifier, string version)
//        {
//            return this.store.Insert<version_v1>()
//                       .Record(rs => rs.Field(x => x.project_id, projectId)
//                                       .Field(x => x.version, version)
//                                       .Field(x => x.job_id, jobId)
//                                       .Field(x => x.branch, branchIdentifier)
//                                       .Field(x => x.when, DateTime.UtcNow))
//                       .Execute();
//        }

        public Task<IKey[]> GetAllKeys()
        {
            return this.store.Select<key_v1>()
//                       .Where(rs => rs.Field(x => x, projectId, (x, y) => x == y))
                       .OrderBy(rs => rs.Field(x => x.activation_date, ListSortDirection.Descending))
                       .Execute()
                       .Then()
                       .Select(this.CreateKey)
                       .Then()
                       .ToArray();
        }

        public Task<IKey[]> GetActiveKeys()
        {
            return this.store.Select<key_v1>()
                       .Where(rs => rs.Field(x => x.is_active, true, (x, y) => x == y))
                       .OrderBy(rs => rs.Field(x => x.activation_date, ListSortDirection.Descending))
                       .Execute()
                       .Then()
                       .Select(this.CreateKey)
                       .Then()
                       .ToArray();
        }

        public Task<IKey[]> GetArchivedKeys()
        {
            return this.store.Select<key_v1>()
                       .Where(rs => rs.Field(x => x.is_active, false, (x, y) => x == y))
                       .OrderBy(rs => rs.Field(x => x.activation_date, ListSortDirection.Descending))
                       .Execute()
                       .Then()
                       .Select(this.CreateKey)
                       .Then()
                       .ToArray();
        }

        public Task<IKey> GetKeyById(Guid keyId)
        {
            return this.store.SelectSingle<key_v1>()
                       .Where(rs => rs.Field(x => x.id, keyId, (x, y) => x == y))
                       .OrderBy(rs => rs.Field(x => x.activation_date, ListSortDirection.Descending))
                       .Execute()
                       .Then()
                       .Return(this.CreateKey);
        }

        public async Task<IKey> AddKey(string name, RSAParameters key)
        {
            var guid = Guid.NewGuid();
            await this.store.Insert<key_v1>()
                      .Record(rs => rs.Field(x => x.id, guid)
                                      .Field(x => x.name, name)
                                      .Field(x => x.activation_date, DateTime.UtcNow)
                                      .Field(x => x.is_active, true)
                                      .Field(x => x.d, key.D)
                                      .Field(x => x.dp, key.DP)
                                      .Field(x => x.dq, key.DQ)
                                      .Field(x => x.exponent, key.Exponent)
                                      .Field(x => x.inverseq, key.InverseQ)
                                      .Field(x => x.modulus, key.Modulus)
                                      .Field(x => x.p, key.P)
                                      .Field(x => x.q, key.Q))
                      .Execute();
            return await this.GetKeyById(guid);
        }

        public Task ArchiveKey(Guid keyId)
        {
            return this.store.Update<key_v1>()
                       .Set(rs => rs.Field(x => x.is_active, false))
                       .Where(rs => rs.Field(x => x.id, keyId, (x, y) => x == y))
                       .Execute();
        }

        private IKey CreateKey(key_v1 arg)
        {
            var myKey = new MyKey();
            myKey.Id = arg.id.GetValueOrDefault();
            myKey.Name = arg.name;
            myKey.IsActive = arg.is_active;
            myKey.D = arg.d;
            myKey.Exponent = arg.exponent;
            myKey.Modulus = arg.modulus;
            myKey.P = arg.p;
            myKey.Q = arg.q;
            myKey.DP = arg.dp;
            myKey.DQ = arg.dq;
            myKey.InverseQ = arg.inverseq;
            return myKey;
        }

        private class MyKey : IKey
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public bool IsActive { get; set; }
            public byte[] D { get; set; }
            public byte[] DP { get; set; }
            public byte[] DQ { get; set; }
            public byte[] Exponent { get; set; }
            public byte[] InverseQ { get; set; }
            public byte[] Modulus { get; set; }
            public byte[] P { get; set; }
            public byte[] Q { get; set; }
        }
    }
}