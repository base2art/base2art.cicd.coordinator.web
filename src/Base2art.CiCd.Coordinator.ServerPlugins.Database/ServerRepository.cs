﻿namespace Base2art.CiCd.Coordinator.ServerPlugins.Database
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.DataStorage.DataManipulation;
    using Base2art.Threading.Tasks;
    using Data;
    using Models;
    using Repositories;

    public class ServerRepository : IServerReadRepository, IServerWriteRepository
    {
        private readonly IDataStore store;

        public ServerRepository(IDataStoreFactory factory) //: base()
            => this.store = factory.Create("builds");

        public async Task<IServer[]> GetServers(ServerType? pluginType)
        {
            var query = this.store.Select<server_v1>();
            if (pluginType.HasValue)
            {
                query = query.Where(rs => rs.Field(x => x.server_type, (int) pluginType.Value, (x, y) => x == y));
            }

            return await QueryServers(query);
        }

        public async Task<IServer> GetServer(Guid serverId)
        {
            var server = await this.store.SelectSingle<server_v1>()
                                   .Where(rs => rs.Field(x => x.id, serverId, (x, y) => x == y))
                                   .Execute();

            var attributes = await this.store.Select<server_attributes_v1>()
                                       .Where(rs => rs.Field(x => x.server_id, serverId, (x, y) => x == y))
                                       .Execute();

            return MapToServer(server, attributes.ToArray());
        }

        public async Task<IServer[]> FindByFeature(Guid featureId)
        {
            {
                var serverIds = this.store.Select<server_attributes_v1>()
                                    .Distinct()
                                    .Fields(rs => rs.Field(x => x.server_id));
//                                    .Where(rs => rs.Field(x => x.feature_id, featureId, (x, y) => x == y));
                var servers = this.store.Select<server_v1>()
                                  .Where(rs => rs.FieldIn(x => x.id, serverIds));

                var debug = await QueryServers(servers);
//                Console.WriteLine(debug);
            }

            {
                var serverIds = this.store.Select<server_attributes_v1>()
                                    .Distinct()
                                    .Fields(rs => rs.Field(x => x.server_id))
                                    .Where(rs => rs.Field(x => x.feature_id, featureId, (x, y) => x == y));
                var servers = this.store.Select<server_v1>()
                                  .Where(rs => rs.FieldIn(x => x.id, serverIds));

                return await QueryServers(servers);
            }
        }

        public Task<IServerFeature[]> AllKnownFeatures()
        {
            return this.store.Select<server_attributes_v1>()
                       .Distinct()
                       .Execute()
                       .Then()
                       .Select<IServerFeature>(y => new ServerFeatureInternal
                                                    {
                                                        Id = y.feature_id,
                                                        DefaultData = y.default_data,
                                                        FullName = y.full_name,
                                                        SimpleName = y.simple_name
                                                    })
                       .Then()
                       .ToArray();
        }

        public async Task<IServer> AddServer(ServerType serverType, string name, string baseUrl, Guid? authenticationKeyId)
        {
            var id = Guid.NewGuid();
            var time = DateTime.UtcNow;
            await this.store.Insert<server_v1>()
                      .Record(rs => rs.Field(x => x.id, id)
                                      .Field(x => x.name, name)
                                      .Field(x => x.base_url, baseUrl)
                                      .Field(x => x.enabled, true)
                                      .Field(x => x.authentication_key_id, authenticationKeyId)
                                      .Field(x => x.server_type, (int) serverType)
                                      .Field(x => x.on_added, time))
                      .Execute();

            return new Server
                   {
                       Id = id,
                       Url = new Uri(baseUrl),
                       Name = name,
                       Enabled = true,
                       ServerType = serverType,
                       Modified = time,
                       AuthenticationKeyId = authenticationKeyId
                   };
        }

        public async Task<IServer> UpdateServer(Guid serverId, string name, string baseUrl, Guid? authenticationKeyId)
        {
            await this.store.Update<server_v1>()
                      .Set(rs => rs.Field(x => x.name, name)
                                   .Field(x => x.authentication_key_id, authenticationKeyId)
                                   .Field(x => x.base_url, baseUrl))
                      .Where(rs => rs.Field(x => x.id, serverId, (x, y) => x == y))
                      .Execute();

            return await this.GetServer(serverId);
        }

        public async Task DeleteServer(Guid serverId)
        {
            await this.store.Delete<server_v1>()
                      .Where(rs => rs.Field(x => x.id, serverId, (x, y) => x == y))
                      .Execute();

//            return await this.GetServer(serverId);
        }

        public async Task<IServer> SetFeatures(Guid serverId, ServerFeature[] features)
        {
            var date = DateTime.UtcNow;

            await this.store.Delete<server_attributes_v1>()
                      .Where(rs => rs.Field(x => x.server_id, serverId, (x, y) => x == y))
                      .Execute();

            await Task.Delay(TimeSpan.FromSeconds(3));
            var inserter = this.store.Insert<server_attributes_v1>();

            foreach (var feature in features)
            {
                inserter.Record(rs => rs.Field(x => x.default_data, feature.DefaultData)
                                        .Field(x => x.server_id, serverId)
                                        .Field(x => x.id, Guid.NewGuid())
                                        .Field(x => x.feature_id, feature.Id)
                                        .Field(x => x.full_name, feature.FullName)
                                        .Field(x => x.simple_name, feature.SimpleName)
                                        .Field(x => x.on_added, date));
            }

            await inserter.Execute();

            return await this.GetServer(serverId);
        }

        public async Task SetStatus(Guid serverId, bool enabled)
        {
            await this.store.Update<server_v1>()
                      .Set(rs => rs.Field(x => x.enabled, enabled))
                      .Where(rs => rs.Field(x => x.id, serverId, (x, y) => x == y))
                      .Execute();
        }

        private static async Task<IServer[]> QueryServers(IQuerySelect<server_v1> query)
        {
            var serversWithAttrs = await query.LeftJoin<server_attributes_v1>((r, l) => r.id == l.server_id)
                                              .Execute()
                                              .Then()
                                              .ToArray();

            var servers = serversWithAttrs.Select(x => x.Item1).GroupBy(x => x.id).Select(x => x.FirstOrDefault()).ToArray();
            var attributes = serversWithAttrs.Select(x => x.Item2).Where(x => x != null).ToArray();
            return servers.Select(x => MapToServer(x, attributes))
                          .Select<Server, IServer>(x => x)
                          .ToArray();
        }

        private static Server MapToServer(server_v1 x, server_attributes_v1[] serverAttributes)
        {
            if (x == null)
            {
                //  || serverAttributes?.Length == 0
                return null;
            }

            serverAttributes = serverAttributes ?? new server_attributes_v1[0];

            return new Server
                   {
                       Id = x.id,
                       Name = x.name,
                       Enabled = x.enabled,
                       Url = new Uri(x.base_url),
                       Features = serverAttributes.Where(y => y.server_id == x.id)
                                                  .Select(y => new ServerFeatureInternal
                                                               {
                                                                   Id = y.feature_id,
                                                                   DefaultData = y.default_data,
                                                                   FullName = y.full_name,
                                                                   SimpleName = y.simple_name,
                                                               })
                                                  .Select<ServerFeatureInternal, IServerFeature>(y => y)
                                                  .ToArray(),
//                       Features = x.features.Split(',').Select(y => y.Trim()).ToArray(),
                       Modified = x.on_added,
                       ServerType = (ServerType) x.server_type,
                       AuthenticationKeyId = x.authentication_key_id
                   };
        }

        private class Server : IServer
        {
            public string Name { get; set; }
            public Guid Id { get; set; }

            public bool Enabled { get; set; }
            public IServerFeature[] Features { get; set; }

            public Uri Url { get; set; }
            public Guid? AuthenticationKeyId { get; set; }
            public ServerType ServerType { get; set; }
            public DateTime Modified { get; set; }
        }

        private class ServerFeatureInternal : IServerFeature
        {
            public Dictionary<string, object> DefaultData { get; set; }
            public string SimpleName { get; set; }
            public string FullName { get; set; }

            IReadOnlyDictionary<string, object> IServerFeature.DefaultData => this.DefaultData;
            public Guid Id { get; set; }
        }
    }
}