namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;

    public class key_v1
    {
        public Guid? id { get; set; }
        public string name { get; set; }

        public byte[] d { get; set; }
        public byte[] dp { get; set; }
        public byte[] dq { get; set; }
        public byte[] exponent { get; set; }
        public byte[] inverseq { get; set; }
        public byte[] modulus { get; set; }
        public byte[] p { get; set; }
        public byte[] q { get; set; }
        public DateTime activation_date { get; set; }
        public bool is_active { get; set; }
    }
}