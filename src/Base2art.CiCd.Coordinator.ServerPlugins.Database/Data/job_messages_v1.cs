namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;
    using System.Collections.Generic;

    public interface job_messages_v1
    {
        Guid job_id { get; }
        string phase { get; }
        string message { get; }
        DateTime date { get; }
        Dictionary<string, object> data { get; }
    }
}