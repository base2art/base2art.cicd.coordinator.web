namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;

    public interface build_state_v1
    {
        Guid id { get; }

        int state { get; }

        DateTime ticks { get; }
    }
}