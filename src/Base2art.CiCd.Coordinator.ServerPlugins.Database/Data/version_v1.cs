namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;

    public interface version_v1
    {
        Guid project_id { get; }
        string branch { get; }
        string version { get; }
        DateTime when { get; }
        Guid job_id { get; }
    }
}