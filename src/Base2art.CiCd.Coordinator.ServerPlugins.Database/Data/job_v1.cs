namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;

    public interface job_v1
    {
        Guid id { get; }
        DateTime date_created { get; }
        DateTime? date_finished { get; }

        string user_name { get; }

        Guid project_id { get; }
        string project_name { get; }
        Guid source_control_branches_id { get; }
        int state { get; }
        Guid? build_id { get; }
        Guid? build_server_id { get; }
    }
}