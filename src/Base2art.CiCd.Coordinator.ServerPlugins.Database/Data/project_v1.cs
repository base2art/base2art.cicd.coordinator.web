namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;
    using System.Collections.Generic;

    public class project_v1
    {
        public Dictionary<string, object> versioning_data { get; set; }

        public int versioning_strategy { get; set; }

        public Guid id { get; set; }

        public string name { get; set; }

        public bool is_active { get; set; }

        public DateTime created { get; set; }
    }
}