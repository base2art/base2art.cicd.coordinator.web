namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;

    public class build_instruction_v1
    {
        public string file { get; set; }
        public string arguments { get; set; }
        public Guid id { get; set; }
        public Guid source_control_id { get; set; }
    }
}