namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;

    public class project_features_v1
    {
        public Guid project_id { get; set; }
        public Guid feature_id { get; set; }
    }
}