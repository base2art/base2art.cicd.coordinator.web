namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;
    using System.Collections.Generic;

    public class server_attributes_v1
    {
//        public string id { get; set; }
//        public string server_id { get; set; }
//        public string attribute_id { get; set; }
//        public string attribute_value { get; set; }

        public Guid id { get; set; }
        public Guid feature_id { get; set; }
        public Guid server_id { get; set; }

        public string simple_name { get; set; }
        public string full_name { get; set; }
        public Dictionary<string, object> default_data { get; set; }

        public DateTime on_added { get; set; }
    }
}