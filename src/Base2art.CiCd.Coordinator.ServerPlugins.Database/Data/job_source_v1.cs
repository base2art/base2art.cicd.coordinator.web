namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;

    public interface job_source_v1
    {
        Guid job_id { get; }
        string branch_name { get; }
        string branch_hash { get; }
        Guid source_control_id { get; }
    }
}