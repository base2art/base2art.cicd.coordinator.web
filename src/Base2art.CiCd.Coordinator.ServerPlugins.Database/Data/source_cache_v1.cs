namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;

    public class source_cache_v1
    {
        public string name { get; set; }
        public string hash { get; set; }
        public string log_data { get; set; }
        public DateTime created { get; set; }
        public Guid id { get; set; }
    }
}