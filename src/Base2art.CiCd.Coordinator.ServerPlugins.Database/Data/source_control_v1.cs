namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;
    using System.Collections.Generic;
    using Models;

    public class source_control_v1
    {
        public Guid id { get; set; }
        public Guid project_id { get; set; }
        public Guid type { get; set; }
        public Dictionary<string, object> data { get; set; }
    }
}