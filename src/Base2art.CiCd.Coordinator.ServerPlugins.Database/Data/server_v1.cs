namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;

    public class server_v1
    {
        public Guid id { get; set; }

//        public string features { get; set; }

        public string base_url { get; set; }

        public string name { get; set; }

        public bool enabled { get; set; }

        public int server_type { get; set; }

        public DateTime on_added { get; set; }
        
        public Guid? authentication_key_id { get; set; }
    }
}