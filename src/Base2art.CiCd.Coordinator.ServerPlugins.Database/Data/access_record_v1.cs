namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;
    using Models;

    public class access_record_v1
    {
        public Guid id { get; set; }
        public string object_type { get; set; }
        public int operation_type { get; set; }
        public int permission_mode { get; set; }
        public string user_name { get; set; }
    }
}