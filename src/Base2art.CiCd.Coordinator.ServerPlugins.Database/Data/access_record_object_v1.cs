namespace Base2art.CiCd.Coordinator.ServerPlugins.Database.Data
{
    using System;

    public class access_record_object_v1
    {
        public Guid id { get; set; }
        public string object_type { get; set; }
        public Guid object_id { get; set; }
        public int operation_type { get; set; }
//        public int permission_mode { get; set; }
        public string user_name { get; set; }
    }
}