namespace Base2art.CiCd.Coordinator.ServerPlugins.Database
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Data;
    using DataStorage;
    using Models;
    using Threading.Tasks;

    public class SecurityService : ISecurityService
    {
        private readonly IDataStore store;
        private readonly IJsonSerializer serializer;

        public SecurityService(IDataStoreFactory factory)
        {
            this.store = factory.Create("builds");
            this.serializer = new CamelCasingIndentedSimpleJsonSerializer();
        }

        public Task<IAccessData[]> GetAccessRecordsByUser(string username)
        {
            var items = Enum.GetValues(typeof(OperationType)).OfType<OperationType>()
                            .Select(x =>
                                        new AccessData
                                        {
                                            Id = new Guid("9E2BDD45-67DB-4C1E-ABEE-FF8191DD97CA"),
                                            ObjectType = Type("Base2art.CiCd.Coordinator.Web.Public.Resources.Server"),
                                            OperationType = x,
                                            PermissionMode = PermissionMode.Full
                                        }
                                   )
                            .Select<AccessData, IAccessData>(x => x)
                            .ToArray();
            return Task.FromResult(items);

            return this.store.Select<access_record_v1>()
                       .Where(rs => rs.Field(x => x.user_name, username, (x, y) => x == y))
                       .Execute()
                       .Then()
                       .Select(this.Map)
                       .Then()
                       .ToArray();
        }

        public Task<IRecordAccessData[]> GetAccessRecordsByUserAndObjectType(string username, Type objectType)
        {
            var items = Enum.GetValues(typeof(OperationType)).OfType<OperationType>()
                            .Select(x =>
                                        new RecordAccessData
                                        {
                                            Id = new Guid("9E2BDD45-67DB-4C1E-ABEE-FF8191DD97CA"),
                                            ObjectType = Type("Base2art.CiCd.Coordinator.Web.Public.Resources.Server"),
                                            OperationType = x,
                                            ObjectId = new Guid("53c0e6f4-b365-438b-89a4-1b9707d829be")
                                        }
                                   )
                            .Select<RecordAccessData, IRecordAccessData>(x => x)
                            .ToArray();
            return Task.FromResult(items);

            return this.store.Select<access_record_object_v1>()
                       .Where(rs => rs.Field(x => x.user_name, username, (x, y) => x == y))
                       .Execute()
                       .Then()
                       .Select(this.Map)
                       .Then()
                       .ToArray();
        }

        public Task<IAccessData[]> AddAccessRecord(string username, Type objectType, OperationType operationType) =>
            throw new NotImplementedException();

        public Task<IAccessData[]> AddAccessRecord(string username, Type objectType, OperationType operationType, Guid objectId) =>
            throw new NotImplementedException();

        public Task<IAccessData[]> DenyAccessRecord(string username, Type objectType, OperationType operationType) =>
            throw new NotImplementedException();

        public Task<IAccessData[]> DenyAccessRecord(string username, Type objectType, OperationType operationType, Guid objectId) =>
            throw new NotImplementedException();

        public Type Type(string typename)
        {
            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                var t = asm.GetType(typename, false, true);
                if (t != null)
                {
                    return t;
                }
            }

            throw new ArgumentOutOfRangeException();
        }

        private IRecordAccessData Map(access_record_object_v1 result)
        {
            return new RecordAccessData
                   {
                       Id = result.id,
                       ObjectId = result.object_id,
                       ObjectType = Type(result.object_type),
                       OperationType = (OperationType) result.operation_type,
                   };
        }

        private IAccessData Map(access_record_v1 result)
        {
            return new AccessData
                   {
                       Id = result.id,
                       ObjectType = Type(result.object_type),
                       OperationType = (OperationType) result.operation_type,
                       PermissionMode = (PermissionMode) result.permission_mode
                   };
        }

        private class RecordAccessData : IRecordAccessData
        {
            public Guid Id { get; set; }
            public Type ObjectType { get; set; }
            public Guid ObjectId { get; set; }
            public OperationType OperationType { get; set; }
        }

        private class AccessData : IAccessData
        {
            public Guid Id { get; set; }
            public Type ObjectType { get; set; }
            public PermissionMode PermissionMode { get; set; }
            public OperationType OperationType { get; set; }
        }
    }
}