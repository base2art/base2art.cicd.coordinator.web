﻿namespace Base2art.CiCd.Coordinator.ServerPlugins.Database
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Data;
    using Repositories;

    public class VersionRepository : IVersionRepository
    {
        private readonly IDataStore store;

        public VersionRepository(IDataStoreFactory factory)
            => this.store = factory.Create("builds");

        public async Task<string> GetLastVersion(Guid projectId, string branchIdentifier)
        {
            var items = await this.store.SelectSingle<version_v1>()
                                  .Where(rs => rs.Field(x => x.project_id, projectId, (x, y) => x == y))
                                  .OrderBy(rs => rs.Field(x => x.when, ListSortDirection.Descending))
                                  .Execute();

            return items?.version;
        }

        public Task SetLastVersion(Guid projectId, Guid jobId, string branchIdentifier, string version)
        {
            return this.store.Insert<version_v1>()
                       .Record(rs => rs.Field(x => x.project_id, projectId)
                                       .Field(x => x.version, version)
                                       .Field(x => x.job_id, jobId)
                                       .Field(x => x.branch, branchIdentifier)
                                       .Field(x => x.when, DateTime.UtcNow))
                       .Execute();
        }
    }
}