﻿namespace Base2art.CiCd.Coordinator.ServerPlugins.Database
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Xml.Schema;
    using Base2art.DataStorage;
    using Base2art.DataStorage.DataManipulation.Builders;
    using Base2art.Threading.Tasks;
    using Data;
    using Models;
    using Repositories;

    public class JobStoreRepository : IJobStoreRepository
    {
        private readonly IDynamicDataStore dynamicStore;
        private readonly IDataStore store;

        public JobStoreRepository(IDataStoreFactory factory, IDynamicDataStoreFactory dynamicDataStoreFactory)
        {
            this.dynamicStore = dynamicDataStoreFactory.Create("builds");
            this.store = factory.Create("builds");
        }

        public Task<IJobInfo[]> FindJobs(
            JobState? state,
            string branchHash,
            string branchName,
            Guid? projectId,
            int pageSize,
            int pageIndex,
            JobSort sort)
        {
            void AddFields(IWhereClauseBuilder<job_v1> rs)
            {
                if (projectId.HasValue)
                {
                    rs = rs.Field(x => x.project_id, projectId.Value, (x, y) => x == y);
                }

                if (state.HasValue)
                {
                    rs = rs.Field(x => x.state, (int) state.Value, (x, y) => x == y);
                }
            }

            var selector = this.store.Select<job_v1>()
                               .Where(AddFields)
                               .Limit(pageSize)
                               .Join<job_source_v1>((x, y) => x.id == y.job_id)
                               .LeftJoin<version_v1>((j, js, v) => j.id == v.job_id)
                               .LeftJoin<source_cache_v1>((j, js, v, sc) => js.branch_name == sc.name && js.branch_hash == sc.hash)
                               .Offset(pageIndex * pageSize)
                               .OrderBy1(rs => rs.Field(x => x.date_created, this.GetSort(sort)));

            if (!string.IsNullOrWhiteSpace(branchName) && !string.IsNullOrWhiteSpace(branchHash))
            {
                selector = selector.Where2(rs => rs.Field(x => x.branch_name, branchName, (x, y) => x == y)
                                                   .Field(x => x.branch_hash, branchHash, (x, y) => x == y));
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(branchName))
                {
                    selector = selector.Where2(rs => rs.Field(x => x.branch_name, branchName, (x, y) => x == y));
                }

                if (!string.IsNullOrWhiteSpace(branchHash))
                {
                    selector = selector.Where2(rs => rs.Field(x => x.branch_hash, branchHash, (x, y) => x == y));
                }
            }

            return selector.Execute()
                           .Then()
                           .Return(x => x.GroupBy(y => y.Item1.id))
                           .Then()
                           .Select(this.CreateJobModel)
                           .Then()
                           .Where(x => x != null)
                           .Then()
                           .ToArray();
        }

        public async Task<IJobInfo[]> FindMostRecentJobsByProjectAndBranch(JobState? searchDataState, int pageSize, int pageIndex, JobSort sort)
        {
            var sql = @"
SELECT j.id as id, j.date_created, j.project_id, j.state  --, js.branch_name,
    FROM job_v1 j
	Join job_source_v1 js
	  ON j.id = js.job_id

    LEFT JOIN (
	    SELECT j.id as job_id, js.branch_name, j.date_created, j.project_id
		    FROM job_v1 j
		    Join job_source_v1 js
		      ON j.id = js.job_id
    ) g
	  ON j.project_id = g.project_id AND js.branch_name = g.branch_name AND j.date_created < g.date_created
    WHERE g.job_id IS NULL
	AND j.state = COALESCE(@state, j.state);
";
            
            var result = await this.dynamicStore.Query(sql)
//                                   .WithParameters(new Dictionary<string, object> {{"state", searchDataState.Value}})
                                   .WithParameters(new {state = searchDataState})
                                   .Execute<job_v1>()
                                   .Then()
                                   .ToArray();

            if (result.Length == 0)
            {
                return new IJobInfo[0];
            }

            var selector = this.store.Select<job_v1>()
                               .Where(rs => rs.FieldIn(x => x.id, result.Select(x=>x.id).ToArray()))
                               .Limit(pageSize)
                               .Join<job_source_v1>((x, y) => x.id == y.job_id)
                               .LeftJoin<version_v1>((j, js, v) => j.id == v.job_id)
                               .LeftJoin<source_cache_v1>((j, js, v, sc) => js.branch_name == sc.name && js.branch_hash == sc.hash)
                               .Offset(pageIndex * pageSize)
                               .OrderBy1(rs => rs.Field(x => x.date_created, this.GetSort(sort)));

            return await selector.Execute()
                                 .Then()
                                 .Return(x => x.GroupBy(y => y.Item1.id))
                                 .Then()
                                 .Select(this.CreateJobModel)
                                 .Then()
                                 .Where(x => x != null)
                                 .Then()
                                 .ToArray();
        }

        public async Task SetState(Guid jobId, JobState state)
        {
            void update(ISetListBuilder<job_v1> rs)
            {
                rs.Field(x => x.state, (int) state);
                if (state == JobState.CompletedFail || state == JobState.CompletedSuccess)
                {
                    rs.Field(x => x.date_finished, DateTime.UtcNow);
                }
            }

            await this.store.Update<job_v1>()
                      .Set(rs => update(rs))
                      .Where(rs => rs.Field(x => x.id, jobId, (x, y) => x == y))
                      .Execute();
        }

        public Task SetBuildInfo(Guid jobId, Guid buildServerId, Guid buildId)
        {
            return this.store.Update<job_v1>()
                       .Set(rs => rs.Field(x => x.build_id, buildId)
                                    .Field(x => x.build_server_id, buildServerId))
                       .Where(rs => rs.Field(x => x.id, jobId, (x, y) => x == y))
                       .Execute();
        }

        public async Task CreateJob(
            Guid id,
            Guid projectId,
            string projectName,
            string userName,
            Dictionary<Guid, SourceControlBranchData> sourceControl)
        {
            var dateCreated = DateTime.UtcNow;

            Guid branchHashId = string.Join("__::__", sourceControl.Select(x => x.Value.Name))
                                      .HashAsGuid();
            await this.store.Insert<job_v1>()
                      .Record(rs => rs.Field(x => x.state, (int) JobState.Pending)
                                      .Field(x => x.id, id)
                                      .Field(x => x.project_name, projectName)
                                      .Field(x => x.user_name, userName)
                                      .Field(x => x.date_created, dateCreated)
                                      .Field(x => x.source_control_branches_id, branchHashId)
                                      .Field(x => x.project_id, projectId))
                      .Execute();

            var inserter = this.store.Insert<job_source_v1>();
            foreach (var data in sourceControl)
            {
                inserter.Record(rs => rs.Field(x => x.job_id, id)
                                        .Field(x => x.source_control_id, Guid.NewGuid())
                                        .Field(x => x.branch_name, data.Value.Name)
                                        .Field(x => x.branch_hash, data.Value.Hash));
            }

            await inserter.Execute();
        }

        public async Task<IJobInfo[]> FindLatestJobs(SourceControlBranchData[] sourceControlBranchData)
        {
            var latestJobIds = await this.store.Select<job_v1>()
                                         .Join<job_source_v1>((j, js) => j.id == js.job_id)
                                         .LeftJoin<job_v1>((j, js, jo)
                                                               => j.project_id == jo.project_id
                                                                  && j.source_control_branches_id == jo.source_control_branches_id
                                                                  && jo.date_created > j.date_created)
                                         .LeftJoin<version_v1>((j, js, jo, v) => j.id == v.job_id)
                                         .LeftJoin<source_cache_v1>((j, js, jo, v, sc) => js.branch_name == sc.name && js.branch_hash == sc.hash)
                                         .Where2(rs => rs.FieldIn(x => x.branch_name, sourceControlBranchData.Select(x => x.Name)))
                                         .Where3(rs => rs.Field(x => x.date_created, null, (x, y) => x == y))
                                         .Execute();

            var groups = latestJobIds.GroupBy(x => x.Item1.id);
            return groups.Select(s => this.CreateJobModel(s.Select(x => Tuple.Create(x.Item1, x.Item2, x.Item4, x.Item5)))).ToArray();
        }

        public Task AddMessage(Guid buildId, JobPhase phase, string message, Dictionary<string, object> data)
        {
            return this.store.Insert<job_messages_v1>()
                       .Record(rs => rs.Field(x => x.job_id, buildId)
                                       .Field(x => x.phase, phase.ToString("G"))
                                       .Field(x => x.message, message)
                                       .Field(x => x.data, data)
                                       .Field(x => x.date, DateTime.UtcNow))
                       .Execute();
        }

        public async Task<IJobInfo> GetJobInfo(Guid jobId)
        {
            var selector = await this.store.Select<job_v1>()
                                     .Where(rs => rs.Field(x => x.id, jobId, (x, y) => x == y))
                                     .Join<job_source_v1>((x, y) => x.id == y.job_id)
                                     .LeftJoin<version_v1>((x, y, v) => x.id == v.job_id)
                                     .LeftJoin<source_cache_v1>((x, y, v, sc) => y.branch_name == sc.name && y.branch_hash == sc.hash)
                                     .Execute();

            return this.CreateJobModel(selector);
        }

        public Task<IJobMessage[]> GetJobMessages(Guid jobId)
        {
            return this.store.Select<job_messages_v1>()
                       .Where(rs => rs.Field(x => x.job_id, jobId, (x, y) => x == y))
                       .OrderBy(rs => rs.Field(x => x.date, ListSortDirection.Ascending))
                       .Execute()
                       .Then()
                       .Select(x => MapJobMessageModel(x))
                       .Then()
                       .ToArray();
        }

        private IJobInfo CreateJobModel(IEnumerable<Tuple<job_v1, job_source_v1, version_v1, source_cache_v1>> dbJob)
        {
            var first = dbJob.FirstOrDefault(x => x.Item1.project_id != Guid.Empty);
            if (first == null)
            {
                return null;
            }

            var jobV1 = first.Item1;
            return new JobInfoModel
                   {
                       Id = jobV1.id,
                       ProjectId = jobV1.project_id,
                       ProjectName = jobV1.project_name,
                       OriginatingUser = jobV1.user_name,
                       Build = this.CreateGuid(jobV1.build_id),
                       BuildServer = this.CreateGuid(jobV1.build_server_id),
                       State = (JobState) jobV1.state,
                       DateCreated = jobV1.date_created,
                       DateFinished = jobV1.date_finished,
                       Version = first?.Item3?.version,
                       SourceCode = dbJob.GroupBy(x => x.Item2.source_control_id)
                                         .Select(x => x.FirstOrDefault().Item2)
                                         .ToDictionary(
                                                       x => x.source_control_id,
                                                       x => MapBranchDataModel(x, first.Item4))
                   };
        }

        private Guid CreateGuid(Guid? id) => id.GetValueOrDefault();

        private static IJobMessage MapJobMessageModel(job_messages_v1 x)
            => new JobMessageModel
               {
                   Message = x.message,
                   Phase = x.phase,
                   Data = x.data
               };

        private ListSortDirection GetSort(JobSort jobSort)
            => jobSort == JobSort.DateCreatedDescending
                   ? ListSortDirection.Descending
                   : ListSortDirection.Ascending;

        private static IBranchLogData MapBranchDataModel(job_source_v1 x, source_cache_v1 sourceControl)
        {
            var camelCasingSimpleJsonSerializer = new CamelCasingSimpleJsonSerializer();
            var logData = camelCasingSimpleJsonSerializer.Deserialize<SourceControlBranchCommitLogData[]>(sourceControl.log_data ?? "[]");

            return new BranchDataModel
                   {
                       Name = x.branch_name,
                       Hash = x.branch_hash,
                       CommitLogs = logData.Select(y => new CommitLogDataModel
                                                        {
                                                            Id = y.Id,
                                                            Author = y.Author,
                                                            Message = y.Message,
                                                            When = y.When,
                                                            FilesAdded = y.FilesAdded,
                                                            FilesRemoved = y.FilesRemoved,
                                                            FilesChanged = y.FilesChanged,
                                                        })
                                           .Select<CommitLogDataModel, ICommitLogData>(z => z)
                                           .ToArray()
                   };
        }

        private class BranchDataModel : IBranchLogData
        {
            public string Name { get; set; }
            public string Hash { get; set; }
            public ICommitLogData[] CommitLogs { get; set; }
        }

        private class CommitLogDataModel : ICommitLogData
        {
            public string Id { get; set; }
            public DateTime When { get; set; }
            public string Author { get; set; }
            public string Message { get; set; }
            public string[] FilesAdded { get; set; }
            public string[] FilesRemoved { get; set; }
            public string[] FilesChanged { get; set; }
        }

        private class JobInfoModel : IJobInfo
        {
            public Guid Id { get; set; }
            public Guid ProjectId { get; set; }
            public JobState State { get; set; }
            public Guid? BuildServer { get; set; }
            public Guid? Build { get; set; }
            public IReadOnlyDictionary<Guid, IBranchLogData> SourceCode { get; set; }
            public DateTime DateCreated { get; set; }
            public DateTime? DateFinished { get; set; }
            public string OriginatingUser { get; set; }
            public string ProjectName { get; set; }
            public string Version { get; set; }
        }

        private class JobMessageModel : IJobMessage
        {
            public string Message { get; set; }
            public string Phase { get; set; }
            public IReadOnlyDictionary<string, object> Data { get; set; }
        }
    }
}