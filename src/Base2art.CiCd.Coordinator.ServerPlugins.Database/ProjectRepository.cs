namespace Base2art.CiCd.Coordinator.ServerPlugins.Database
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http.Headers;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.DataStorage.DataManipulation;
    using Data;
    using Models;
    using Repositories;

    public class ProjectRepository : IProjectRepository
    {
        private readonly IDataStore store;
        private readonly IJsonSerializer serializer;

        public ProjectRepository(IDataStoreFactory factory)
        {
            this.store = factory.Create("builds");
            this.serializer = new CamelCasingIndentedSimpleJsonSerializer();
        }

        public async Task<IProject[]> GetAll()
        {
            var results = await this.store.Select<project_v1>()
                                    .LeftJoin<source_control_v1>((p, sc) => p.id == sc.project_id)
                                    .LeftJoin<build_instruction_v1>((p, sc, bi) => sc.id == bi.source_control_id)
                                    .LeftJoin<project_features_v1>((p, sc, bi, pf) => p.id == pf.project_id)
                                    .Execute();

            return this.Map(results);
        }

        public async Task<IProject[]> GetAllActive()
        {
            var results = await this.store.Select<project_v1>()
                                    .LeftJoin<source_control_v1>((p, sc) => p.id == sc.project_id)
                                    .LeftJoin<build_instruction_v1>((p, sc, bi) => sc.id == bi.source_control_id)
                                    .LeftJoin<project_features_v1>((p, sc, bi, pf) => p.id == pf.project_id)
                                    .Where1(rs => rs.Field(x => x.is_active, true, (b, b1) => b == b1))
                                    .Execute();

            return this.Map(results);
        }

        public async Task<IProject> GetProjectById(Guid projectId)
        {
            var results = await this.store.Select<project_v1>()
                                    .LeftJoin<source_control_v1>((p, sc) => p.id == sc.project_id)
                                    .LeftJoin<build_instruction_v1>((p, sc, bi) => sc.id == bi.source_control_id)
                                    .LeftJoin<project_features_v1>((p, sc, bi, pf) => p.id == pf.project_id)
                                    .Where1(rs => rs.Field(x => x.id, projectId, (b, b1) => b == b1))
                                    .Execute();

            return this.Map(results).FirstOrDefault();
        }

        public Task DeactivateProject(Guid projectId)
        {
            return this.store.Update<project_v1>()
                       .Set(rs => rs.Field(x => x.is_active, false))
                       .Where(rs => rs.Field(x => x.id, projectId, (b, b1) => b == b1))
                       .Execute();
        }

        public Task CreateProject(
            Guid projectId,
            string name,
//            string[] features,
            VersioningStrategy versioningStrategy,
            Dictionary<string, string> versioningData)
        {
            return this.store.Insert<project_v1>()
                       .Record(rs => rs.Field(x => x.id, projectId)
                                       .Field(x => x.created, DateTime.UtcNow)
                                       .Field(x => x.name, name)
                                       .Field(x => x.is_active, true)
//                                       .Field(x => x.required_features_list, this.serializer.Serialize(features))
                                       .Field(x => x.versioning_strategy, (int) versioningStrategy)
                                       .Field(x => x.versioning_data, versioningData.ToDictionary(x => x.Key, x => (object) x.Value)))
                       .Execute();
        }

        public Task UpdateProject(
            Guid projectId,
            string name,
//            string[] features,
            VersioningStrategy versioningStrategy,
            Dictionary<string, string> versioningData)
        {
            return this.store.Update<project_v1>()
                       .Set(rs => rs.Field(x => x.is_active, true)
                                    .Field(x => x.name, name)
//                                    .Field(x => x.required_features_list, this.serializer.Serialize(features))
                                    .Field(x => x.versioning_strategy, (int) versioningStrategy)
                                    .Field(x => x.versioning_data, versioningData.ToDictionary(x => x.Key, x => (object) x.Value)))
                       .Where(rs => rs.Field(x => x.id, projectId, (b, b1) => b == b1))
                       .Execute();
        }

        public async Task UpdateSourceControl(
            Guid projectId,
            Guid sourceControlId,
            Guid scSourceControlType,
            Dictionary<string, object> scSourceControlParameters,
            BuildInstructionData[] scInstructions)
        {
            await this.RemoveSourceControl(projectId, sourceControlId);
            await this.AddSourceControl(projectId, sourceControlId, scSourceControlType, scSourceControlParameters, scInstructions);
        }

        public async Task RemoveSourceControl(Guid projectId, Guid sourceControlId)
        {
            await this.store.Delete<build_instruction_v1>()
                      .Where(rs => rs.Field(x => x.source_control_id, sourceControlId, (x, y) => x == y))
                      .Execute();

            await this.store.Delete<source_control_v1>()
                      .Where(rs => rs.Field(x => x.id, sourceControlId, (x, y) => x == y))
                      .Execute();
        }

        public Task AddSourceControl(
            Guid projectId,
            Guid sourceControlType,
            Dictionary<string, object> sourceControlParameters,
            BuildInstructionData[] instructions)
        {
            return this.AddSourceControl(projectId, Guid.NewGuid(), sourceControlType, sourceControlParameters, instructions);
        }

        public Task AddFeature(Guid projectId, Guid featureId)
        {
            return this.store.Insert<project_features_v1>()
                       .Record(rs => rs.Field(x => x.project_id, projectId)
                                       .Field(x => x.feature_id, featureId))
                       .Execute();
        }

        public Task RemoveFeature(Guid projectId, Guid featureId)
        {
            return this.store.Delete<project_features_v1>()
                       .Where(rs => rs.Field(x => x.project_id, projectId, (x, y) => x == y)
                                      .Field(x => x.feature_id, featureId, (x, y) => x == y))
                       .Execute();
        }

        public async Task SetFeatures(Guid projectId, Guid[] features)
        {
            await this.store.Delete<project_features_v1>()
                      .Where(rs => rs.Field(x => x.project_id, projectId, (x, y) => x == y))
                      .Execute();

            IQueryInsert<project_features_v1> insert = this.store.Insert<project_features_v1>();
            foreach (var featureId in features)
            {
                insert.Record(rs => rs.Field(x => x.project_id, projectId)
                                      .Field(x => x.feature_id, featureId));
            }

            await insert.Execute();
        }

        private async Task AddSourceControl(
            Guid projectId,
            Guid scId,
            Guid sourceControlType,
            Dictionary<string, object> sourceControlParameters,
            BuildInstructionData[] instructions)
        {
            await this.store.Insert<source_control_v1>()
                      .Record(rs => rs.Field(x => x.id, scId)
                                      .Field(x => x.project_id, projectId)
                                      .Field(x => x.type, sourceControlType)
                                      .Field(x => x.data, sourceControlParameters))
                      .Execute();

            foreach (var instruction in instructions)
            {
                var iid = Guid.NewGuid();

                var data = this.serializer.Serialize(instruction.ExecutableArguments ?? new string[0]);

                await this.store.Insert<build_instruction_v1>()
                          .Record(rs => rs.Field(x => x.source_control_id, scId)
                                          .Field(x => x.id, iid)
                                          .Field(x => x.file, instruction.ExecutableFile)
                                          .Field(x => x.arguments, data))
                          .Execute();
            }
        }

        private IProject[] Map(IEnumerable<Tuple<project_v1, source_control_v1, build_instruction_v1, project_features_v1>> results)
        {
            var projectGroupings = results.GroupBy(x => x.Item1.id);
            List<IProject> projects = new List<IProject>();

            foreach (var projectGrouping in projectGroupings)
            {
                var projectGroupData = projectGrouping.First();

                var projectData = projectGroupData.Item1;
                var projectModel = BuildProjectModel(projectData);

                projectModel.RequiredFeatures = BuildProjectModelRequiredFeatures(projectGrouping);
                projectModel.SourceControl = BuildProjectModelSourceControl(projectGrouping);
                projects.Add(projectModel);
            }

            return projects.ToArray();
        }

        private SourceControlInfo[] BuildProjectModelSourceControl(
            IEnumerable<Tuple<project_v1, source_control_v1, build_instruction_v1, project_features_v1>> projectGrouping)
        {
            var scGrouping = projectGrouping.Where(x => x.Item2 != null && x.Item2.id != Guid.Empty)
                                            .GroupBy(x => x.Item2.id);

            return scGrouping.Select(scGroup =>
            {
                return this.BuildSourceControlInfo(
                                                   scGroup.First().Item2,
                                                   scGroup.Select(x => x.Item3)
                                                          .Where(x => x.id != Guid.Empty)
                                                          .ToArray());
            }).ToArray();
        }

        private SourceControlInfo BuildSourceControlInfo(
            source_control_v1 sourceControlV1,
            IEnumerable<build_instruction_v1> scGroup)
        {
            var sourceControlInfo = new SourceControlInfo();
            sourceControlInfo.Id = sourceControlV1.id;
            sourceControlInfo.SourceControlType = sourceControlV1.type;

            var dataStr = this.serializer.Serialize(sourceControlV1.data);
            sourceControlInfo.SourceControlData = this.serializer.Deserialize<Dictionary<string, string>>(dataStr);

            sourceControlInfo.BuildInstructions = scGroup.GroupBy(x => x.id).Select(x => x.First()).Select(x => new BuildInstruction
                                                             {
                                                                 ExecutableFile = x.file,
                                                                 ExecutableArguments =
                                                                     this.serializer
                                                                         .Deserialize<string[]
                                                                         >(x.arguments ?? "[]"),
                                                             })
                                                         .ToArray();

            return sourceControlInfo;
        }

        private static Guid[] BuildProjectModelRequiredFeatures(
            IEnumerable<Tuple<project_v1, source_control_v1, build_instruction_v1, project_features_v1>> projectGrouping)
        {
            return projectGrouping.Select(x => x.Item4.feature_id).Distinct().ToArray();
        }

        private static ProjectModel BuildProjectModel(project_v1 projectData)
        {
            var projectModel = new ProjectModel();

            projectModel.Id = projectData.id;
            projectModel.Created = projectData.created;
            projectModel.Name = projectData.name;
            projectModel.IsActive = projectData.is_active;

            projectModel.Versioning = new Versioning
                                      {
                                          Strategy = (VersioningStrategy) projectData.versioning_strategy,
                                          DefaultData = projectData.versioning_data.ToDictionary(x => x.Key, x => x.Value?.ToString())
                                      };
            return projectModel;
        }

        private class ProjectModel : IProject
        {
            public SourceControlInfo[] SourceControl { get; set; }
            public Guid Id { get; set; }
            public string Name { get; set; }

            IEnumerable<ISourceControlInfo> IProject.SourceControl => this.SourceControl;

            public Guid[] RequiredFeatures { get; set; }

            public bool IsActive { get; set; }

            public DateTime Created { get; set; }

            public Versioning Versioning { get; set; }
            IVersioning IProject.Versioning => this.Versioning;
        }

        private class Versioning : IVersioning
        {
            public VersioningStrategy Strategy { get; set; }
            public Dictionary<string, string> DefaultData { get; set; }

            IReadOnlyDictionary<string, string> IVersioning.DefaultData => this.DefaultData;
        }

        private class SourceControlInfo : ISourceControlInfo
        {
            public Dictionary<string, string> SourceControlData { get; set; }

            public BuildInstruction[] BuildInstructions { get; set; }
            public Guid Id { get; set; }

            public Guid SourceControlType { get; set; }
            IReadOnlyDictionary<string, string> ISourceControlInfo.SourceControlData => this.SourceControlData;

            IBuildInstruction[] ISourceControlInfo.BuildInstructions => this.BuildInstructions;
        }

        private class BuildInstruction : IBuildInstruction
        {
            public string ExecutableFile { get; set; }
            public string[] ExecutableArguments { get; set; }
        }
    }
}