namespace Base2art.CiCd.Coordinator.ServerPlugins.Database
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Data;

    public class ProvisionDatabaseTask
    {
        private readonly IDbms dbms;
        private readonly IDataStore store;

        public ProvisionDatabaseTask(IDbmsFactory dbmsFactory, IDataStoreFactory store)
        {
            this.dbms = dbmsFactory.Create("builds");
            this.store = store.Create("builds");
        }

        public Task ExecuteAsync()
        {
            var types = this.GetAllTypesInNamespace(typeof(ProvisionDatabaseTask).Assembly, typeof(build_instruction_v1).Namespace);

            foreach (var type in types)
            {
                Console.WriteLine(type.Name);
                var method = this.GetType().GetMethod(nameof(this.RunFor), BindingFlags.NonPublic | BindingFlags.Instance);
                var methodGeneric = method.MakeGenericMethod(type);
                methodGeneric.Invoke(this, null);
            }

            return Task.CompletedTask;
        }

        private Type[] GetAllTypesInNamespace(Assembly asm, string @namespace)
        {
            var types = asm.GetExportedTypes();

            return types.Where(x => string.Equals(x.Namespace, @namespace, StringComparison.OrdinalIgnoreCase)).ToArray();
        }

        private void RunFor<T>()
        {
#pragma warning disable 1998
            this.dbms.CreateOrUpdateTable<T>().Execute().RunAway(async x => Console.WriteLine(x.Message + "" + x.StackTrace));
#pragma warning restore 1998
        }
    }
}