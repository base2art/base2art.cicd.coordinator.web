namespace Base2art.CiCd.Coordinator.ServerPlugins.Database
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.Threading.Tasks;
    using Data;
    using Models;
    using Repositories;

    public class SourceRepository : ISourceRepository
    {
        private readonly IDataStore store;
        private readonly IJsonSerializer serializer;

        public SourceRepository(IDataStoreFactory factory)
        {
            this.store = factory.Create("builds");
            this.serializer = new CamelCasingSimpleJsonSerializer();
        }

        public Task<Guid[]> GetProjectsMissingBranches()
        {
            return this.store.Select<source_control_v1>()
                       .LeftJoin<source_cache_v1>((x, y) => x.id == y.id)
                       .Where2(rs => rs.Field(x => x.id, null, (x, y) => x == y))
                       .Execute()
                       .Then()
                       .Select(x => x.Item1.project_id)
                       .Then()
                       .ToArray();
        }

        public Task<IBranchLogData[]> GetBranches(Guid sourceControlId)
        {
            return this.store.Select<source_cache_v1>()
                       .Where(rs => rs.Field(x => x.id, sourceControlId, (x, y) => x == y))
                       .Execute()
                       .Then()
                       .Select(this.MapTo)
                       .Then()
                       .Select<IBranchLogData>(x => x)
                       .Then()
                       .ToArray();
        }

        public Task<IBranchLogData[]> GetBranches(Guid sourceControlId, DateTime olderThan)
        {
            return this.store.Select<source_cache_v1>()
                       .Where(rs => rs.Field(x => x.id, sourceControlId, (x, y) => x == y)
                                      .Field(x => x.created, olderThan, (x, y) => x < y))
                       .Execute()
                       .Then()
                       .Select(this.MapTo)
                       .Then()
                       .Select<IBranchLogData>(x => x)
                       .Then()
                       .ToArray();
        }

        public async Task<IReadOnlyDictionary<Guid, IBranchLogData[]>> GetBranches(Guid[] sourceControlIds)
        {
            var sources = await this.store.Select<source_cache_v1>()
                                    .Where(rs => rs.FieldIn(x => x.id, sourceControlIds))
                                    .Execute();

            return this.MapTo(sources);
        }

        public async Task<IReadOnlyDictionary<Guid, IBranchLogData[]>> GetBranches(Guid[] sourceControlIds, DateTime olderThan)
        {
            var sources = await this.store.Select<source_cache_v1>()
                                    .Where(rs => rs.Field(x => x.created, olderThan, (x, y) => x < y)
                                                   .FieldIn(x => x.id, sourceControlIds))
                                    .Execute();

            return this.MapTo(sources);
        }

        private IReadOnlyDictionary<Guid, IBranchLogData[]> MapTo(IEnumerable<source_cache_v1> sources)
        {
            return sources.GroupBy(x => x.id)
                          .ToDictionary(
                                        x => x.Key,
                                        x => x.Select(y => this.MapTo(y))
                                              .Select<BranchDataModel, IBranchLogData>(y => y)
                                              .ToArray());
        }

        private BranchDataModel MapTo(source_cache_v1 y)
        {
            return new BranchDataModel
                   {
                       Name = y.name,
                       Hash = y.hash,
                       CommitLogs = this.serializer
                                        .Deserialize<SourceControlBranchCommitLogData[]>(y.log_data ?? "[]")
                                        .Select(z => new CommitLogData
                                                     {
                                                         Id = z.Id,
                                                         Author = z.Author,
                                                         Message = z.Message,
                                                         When = z.When,
                                                         FilesRemoved = z.FilesRemoved,
                                                         FilesAdded = z.FilesAdded,
                                                         FilesChanged = z.FilesChanged,
                                                     })
                                        .Select<CommitLogData, ICommitLogData>(z => z)
                                        .ToArray()
                   };
        }

        public async Task<bool> SetBranches(Guid sourceControlId, SourceControlBranchData[] branches)
        {
            var oldCacheItems = await this.store.Select<source_cache_v1>()
                                          .Where(rs => rs.Field(x => x.id, sourceControlId, (x, y) => x == y))
                                          .Execute()
                                          .Then()
                                          .ToArray();

            if (oldCacheItems.Length == branches.Length)
            {
                var bns = branches.Select(x => x.Name + ":" + x.Hash).Select(x => x.ToUpperInvariant()).ToImmutableSortedSet();
                var ois = oldCacheItems.Select(x => x.name + ":" + x.hash).Select(x => x.ToUpperInvariant()).ToImmutableSortedSet();
                if (bns.SetEquals(ois))
                {
                    await this.store.Update<source_cache_v1>()
                              .Where(rs => rs.Field(x => x.id, sourceControlId, (x, y) => x == y))
                              .Set(rs => rs.Field(x => x.created, DateTime.UtcNow))
                              .Execute();
                    return false;
                }
            }

            await this.store.Delete<source_cache_v1>()
                      .Where(rs => rs.Field(x => x.id, sourceControlId, (x, y) => x == y))
                      .Execute();

            var inserter = this.store.Insert<source_cache_v1>();

            foreach (var branch in branches)
            {
                inserter.Record(rs => rs.Field(x => x.id, sourceControlId)
                                        .Field(x => x.name, branch.Name)
                                        .Field(x => x.hash, branch.Hash)
                                        .Field(x => x.created, DateTime.UtcNow)
                                        .Field(x => x.log_data, "[]"));
            }

            await inserter.Execute();

            return true;
        }

        public async Task SetCommits(
            Guid sourceControlId,
            string branchName,
            string commitHash,
            SourceControlBranchCommitLogData[] commits)
        {
            await this.store.Update<source_cache_v1>()
                      .Where(rs => rs.Field(x => x.id, sourceControlId, (x, y) => x == y)
                                     .Field(x => x.name, branchName, (x, y) => x == y)
                                     .Field(x => x.hash, commitHash, (x, y) => x == y))
                      .Set(rs => rs.Field(x => x.log_data, this.serializer.Serialize(commits)))
                      .Execute();
        }

        private class BranchDataModel : IBranchLogData
        {
            public string Name { get; set; }

            public string Hash { get; set; }

            public ICommitLogData[] CommitLogs { get; set; }
        }

        private class CommitLogData : ICommitLogData
        {
            public string Id { get; set; }
            public DateTime When { get; set; }
            public string Author { get; set; }
            public string Message { get; set; }
            public string[] FilesAdded { get; set; }
            public string[] FilesRemoved { get; set; }
            public string[] FilesChanged { get; set; }
        }
    }
}