namespace Base2art.Web.Razor.Forms.Generation
{
    using System.Threading.Tasks;
    using Base2art.Razor.Api.Text;
    using Pages;

    public interface IHtmlBuilder
    {
        WebPage BuildWebPage();

        Task<WebPage> BuildWebPageAsync();

        Task<IEncodedString> Build();
    }
}