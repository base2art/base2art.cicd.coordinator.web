namespace Base2art.Web.Razor.Forms.Generation
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Base2art.Razor.Api;
    using FormInputs;
    using Pages;
    using PropertyBuilders;
    using ViewModels.FormInputs;

    public class SingleChooserInputBuilder<T, TProp> : HtmlControlBuilderBase<T, ChooserInputModel>
    {
        private readonly LabelBuilder<SingleChooserInputBuilder<T, TProp>, T> label;
        private readonly ValueBuilder<SingleChooserInputBuilder<T, TProp>, T, TProp> value;
        private readonly ReadOnlyBuilder<SingleChooserInputBuilder<T, TProp>> readOnly;
        private readonly RequiredBuilder<SingleChooserInputBuilder<T, TProp>> required;
        private readonly OptionsBuilder<SingleChooserInputBuilder<T, TProp>> options;

        private readonly NameAndIdBuilder<SingleChooserInputBuilder<T, TProp>, T> name;

        public SingleChooserInputBuilder(
            ChooserInputModel model,
            T value,
            Expression forsExpression,
            Func<T, TProp> lookup,
            Func<T, TProp[]> lookups,
            Func<TProp, string> stringifier)
            : base(model, value)
        {
            this.value = new ValueBuilder<SingleChooserInputBuilder<T, TProp>, T, TProp>(this, this.Model, lookup, lookups, stringifier);
            this.label = new LabelBuilder<SingleChooserInputBuilder<T, TProp>, T>(this, this.Model, forsExpression);
            this.name = new NameAndIdBuilder<SingleChooserInputBuilder<T, TProp>, T>(this, this.Model, forsExpression);
            this.readOnly = new ReadOnlyBuilder<SingleChooserInputBuilder<T, TProp>>(this, this.Model);
            this.required = new RequiredBuilder<SingleChooserInputBuilder<T, TProp>>(this, this.Model);
            this.options = new OptionsBuilder<SingleChooserInputBuilder<T, TProp>>(this, this.Model);
        }

        public SingleChooserInputBuilder<T, TProp> WithLabel(string labelValue) => this.label.WithLabel(labelValue);

        public SingleChooserInputBuilder<T, TProp> WithValue(TProp data) => this.value.WithValue(data);

        public SingleChooserInputBuilder<T, TProp> WithDefaultValue(TProp data) => this.value.WithDefaultValue(data);

        public SingleChooserInputBuilder<T, TProp> ReadOnly(bool isReadonly = true) => this.readOnly.Readonly(isReadonly);

        public SingleChooserInputBuilder<T, TProp> Required(bool isRequired = true) => this.required.Required(isRequired);

        public SingleChooserInputBuilder<T, TProp> WithOptions(KeyValuePair<string, string>[] values) => this.options.WithOptions(values);

        public SingleChooserInputBuilder<T, TProp> ShowCompact(bool? styleValue = true) => this.options.ShowCompact(styleValue);

        // public ChooserInputBuilder<T, TProp> Minimum(int minimum) => this.options.Minimum(minimum);
        //
        // public ChooserInputBuilder<T, TProp> Maximum(int maximum) => this.options.Maximum(maximum);

        protected override WebPage BuildControl() => WebPage.Create(
                                                                    this.options.ShowCompact(4)
                                                                        ? new DropDownInput()
                                                                        : (TemplateBase<ChooserInputModel>) new RadioButtonGroupInput(),
                                                                    this.Model);
    }
}