namespace Base2art.Web.Razor.Forms.Generation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using Comparison;
    using Controls;
    using FormInputs;
    using Pages;
    using PropertyBuilders;
    using ViewModels.FormInputs;

    public class ShowHideBuilder<T> : BasicControlBuilderBase<T, ShowHideModel>
        // , ILabeledBuilder<>
    {
        private readonly ForBuilder<ShowHideBuilder<T>, T> fors;

        public ShowHideBuilder(T value) : base(value)
        {
            this.fors = new ForBuilder<ShowHideBuilder<T>, T>(this, this.Model);
        }

        public ShowHideBuilder<T, string> For(Expression<Func<T, string>> lookup)
            => this.Wrap(this.fors.For(lookup), StandardComparers.StringComparer);

        public ShowHideBuilder<T, byte[]> For(Expression<Func<T, byte[]>> lookup)
            => this.Wrap(this.fors.For(lookup), StandardComparers.ByteArrayComparer);

        public ShowHideBuilder<T, Uri> For(Expression<Func<T, Uri>> lookup)
            => this.Wrap(this.fors.For(lookup), StandardComparers.UriComparer);

        public ShowHideBuilder<T, Guid?> For(Expression<Func<T, Guid?>> lookup)
            => this.Wrap(this.fors.For(lookup), StandardComparers.NullableGuidComparer);

        public ShowHideBuilder<T, TEnum> For<TEnum>(Expression<Func<T, TEnum>> lookup)
            where TEnum : Enum => this.Wrap(this.fors.For(lookup), StandardComparers.EnumComparer<TEnum>());


        private ShowHideBuilder<T, T1> Wrap<T1>((ShowHideBuilder<T>, Func<T, T1>, Func<T1, string>) @for, IComparer<T1> stringComparer)
            => new ShowHideBuilder<T, T1>(
                                          new ShowHideModel {Type = this.Model.Type},
                                          this.Value,
                                          this.fors.Expression,
                                          @for.Item2,
                                          null,
                                          @for.Item3,
                                          stringComparer);
    }

    public class ShowHideBuilder<T, TProp> : HtmlControlBuilderBase<T, ShowHideModel>
    {
        private readonly Func<TProp, string> stringifier;
        private readonly IComparer<TProp> comparer;
        private readonly NameAndIdBuilder<ShowHideBuilder<T, TProp>, T> name;
        private readonly ValueBuilder<ShowHideBuilder<T, TProp>, T, TProp> value;

        public ShowHideBuilder(
            ShowHideModel model,
             T value,
            Expression expression,
            Func<T, TProp> lookup,
            Func<T, TProp[]> lookups,
            Func<TProp, string> stringifier,
            IComparer<TProp> comparer)
            : base(model, value)
        {
            this.stringifier = stringifier;
            this.comparer = comparer;
            this.value = new ValueBuilder<ShowHideBuilder<T, TProp>, T, TProp>(this, this.Model, lookup, lookups, this.stringifier);
            this.name = new NameAndIdBuilder<ShowHideBuilder<T, TProp>, T>(this, this.Model, expression);
        }

        public ShowHideWhenBuilder When(TProp data)
            => new ShowHideWhenBuilder(this, data, this.comparer);

        protected override WebPage BuildControl()
        {
            return WebPage.Create(new ShowHide(), this.Model);
        }

        private void RegisterShow(string data, Expression func)
        {
            var showHides = new List<ShowHideItemModel>(this.Model.ShowHides ?? new ShowHideItemModel[0]);

            showHides.Add(new ShowHideItemModel
                          {
                              Value = data,
                              Control = func.GetAccessString(),
                              Show = true
                          });

            this.Model.ShowHides = showHides.ToArray();
        }

        private void RegisterHide<TEnum>(string data, Expression<Func<T, TEnum>> func)
        {
            var showHides = new List<ShowHideItemModel>(this.Model.ShowHides ?? new ShowHideItemModel[0]);

            showHides.Add(new ShowHideItemModel
                          {
                              Value = data,
                              Control = func.GetAccessString(),
                              Show = false
                          });

            this.Model.ShowHides = showHides.ToArray();
        }

        public class ShowHideWhenBuilder
        {
            private readonly ShowHideBuilder<T, TProp> returnValue;
            private readonly TProp data;
            private readonly IComparer<TProp> comparer;

            public ShowHideWhenBuilder(ShowHideBuilder<T, TProp> returnValue, TProp data, IComparer<TProp> comparer)
            {
                this.returnValue = returnValue;
                this.data = data;
                this.comparer = comparer;
            }

            public ShowHideBuilder<T, TProp> Hide(Expression<Func<T, string>> lookup)
                => this.WrapHide(lookup);

            public ShowHideBuilder<T, TProp> Hide(Expression<Func<T, byte[]>> lookup)
                => this.WrapHide(lookup);

            public ShowHideBuilder<T, TProp> Hide(Expression<Func<T, Uri>> lookup)
                => this.WrapHide(lookup);

            public ShowHideBuilder<T, TProp> Hide(Expression<Func<T, Guid?>> lookup)
                => this.WrapHide(lookup);
            
            public ShowHideBuilder<T, TProp> Hide(Expression<Func<T, IDictionary<string, string>>> lookup)
                => this.WrapHide(lookup);

            public ShowHideBuilder<T, TProp> Hide<TEnum>(Expression<Func<T, TEnum>> lookup)
                where TEnum : Enum => this.WrapHide(lookup);
            
            public ShowHideBuilder<T, TProp> Hide(Expression<Func<T, string[]>> lookup)
                => this.WrapHide(lookup);

            public ShowHideBuilder<T, TProp> Show(Expression<Func<T, string>> lookup)
                => this.WrapShow(lookup);

            public ShowHideBuilder<T, TProp> Show(Expression<Func<T, byte[]>> lookup)
                => this.WrapShow(lookup);

            public ShowHideBuilder<T, TProp> Show(Expression<Func<T, Uri>> lookup)
                => this.WrapShow(lookup);

            public ShowHideBuilder<T, TProp> Show(Expression<Func<T, Guid?>> lookup)
                => this.WrapShow(lookup);

            public ShowHideBuilder<T, TProp> Show<TEnum>(Expression<Func<T, TEnum>> lookup)
                where TEnum : Enum => this.WrapShow(lookup);
            
            public ShowHideBuilder<T, TProp> Show(Expression<Func<T, string[]>> lookup)
                => this.WrapShow(lookup);
            
            public ShowHideBuilder<T, TProp> Show(Expression<Func<T, IDictionary<string, string>>> lookup)
                => this.WrapShow(lookup);

            private ShowHideBuilder<T, TProp> WrapHide<T1>(Expression<Func<T, T1>> lookup)
            {
                KeyValuePair<string, TProp>[] keyValuePairs = typeof(TProp).IsEnum ? Enums.GetOptionValuesUnsafe<TProp>() : null;
                var realData = this.data?.ToString();
                if (keyValuePairs != null)
                {
                    var pair = keyValuePairs.FirstOrDefault(x => this.comparer.Compare(x.Value, this.data) == 0);

                    if (this.comparer.Compare(pair.Value, this.data) == 0)
                    {
                        realData = pair.Key;
                    }
                }

                this.returnValue.RegisterHide(realData, lookup);
                return this.returnValue;
            }

            private ShowHideBuilder<T, TProp> WrapShow<T1>(Expression<Func<T, T1>> lookup)
            {
                KeyValuePair<string, TProp>[] keyValuePairs = typeof(TProp).IsEnum ? Enums.GetOptionValuesUnsafe<TProp>() : null;
                var realData = this.data?.ToString();
                if (keyValuePairs != null)
                {
                    var pair = keyValuePairs.FirstOrDefault(x => this.comparer.Compare(x.Value, this.data) == 0);

                    if (this.comparer.Compare(pair.Value, this.data) == 0)
                    {
                        realData = pair.Key;
                    }
                }

                this.returnValue.RegisterShow(realData, lookup);
                return this.returnValue;
            }
        }
    }
}