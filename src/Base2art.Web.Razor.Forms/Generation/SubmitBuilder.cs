namespace Base2art.Web.Razor.Forms.Generation
{
    using System;
    using System.Linq.Expressions;
    using FormInputs;
    using Pages;
    using PropertyBuilders;
    using ViewModels.FormInputs;

    public class SubmitBuilder<T> : BasicControlBuilderBase<T, SubmitModel>
    {
        private readonly ForBuilder<SubmitBuilder<T>, T> fors;

        public SubmitBuilder(T value) : base(value)
        {
            this.fors = new ForBuilder<SubmitBuilder<T>, T>(this, this.Model);
        }

        public SubmitBuilder<T, string> For(Expression<Func<T, string>> lookup)
            => this.Wrap(this.fors.For(lookup));

        public SubmitBuilder<T, byte[]> For(Expression<Func<T, byte[]>> lookup)
            => this.Wrap(this.fors.For(lookup));

        public SubmitBuilder<T, Uri> For(Expression<Func<T, Uri>> lookup)
            => this.Wrap(this.fors.For(lookup));

        public SubmitBuilder<T, Guid?> For(Expression<Func<T, Guid?>> lookup)
            => this.Wrap(this.fors.For(lookup));

        public SubmitBuilder<T, TEnum> For<TEnum>(Expression<Func<T, TEnum>> lookup)
            where TEnum : Enum => this.Wrap(this.fors.For(lookup));


        private SubmitBuilder<T, TOther> Wrap<TOther>((SubmitBuilder<T>, Func<T, TOther>, Func<TOther, string>) @for)
        {
            var builder = new SubmitBuilder<T, TOther>(
                                                       new SubmitModel {Type = this.Model.Type},
                                                       this.Value,
                                                       this.fors.Expression,
                                                       @for.Item2,
                                                       null,
                                                       @for.Item3);

            return builder;
        }
    }

    public class SubmitBuilder<T, TProp> : HtmlControlBuilderBase<T, SubmitModel>
    {
        private readonly LabelBuilder<SubmitBuilder<T, TProp>, T> label;
        private readonly NameAndIdBuilder<SubmitBuilder<T, TProp>, T> name;
        private readonly ValueBuilder<SubmitBuilder<T, TProp>, T, TProp> value;

        public SubmitBuilder(
            SubmitModel model,
            T value,
            Expression expression,
            Func<T, TProp> lookup,
            Func<T, TProp[]> lookups,
            Func<TProp, string> stringifier) : base(model, value)
        {
            this.value = new ValueBuilder<SubmitBuilder<T, TProp>, T, TProp>(this, this.Model, lookup, lookups, stringifier);
            this.label = new LabelBuilder<SubmitBuilder<T, TProp>, T>(this, this.Model, expression);
            this.name = new NameAndIdBuilder<SubmitBuilder<T, TProp>, T>(this, this.Model, expression);

            this.label.WithLabel("Submit");
        }

        public SubmitBuilder<T, TProp> WithValue(TProp data) => this.value.WithValue(data);

        public SubmitBuilder<T, TProp> WithLabel(string labelValue) => this.label.WithLabel(labelValue);

        protected override WebPage BuildControl()
        {
            return WebPage.Create(new Submit(), this.Model);
        }
    }
}

// public SubmitBuilder<T> For(Expression<Func<T, Guid>> lookup)
// {
//     this.func = lookup;
//     this.valueLookup = x => lookup.Compile().Invoke(x).GetValueIfNotEmpty();
//     return this;
// }
//
// public SubmitBuilder<T> For(Expression<Func<T, int>> lookup)
// {
//     this.func = lookup;
//     this.valueLookup = x => lookup.Compile().Invoke(x).GetValueIfNotEmpty();
//     return this;
// }
//
// public SubmitBuilder<T> For(Expression<Func<T, long>> lookup)
// {
//     this.func = lookup;
//     this.valueLookup = x => lookup.Compile().Invoke(x).GetValueIfNotEmpty();
//     return this;
// }
//
// public SubmitBuilder<T> For(Expression<Func<T, string>> lookup)
// {
//     this.func = lookup;
//     this.valueLookup = x => lookup.Compile().Invoke(x);
//     return this;
// }