namespace Base2art.Web.Razor.Forms.Generation
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Controls;
    using PropertyBuilders;
    using ViewModels.FormInputs;

    public class ChooserInputBuilder<T> : BasicControlBuilderBase<T, ChooserInputModel>
    {
        private readonly ForBuilder<ChooserInputBuilder<T>, T> fors;

        public ChooserInputBuilder(T value) : base(value)
            => this.fors = new ForBuilder<ChooserInputBuilder<T>, T>(this, this.Model);

        public SingleChooserInputBuilder<T, string> For(Expression<Func<T, string>> lookup)
            => this.Wrap(this.fors.For(lookup));

        public SingleChooserInputBuilder<T, byte[]> For(Expression<Func<T, byte[]>> lookup)
            => this.Wrap(this.fors.For(lookup));

        public SingleChooserInputBuilder<T, Uri> For(Expression<Func<T, Uri>> lookup)
            => this.Wrap(this.fors.For(lookup));

        public SingleChooserInputBuilder<T, Guid?> For(Expression<Func<T, Guid?>> lookup)
            => this.Wrap(this.fors.For(lookup));

        public MultiChooserInputBuilder<T, bool?> For(Expression<Func<T, bool?>> lookup)
            => this.WrapMany<bool?>(this.fors.For(lookup), new[]
                                                           {
                                                               new KeyValuePair<string, string>("True", lookup.ExpressionName())
                                                           }).Minimum(0).Maximum(1);

        public SingleChooserInputBuilder<T, TEnum> For<TEnum>(Expression<Func<T, TEnum>> lookup)
            where TEnum : Enum => this.Wrap(this.fors.For(lookup), Enums.GetOptions<TEnum>());

        public MultiChooserInputBuilder<T, TProp> For<TProp>(Expression<Func<T, TProp[]>> lookup, Func<TProp, string> map)
            where TProp : class
            => this.WrapMany(this.fors.For(lookup, map));

        public MultiChooserInputBuilder<T, Guid> For(Expression<Func<T, Guid[]>> lookup)
            => this.WrapMany(this.fors.For(lookup, prop => prop.ToString("N")));

        private SingleChooserInputBuilder<T, TOther> Wrap<TOther>(
            (ChooserInputBuilder<T>, Func<T, TOther>, Func<TOther, string>) @for,
            KeyValuePair<string, string>[] options = null)
        {
            var builder = new SingleChooserInputBuilder<T, TOther>(
                                                                   new ChooserInputModel {Type = this.Model.Type},
                                                                   this.Value,
                                                                   this.fors.Expression,
                                                                   @for.Item2,
                                                                   null,
                                                                   @for.Item3);
            builder.WithOptions(options);
            return builder;
        }

        private MultiChooserInputBuilder<T, T1> WrapMany<T1>(
            (ChooserInputBuilder<T>, Func<T, T1>, Func<T1, string>) @for,
            KeyValuePair<string, string>[] options)
        {
            var builder = new MultiChooserInputBuilder<T, T1>(new MultiChooserInputModel {Type = this.Model.Type},
                                                              this.Value,
                                                              this.fors.Expression,
                                                              (x) => new[] {@for.Item2(x)},
                                                              @for.Item3);
            builder.WithOptions(options);
            return builder;
        }

        private MultiChooserInputBuilder<T, TOther> WrapMany<TOther>(
            (ChooserInputBuilder<T>, Func<T, TOther[]>, Func<TOther, string>) @for,
            KeyValuePair<string, string>[] options = null)
        {
            var builder = new MultiChooserInputBuilder<T, TOther>(new MultiChooserInputModel {Type = this.Model.Type},
                                                                  this.Value,
                                                                  this.fors.Expression,
                                                                  @for.Item2,
                                                                  @for.Item3);
            builder.WithOptions(options);
            return builder;
        }
    }
}