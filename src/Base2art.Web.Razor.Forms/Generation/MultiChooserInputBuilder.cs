namespace Base2art.Web.Razor.Forms.Generation
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Base2art.Razor.Api;
    using FormInputs;
    using Pages;
    using PropertyBuilders;
    using ViewModels.FormInputs;

    public class MultiChooserInputBuilder<T, TProp> : HtmlControlBuilderBase<T, MultiChooserInputModel>
    {
        private readonly LabelBuilder<MultiChooserInputBuilder<T, TProp>, T> label;
        private readonly ValuesBuilder<MultiChooserInputBuilder<T, TProp>, T, TProp> value;
        private readonly ReadOnlyBuilder<MultiChooserInputBuilder<T, TProp>> readOnly;
        private readonly RequiredBuilder<MultiChooserInputBuilder<T, TProp>> required;
        private readonly OptionsBuilder<MultiChooserInputBuilder<T, TProp>> options;

        private readonly NameAndIdBuilder<MultiChooserInputBuilder<T, TProp>, T> name;
        // private readonly ActionInvokerBuilder<MultiChooserInputBuilder<T, TProp>> actionInvoker;
        // private bool? style;

        public MultiChooserInputBuilder(
            MultiChooserInputModel model,
            T value,
            Expression forsExpression,
            Func<T, TProp[]> forsValueLookup,
            Func<TProp, string> stringifier)
            : base(model, value)
        {
            // this.fors = new ForBuilder<ChooserInputBuilder<T>, T>(this, this.Model);
            this.value = new ValuesBuilder<MultiChooserInputBuilder<T, TProp>, T, TProp>(this, this.Model, forsValueLookup, stringifier);
            this.label = new LabelBuilder<MultiChooserInputBuilder<T, TProp>, T>(this, this.Model, forsExpression);
            this.name = new NameAndIdBuilder<MultiChooserInputBuilder<T, TProp>, T>(this, this.Model, forsExpression);
            this.readOnly = new ReadOnlyBuilder<MultiChooserInputBuilder<T, TProp>>(this, this.Model);
            this.required = new RequiredBuilder<MultiChooserInputBuilder<T, TProp>>(this, this.Model);
            this.options = new OptionsBuilder<MultiChooserInputBuilder<T, TProp>>(this, this.Model);
            // this.actionInvoker = new ActionInvokerBuilder<MultiChooserInputBuilder<T, TProp>>(this, this.Model);
        }

        public MultiChooserInputBuilder<T, TProp> WithLabel(string labelValue) => this.label.WithLabel(labelValue);

        public MultiChooserInputBuilder<T, TProp> WithValues(TProp[] data) => this.value.WithValues(data);

        public MultiChooserInputBuilder<T, TProp> ReadOnly(bool isReadonly = true) => this.readOnly.Readonly(isReadonly);

        // public MultiChooserInputBuilder<T, TProp> Required(bool isRequired = true) => this.required.Required(isRequired);

        public MultiChooserInputBuilder<T, TProp> WithOptions(KeyValuePair<string, string>[] values) => this.options.WithOptions(values);
        
        public MultiChooserInputBuilder<T, TProp> ShowCompact(bool? styleValue = true) => this.options.ShowCompact(styleValue);

        public MultiChooserInputBuilder<T, TProp> Minimum(int minimum) => this.options.Minimum(minimum);

        public MultiChooserInputBuilder<T, TProp> Maximum(int maximum) => this.options.Maximum(maximum);

        protected override WebPage BuildControl() => WebPage.Create(
                                                                    this.options.ShowCompact(4)
                                                                        ? new MultiChooserInput()
                                                                        : (TemplateBase<MultiChooserInputModel>) new CheckboxListChooserInput(),
                                                                    this.Model);
    }
}