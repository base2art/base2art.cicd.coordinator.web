namespace Base2art.Web.Razor.Forms.Generation
{
    using System;
    using System.Linq.Expressions;
    using FormInputs;
    using Pages;
    using ViewModels.FormInputs;

    public class ValidationSummaryBuilder<T> : ControlBuilderBase<T, ValidationSummaryModel>
    {
//        private (string Code, string Message, string Path)[] errors;

        public ValidationSummaryBuilder(T value) : base(value)
        {
        }

        public ValidationSummaryBuilder<T> WithErrors((string Code, string Message, string Path)[] errorsToAdd)
        {
            this.Model.IsValid = (errorsToAdd?.Length).GetValueOrDefault() == 0;

            this.Model.Errors = errorsToAdd ?? new (string Code, string Message, string Path)[0];

            return this;
        }

        protected override WebPage BuildControl()
        {
            return WebPage.Create(new ValidationSummary(), this.Model);
        }
    }
}