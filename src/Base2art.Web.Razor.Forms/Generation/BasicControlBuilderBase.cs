namespace Base2art.Web.Razor.Forms.Generation
{
    using ViewModels;

    public abstract class BasicControlBuilderBase<TData, TModel> //: IControlBuilder<TData> //: IHtmlBuilder, 
        where TModel : new()
    {
        protected BasicControlBuilderBase(TData value) => this.Value = value;

        protected ITypeModel Model { get; } = new TypeModel();

        protected TData Value { get; }

        private class TypeModel : ITypeModel
        {
            public string Type { get; set; }
        }
    }
}