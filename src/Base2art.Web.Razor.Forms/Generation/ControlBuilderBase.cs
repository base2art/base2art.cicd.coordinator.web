namespace Base2art.Web.Razor.Forms.Generation
{
    using System;
    using System.Threading.Tasks;
    using Base2art.Razor.Api.Text;
    using Controls;
    using Pages;

    public abstract class ControlBuilderBase<TData, TModel> : IHtmlBuilder, IControlBuilder<TData>
        where TModel : new()
    {
        private readonly RawStringFactory raw = new RawStringFactory();

        protected ControlBuilderBase(TData value) => this.Value = value;

        private event EventHandler<DataBoundEventArgs<TData>> PreBuild;

        event EventHandler<DataBoundEventArgs<TData>> IControlBuilder<TData>.PreBuild
        {
            add => this.PreBuild += value;
            remove => this.PreBuild -= value;
        }

        protected TModel Model { get; } = new TModel();

        protected TData Value { get; }

        public WebPage BuildWebPage() => this.BuildControlWrapper();

        public Task<WebPage> BuildWebPageAsync() => Task.FromResult(this.BuildControlWrapper());

        public async Task<IEncodedString> Build() => this.raw.CreateEncodedString(await this.BuildControlWrapper().Render());

        private WebPage BuildControlWrapper()
        {
            this.PreBuild?.Invoke(this, new DataBoundEventArgs<TData>(this.Value));
            return this.BuildControl();
        }

        protected abstract WebPage BuildControl();
    }
}