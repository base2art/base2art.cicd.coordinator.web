namespace Base2art.Web.Razor.Forms.Generation.PropertyBuilders
{
    using System.Collections.Generic;
    using ViewModels;

    public class OptionsBuilder<TRet>
    {
        private readonly TRet returnValue;

        public OptionsBuilder(TRet returnValue, IOptionedModel model)
        {
            this.returnValue = returnValue;
            this.Model = model;
        }

        protected IOptionedModel Model { get; }
        protected int OptionsCount { get; private set; }
        protected bool? StylePreference { get; private set; }

        public TRet WithOptions(KeyValuePair<string, string>[] options)
        {
            this.Model.Options = options ?? new KeyValuePair<string, string>[0];
            this.OptionsCount = this.Model.Options.Length;
            return this.returnValue;
        }

        public TRet Minimum(int minimum)
        {
            this.Model.Minimum = minimum;
            return this.returnValue;
        }

        public TRet Maximum(int maximum)
        {
            this.Model.Maximum = maximum;
            return this.returnValue;
        }

        public TRet ShowCompact(bool? styleValue)
        {
            this.StylePreference = styleValue;
            return this.returnValue;
        }

        public bool ShowCompact(int i)
        {
            if (this.StylePreference.HasValue)
            {
                return this.StylePreference.Value;
            }

            return this.OptionsCount > i;
        }
    }
}