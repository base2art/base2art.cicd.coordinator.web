namespace Base2art.Web.Razor.Forms.Generation.PropertyBuilders
{
    using ViewModels;

    public class RequiredBuilder<TRet>
    {
        private readonly TRet returnValue;

        public RequiredBuilder(TRet returnValue, IRequiredModel model)
        {
            this.returnValue = returnValue;
            this.Model = model;
        }

        protected IRequiredModel Model { get; }

        public TRet Required(bool isRequired)
        {
            this.Model.Required = isRequired;
            return this.returnValue;
        }
    }
}