namespace Base2art.Web.Razor.Forms.Generation.PropertyBuilders
{
    using System;
    using System.Linq;
    using Controls;
    using ViewModels;

    public class ValuesBuilder<TRet, TModel, TProp> //: ValueBuilder<TRet, TModel>
        where TRet : IControlBuilder<TModel>
    {
        private readonly TRet returnValue;

        private Func<TModel, TProp[]> valueLookup;
        private readonly Func<TProp, string> stringifier;
        private Func<TModel, TProp[]> defaultValueLookup;

        public ValuesBuilder(TRet returnValue, IValuesModel valueModel, Func<TModel, TProp[]> valueLookup, Func<TProp, string> stringifier)
        {
            this.returnValue = returnValue;
            returnValue.PreBuild += (sender, args) 
                => this.Model.Values = (this.valueLookup?.Invoke(args.Model) ?? this.defaultValueLookup?.Invoke(args.Model))
                       ?.Select(stringifier)
                       ?.ToArray();
            this.Model = valueModel;
            this.valueLookup = valueLookup;
            this.stringifier = stringifier;
        }

        protected IValuesModel Model { get; }

        public TRet WithValues(TProp[] data)
        {
            this.valueLookup = model => data;
            return this.returnValue;
        }

        public TRet WithDefaultValues(TProp[] data)
        {
            this.defaultValueLookup = model => data;
            return this.returnValue;
        }
    }
}