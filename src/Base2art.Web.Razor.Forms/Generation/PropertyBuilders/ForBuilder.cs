namespace Base2art.Web.Razor.Forms.Generation.PropertyBuilders
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using Controls;
    using Newtonsoft.Json;
    using ViewModels;

    public class ForBuilder<TRet, TModel> //: ValueBuilder<TRet, TModel>
        // where TRet : IControlBuilder<TModel>
    {
        private readonly TRet returnValue;

        public ForBuilder(TRet returnValue, ITypeModel valueModel)
        {
            this.returnValue = returnValue;
            this.Model = valueModel;
        }

        public Expression Expression { get; set; }

        // public Func<TModel, string> ValueLookup { get; set; }
        // public Func<TModel, string[]> ValuesLookup { get; set; }

        protected ITypeModel Model { get; }

        public (TRet, Func<TModel, string>, Func<string, string>) For(Expression<Func<TModel, string>> lookup)
        {
            this.Expression = lookup;
            return (this.returnValue, x => lookup.Compile().InvokeCatchNullRef(x), x => x?.ToString());
        }

        public (TRet, Func<TModel, byte[]>, Func<byte[], string>) For(Expression<Func<TModel, byte[]>> lookup)
        {
            this.Expression = lookup;
            return (this.returnValue, x => lookup.Compile().InvokeCatchNullRef(x), x => Convert.ToBase64String(x ?? new byte[0]));
        }

        public (TRet, Func<TModel, Uri>, Func<Uri, string>) For(Expression<Func<TModel, Uri>> lookup)
        {
            this.Expression = lookup;
            this.Model.Type = "url";
            return (this.returnValue, x => lookup.Compile().InvokeCatchNullRef(x), x => x?.ToString());
        }

        public (TRet, Func<TModel, Guid?>, Func<Guid?, string>) For(Expression<Func<TModel, Guid?>> lookup)
        {
            this.Expression = lookup;
            // this.Model.Type = "text";
            return (this.returnValue, x => lookup.Compile().InvokeCatchNullRef(x), x => x?.ToString());
        }

        public (TRet, Func<TModel, bool?>, Func<bool?, string>) For(Expression<Func<TModel, bool?>> lookup)
        {
            this.Expression = lookup;
            // this.Model.Type = "text";
            return (this.returnValue, x => lookup.Compile().InvokeCatchNullRef(x), x => x?.ToString());
        }

        public (TRet, Func<TModel, TEnum>, Func<TEnum, string>) For<TEnum>(Expression<Func<TModel, TEnum>> lookup)
            where TEnum : Enum
        {
            this.Expression = lookup;
            var underlyingType = Enum.GetUnderlyingType(typeof(TEnum));
            return (this.returnValue, x => lookup.Compile().InvokeCatchNullRef(x), x => this.MapValue(underlyingType, x));
            // this.ValueLookup = x => this.MapValue(underlyingType, lookup.Compile().InvokeCatchNullRef(x));

            // this.Model.Type = "text";
        }

        /*MULTIPLES*/
        public (TRet, Func<TModel, string[]>, Func<string[], string>) For(Expression<Func<TModel, string[]>> lookup)
        {
            this.Expression = lookup;
            return (this.returnValue, x => lookup.Compile().InvokeCatchNullRef(x), x => string.Join(Environment.NewLine, x ?? new string[0]));
        }

        public (TRet, Func<TModel, TProp[]>, Func<TProp, string>) For<TProp>(Expression<Func<TModel, TProp[]>> lookup, Func<TProp, string> mapper)
        {
            this.Expression = lookup;
            // this.Model.Type = "json";

            return (this.returnValue, x => lookup.Compile().InvokeCatchNullRef(x), mapper);
            // this.ValuesLookup = x => (lookup.Compile().InvokeCatchNullRef(x) ?? new TProp[0]).Select(mapper).ToArray();
            // return this.returnValue;
        }

        public (TRet, Func<TModel, DateTimeOffset?>, Func<DateTimeOffset?, string>) ForLocalizedDateTime(Expression<Func<TModel, DateTimeOffset?>> lookup)
        {
            this.Expression = lookup;
            this.Model.Type = "datetime-local";
            return (this.returnValue, x => lookup.Compile().InvokeCatchNullRef(x), x => x?.ToString("yyyy-MM-ddTHH:mm"));
            // this.ValueLookup = x => lookup.Compile().InvokeCatchNullRef(x)?.ToString("yyyy-MM-ddTHH:mm");
            // return this.returnValue;
        }

        public (TRet, Func<TModel, DateTime?>, Func<DateTime?, string>) ForDate(Expression<Func<TModel, DateTime?>> lookup)
        {
            this.Expression = lookup;
            this.Model.Type = "date";
            return (this.returnValue, x => lookup.Compile().InvokeCatchNullRef(x), x => x?.ToString("yyyy-MM-dd"));
            // this.ValueLookup = x => lookup.Compile().InvokeCatchNullRef(x)?.ToString("yyyy-MM-dd");
            // return this.returnValue;
        }

        public (TRet, Func<TModel, TimeSpan?>, Func<TimeSpan?, string>) ForTime(Expression<Func<TModel, TimeSpan?>> lookup)
        {
            this.Expression = lookup;
            this.Model.Type = "time";
            return (this.returnValue, x => lookup.Compile().InvokeCatchNullRef(x), x => x?.ToString("hh\\:mm\\:ss"));
            // this.ValueLookup = x => lookup.Compile().InvokeCatchNullRef(x)?.ToString("hh\\:mm\\:ss");
            // return this.returnValue;
        }

/* EXTENSIONS */
        public (TRet, Func<TModel, IDictionary<string, string>>, Func<IDictionary<string, string>, string>) ForJson(
            Expression<Func<TModel, IDictionary<string, string>>> lookup)
        {
            this.Expression = lookup;
            this.Model.Type = "json";
            return (this.returnValue,
                       x => lookup.Compile().InvokeCatchNullRef(x),
                       x => JsonConvert.SerializeObject(x ?? new Dictionary<string, string>(), Formatting.Indented));
            // this.ValueLookup = x =>
            //     JsonConvert.SerializeObject(lookup.Compile().InvokeCatchNullRef(x) ?? new Dictionary<string, string>(), Formatting.Indented);
            // return this.returnValue;
        }

        private string MapValue<TEnum>(Type underlyingType, TEnum @enum)
            where TEnum : Enum
        {
            var val = Convert.ChangeType(@enum, underlyingType);
            return val.ToString();
        }
    }
}
