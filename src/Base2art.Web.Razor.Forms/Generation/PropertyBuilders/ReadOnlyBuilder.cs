namespace Base2art.Web.Razor.Forms.Generation.PropertyBuilders
{
    using ViewModels;

    public class ReadOnlyBuilder<TRet>
    {
        private readonly TRet returnValue;

        public ReadOnlyBuilder(TRet returnValue, IReadOnlyModel model)
        {
            this.returnValue = returnValue;
            this.Model = model;
        }

        protected IReadOnlyModel Model { get; }

        public TRet Readonly(bool isReadonly)
        {
            this.Model.IsReadOnly = isReadonly;
            return this.returnValue;
        }
    }
}