namespace Base2art.Web.Razor.Forms.Generation.PropertyBuilders
{
    using System;

    public class ActionInvokerBuilder<TRet>
    {
        private readonly TRet returnValue;

        public ActionInvokerBuilder(TRet returnValue)
        {
            this.returnValue = returnValue;
        }

        public TRet WithAction(Action actionToExecute)
        {
            actionToExecute();
            return this.returnValue;
        }
    }
}