namespace Base2art.Web.Razor.Forms.Generation.PropertyBuilders
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using Controls;
    using ViewModels;

    public class LabelBuilder<TRet, TModel>
        where TRet : IControlBuilder<TModel>
    {
        private readonly TRet returnValue;
        private readonly Expression func;
        
        private Func<string> labelLookup;

        public LabelBuilder(TRet returnValue, ILabelModel model, Expression func)
        {
            this.returnValue = returnValue;
            this.func = func;
            this.Model = model;
            this.labelLookup = this.GetDefaultLabel;
            this.returnValue.PreBuild += (a, b) => this.Model.Label = this.labelLookup.Invoke();
        }

        protected ILabelModel Model { get; }

        private string GetDefaultLabel()
        {
            return Expressions.ExpressionName(this.func);
        }

        public TRet WithLabel(string labelValue)
        {
            this.labelLookup = () => labelValue;
            return this.returnValue;
        }
        
        
    }
}