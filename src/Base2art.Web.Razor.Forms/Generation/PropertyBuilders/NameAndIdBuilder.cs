namespace Base2art.Web.Razor.Forms.Generation.PropertyBuilders
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using Controls;
    using ViewModels;

    public class NameAndIdBuilder<TRet, TModel>
        where TRet : IControlBuilder<TModel>
    {
        private readonly TRet returnValue;
        private readonly Expression func;

        // private readonly ValueBuilder<TRet, TModel> valueBuilder;
        // private readonly ILabelModel model;

        // private Func<string> labelLookup;
        // private object labelLookup;

        public NameAndIdBuilder(TRet returnValue, INamedAndIdedModel model, Expression func)
        {
            
            this.returnValue = returnValue;
            this.func = func;
            // this.valueBuilder = valueBuilder;
            this.Model = model;
            this.returnValue.PreBuild += (a, b) =>
            {
                this.Model.Id = $"{Guid.NewGuid():N}";
                this.Model.InputName = this.GetAccessString();
            };
        }

        protected INamedAndIdedModel Model { get; }
        
        private string GetAccessString()
            => this.func.GetAccessString();
    }
}