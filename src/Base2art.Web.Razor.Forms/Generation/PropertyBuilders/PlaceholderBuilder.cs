namespace Base2art.Web.Razor.Forms.Generation.PropertyBuilders
{
    using ViewModels;

    public class PlaceholderBuilder<TRet>
    {
        private readonly TRet returnValue;

        public PlaceholderBuilder(TRet returnValue, IPlaceholderModel model)
        {
            this.returnValue = returnValue;
            this.Model = model;
        }

        protected IPlaceholderModel Model { get; }

        public TRet WithPlaceholder(string placeholderValue)
        {
            this.Model.Placeholder = placeholderValue;
            return this.returnValue;
        }
    }
}