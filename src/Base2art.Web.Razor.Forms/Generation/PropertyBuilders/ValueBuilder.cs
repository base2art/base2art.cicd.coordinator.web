namespace Base2art.Web.Razor.Forms.Generation.PropertyBuilders
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using Controls;
    using ViewModels;

    public class ValueBuilder<TRet, TModel, TProp> //: ValueBuilder<TRet, TModel>
        where TRet : IControlBuilder<TModel>
    {
        private readonly TRet returnValue;

        private Func<TModel, TProp> valueLookup;
        private readonly Func<TProp, string> stringifier;
        private Func<TModel, TProp> defaultValueLookup;

        public ValueBuilder(
            TRet returnValue,
            IValueModel valueModel,
            Func<TModel, TProp> valueLookup,
            Func<TModel, TProp[]> valuesLookup,
            Func<TProp, string> stringifier)
        {
            this.returnValue = returnValue;
            returnValue.PreBuild += (sender, args) => this.Model.Value = this.defaultValueLookup == null
                                                                             ? GetValue(valueLookup, valuesLookup, args.Model)
                                                                             : StringOf(this.defaultValueLookup.Invoke(args.Model));
            this.Model = valueModel;
            this.valueLookup = valueLookup;
            this.stringifier = stringifier;
        }

        private string StringOf(TProp item)
        {
            return item == null ? null : this.stringifier.Invoke(item);
        }

        private string GetValue(Func<TModel, TProp> func1, Func<TModel, TProp[]> func2, TModel model)
        {
            if (func1 != null)
            {
                var item = func1.Invoke(model);
                return StringOf(item);
            }

            var strings = func2?.Invoke(model);
            return strings != null ? string.Join(Environment.NewLine, strings.Select(this.stringifier)) : null;
        }

        protected IValueModel Model { get; }

        public TRet WithValue(TProp data)
        {
            this.valueLookup = model => data;
            return this.returnValue;
        }

        public TRet WithDefaultValue(TProp data)
        {
            this.defaultValueLookup = model => data;
            return this.returnValue;
        }
    }
}