namespace Base2art.Web.Razor.Forms.Generation.PropertyBuilders
{
    using System.Text.RegularExpressions;
    using ViewModels;

    public class PatternBuilder<TRet>
    {
        private readonly TRet returnValue;

        public PatternBuilder(TRet returnValue, IPatternedModel model)
        {
            this.returnValue = returnValue;
            this.Model = model;
        }

        protected IPatternedModel Model { get; }

        public TRet WithPattern(Regex pattern)
        {
            this.Model.Pattern = pattern;
            return this.returnValue;
        }
    }
}