namespace Base2art.Web.Razor.Forms.Generation.PropertyBuilders
{
    using ViewModels;

    public class TextAreaRowsBuilder<TRet>
    {
        private readonly TRet returnValue;

        public TextAreaRowsBuilder(TRet returnValue, IRowsModel model)
        {
            this.returnValue = returnValue;
            this.Model = model;
        }

        protected IRowsModel Model { get; }

        public TRet MultiLine(int rowCount)
        {
            this.Model.RowCount = rowCount;
            return this.returnValue;
        }
    }
}