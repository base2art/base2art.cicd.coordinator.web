namespace Base2art.Web.Razor.Forms.Generation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text;

    public static class Generators
    {
        public static string GetAccessString(this Expression func)
        {
            var parts1 = new List<MemberInfo>();
            var parts2 = new List<string>();

            LambdaExpression lambda = func as LambdaExpression;
            if (lambda == null)
            {
                throw new ArgumentNullException(nameof(lambda));
            }

            var lambdaBody = lambda.Body;

            GetAccessStringInternal(parts1, parts2, lambdaBody);

            if (parts2.Count <= 1)
            {
                return string.Join(".", parts2.Select(x => x));
                // return string.Empty;
            }

            StringBuilder sb = new StringBuilder();

            sb.Append(parts2[0]);

            for (int i = 1; i < parts2.Count; i++)
            {
                var part = parts2[i];
                if (part.StartsWith("["))
                {
                    sb.Append(part);
                }
                else
                {
                    sb.Append(".");
                    sb.Append(part);
                }
            }

            return sb.ToString();
        }

        public static MemberInfo GetMemberInfo(this Expression func)
        {
            var parts1 = new List<MemberInfo>();
            var parts2 = new List<string>();

            LambdaExpression lambda = func as LambdaExpression;
            if (lambda == null)
            {
                throw new ArgumentNullException(nameof(lambda));
            }

            var lambdaBody = lambda.Body;

            GetAccessStringInternal(parts1, parts2, lambdaBody);

            return parts1.Count == 1 ? parts1[0] : null;
        }

        public static string HumanizeFromCamelCase<T>(this T name) where T : struct, Enum =>
            name.ToString("G").HumanizeFromCamelCase();

        public static string HumanizeFromCamelCase(this string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return name;
            }

            StringBuilder sb = new StringBuilder();

            sb.Append(name[0]);
            for (int i = 1; i < name.Length; i++)
            {
                var chr = name[i];
                if (char.IsUpper(chr))
                {
                    sb.Append(' ');
                    sb.Append(chr);
                }
                else
                {
                    sb.Append(chr);
                }
            }

            return sb.ToString();
        }

        public static string GetValueIfNotEmpty<T>(this T val)
            where T : struct, IComparable
        {
            return default(T).CompareTo(val) == 0 ? null : val.ToString();
        }

        private static void GetAccessStringInternal(List<MemberInfo> parts1, List<string> parts2, Expression lambdaBody)
        {
            if (lambdaBody.NodeType == ExpressionType.Convert)
            {
                var member = ((UnaryExpression) lambdaBody).Operand as MemberExpression;

                Wrap(member, parts1, parts2);
                return;
            }

            if (lambdaBody.NodeType == ExpressionType.MemberAccess)
            {
                var member = lambdaBody as MemberExpression;

                Wrap(member, parts1, parts2);
                return;
            }

            if (lambdaBody.NodeType == ExpressionType.ArrayIndex)
            {
                var member = lambdaBody as BinaryExpression;
                parts1.Insert(0, null);
                var constant = member?.Right as ConstantExpression;

                parts2.Insert(0, $"[{constant?.Value}]");
                

                GetAccessStringInternal(parts1, parts2, member.Left);

                return;
            }

            if (lambdaBody.NodeType == ExpressionType.Call)
            {
                var member = lambdaBody as MethodCallExpression;
                parts1.Insert(0, null);
                var firstOrDefault = member?.Arguments?.FirstOrDefault();
                var constant = firstOrDefault as ConstantExpression;

                var parm = constant == null ? firstOrDefault?.ToString() : constant.Value?.ToString();

                if (member.Method.Name == "get_Item" && int.TryParse(parm, out _))
                {
                    parts2.Insert(0, $"[{parm}]");
                }
                else
                {
                    parts2.Insert(0, parm);
                }

                if (member != null)
                {
                    GetAccessStringInternal(parts1, parts2, member.Object);
                }

                // 
                //
                // var prop = member.Object as Mem;
                // var item = member.Arguments.FirstOrDefault();
                //
                // // Wrap(member.Object, parts);
                // Console.WriteLine("Here...");
                // // Wrap(member, parts);
                return;
            }

            throw new ArgumentException("Unknown Type: " + lambdaBody.NodeType, nameof(lambdaBody.NodeType));
        }

        private static void Wrap(MemberExpression member, List<MemberInfo> parts1, List<string> parts2)
        {
            parts1.Insert(0, member.Member);
            parts2.Insert(0, member.Member.Name);

            var expr1 = member?.Expression;
            // if (expr1.NodeType == ExpressionType.Parameter)
            // {
            //     return;
            // }

            if (expr1.NodeType == ExpressionType.MemberAccess)
            {
                GetAccessStringInternal(parts1, parts2, expr1);
            }

            if (expr1.NodeType == ExpressionType.Convert)
            {
                GetAccessStringInternal(parts1, parts2, expr1);
            }

            if (expr1.NodeType == ExpressionType.Call)
            {
                GetAccessStringInternal(parts1, parts2, expr1);
            }

            if (expr1.NodeType == ExpressionType.ArrayIndex)
            {
                GetAccessStringInternal(parts1, parts2, expr1);
            }
        }
    }
}

// private static MemberExpression GetMemberExpression(Expression method)
// {
//     LambdaExpression lambda = method as LambdaExpression;
//     if (lambda == null)
//     {
//         throw new ArgumentNullException("method");
//     }
//
//     if (lambda.Body.NodeType == ExpressionType.Convert)
//     {
//         return ((UnaryExpression) lambda.Body).Operand as MemberExpression;
//     }
//
//     if (lambda.Body.NodeType == ExpressionType.MemberAccess)
//     {
//         var memberExpression = lambda.Body as MemberExpression;
//         var expr1 = memberExpression?.Expression;
//         // throw new NotImplementedException(expr1.NodeType + expr1.GetType().ToString());
//         if (expr1.NodeType == ExpressionType.Parameter)
//         {
//             return memberExpression;
//         }
//
//         return null;
//     }
//
//     throw new ArgumentException("method");
// }