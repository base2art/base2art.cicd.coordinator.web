namespace Base2art.Web.Razor.Forms.Generation
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Text.RegularExpressions;
    using FormInputs;
    using Pages;
    using PropertyBuilders;
    using ViewModels.FormInputs;

    public class TextInputBuilder<T> : BasicControlBuilderBase<T, TextInputModel>
    {
        private readonly ForBuilder<TextInputBuilder<T>, T> fors;

        public TextInputBuilder(T value) : base(value)
            => this.fors = new ForBuilder<TextInputBuilder<T>, T>(this, this.Model);

        public TextInputBuilder<T, string> For(Expression<Func<T, string>> lookup)
            => this.Wrap(this.fors.For(lookup));

        public TextInputBuilder<T, string[]> For(Expression<Func<T, string[]>> lookup)
            => this.Wrap(this.fors.For(lookup));

        public TextInputBuilder<T, byte[]> For(Expression<Func<T, byte[]>> lookup)
            => this.Wrap(this.fors.For(lookup));

        public TextInputBuilder<T, Uri> For(Expression<Func<T, Uri>> lookup)
            => this.Wrap(this.fors.For(lookup));
        
        public TextInputBuilder<T, DateTime?> ForDate(Expression<Func<T, DateTime?>> lookup)
            => this.Wrap(this.fors.ForDate(lookup));
        
        public TextInputBuilder<T, DateTimeOffset?> ForLocalizedDateTime(Expression<Func<T, DateTimeOffset?>> lookup)
            => this.Wrap(this.fors.ForLocalizedDateTime(lookup));
        
        public TextInputBuilder<T, TimeSpan?> ForTime(Expression<Func<T, TimeSpan?>> lookup)
            => this.Wrap(this.fors.ForTime(lookup));

        public TextInputBuilder<T, Guid?> For(Expression<Func<T, Guid?>> lookup)
            => this.Wrap(this.fors.For(lookup));

        public TextInputBuilder<T, TEnum> For<TEnum>(Expression<Func<T, TEnum>> lookup)
            where TEnum : Enum => this.Wrap(this.fors.For(lookup));

        public TextInputBuilder<T, IDictionary<string, string>> ForJson(Expression<Func<T, IDictionary<string, string>>> lookup)
            => this.Wrap(this.fors.ForJson(lookup));

        private TextInputBuilder<T, TOther> Wrap<TOther>((TextInputBuilder<T>, Func<T, TOther>, Func<TOther, string>) @for)
        {
            var builder = new TextInputBuilder<T, TOther>(
                                                          new TextInputModel {Type = this.Model.Type},
                                                          this.Value,
                                                          this.fors.Expression,
                                                          @for.Item2,
                                                          null,
                                                          @for.Item3);

            return builder;
        }
    }

    public class TextInputBuilder<T, TProp> : HtmlControlBuilderBase<T, TextInputModel>
    {
        private readonly LabelBuilder<TextInputBuilder<T, TProp>, T> label;
        private readonly ValueBuilder<TextInputBuilder<T, TProp>, T, TProp> value;
        private readonly ReadOnlyBuilder<TextInputBuilder<T, TProp>> readOnly;
        private readonly RequiredBuilder<TextInputBuilder<T, TProp>> required;
        private readonly TextAreaRowsBuilder<TextInputBuilder<T, TProp>> rows;
        private readonly PlaceholderBuilder<TextInputBuilder<T, TProp>> placeholder;
        private readonly PatternBuilder<TextInputBuilder<T, TProp>> pattern;
        private readonly NameAndIdBuilder<TextInputBuilder<T, TProp>, T> name;

        public TextInputBuilder(
            TextInputModel model, 
            T value,
            Expression expression, 
            Func<T, TProp> lookup,
            Func<T, TProp[]> lookups,
            Func<TProp, string> stringifier) 
            : base(model, value)
        {
            this.value = new ValueBuilder<TextInputBuilder<T, TProp>, T, TProp>(this, this.Model, lookup, lookups, stringifier);
            this.label = new LabelBuilder<TextInputBuilder<T, TProp>, T>(this, this.Model, expression);
            this.name = new NameAndIdBuilder<TextInputBuilder<T, TProp>, T>(this, this.Model, expression);

            this.readOnly = new ReadOnlyBuilder<TextInputBuilder<T, TProp>>(this, this.Model);
            this.required = new RequiredBuilder<TextInputBuilder<T, TProp>>(this, this.Model);
            this.rows = new TextAreaRowsBuilder<TextInputBuilder<T, TProp>>(this, this.Model);
            this.placeholder = new PlaceholderBuilder<TextInputBuilder<T, TProp>>(this, this.Model);
            this.pattern = new PatternBuilder<TextInputBuilder<T, TProp>>(this, this.Model);
        }

        public TextInputBuilder<T, TProp> WithValue(TProp data) => this.value.WithValue(data);

        public TextInputBuilder<T, TProp> WithDefaultValue(TProp data) => this.value.WithDefaultValue(data);

        public TextInputBuilder<T, TProp> ReadOnly(bool isReadonly = true) => this.readOnly.Readonly(isReadonly);

        public TextInputBuilder<T, TProp> WithLabel(string labelValue) => this.label.WithLabel(labelValue);

        public TextInputBuilder<T, TProp> Required(bool isRequired = true) => this.required.Required(isRequired);

        public TextInputBuilder<T, TProp> WithPattern(Regex patternRegex) => this.pattern.WithPattern(patternRegex);

        // public TextInputBuilder<T, TProp> Regex(bool isRequired = true) => this.required.Required(isRequired);

        public TextInputBuilder<T, TProp> MultiLine(int rowCount = 2) => this.rows.MultiLine(rowCount);

        public TextInputBuilder<T, TProp> WithPlaceholder(string placeholderValue) => this.placeholder.WithPlaceholder(placeholderValue);

        protected override WebPage BuildControl() =>
            WebPage.Create(new TextInput(), this.Model);
    }
}