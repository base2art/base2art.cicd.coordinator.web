namespace Base2art.Web.Razor.Forms.Generation
{
    using FormInputs;
    using Pages;
    using ViewModels.FormInputs;

    public class JsonVariableBuilder<T> : ControlBuilderBase<T, JsonVariableModel>
    {
        public JsonVariableBuilder(T value) : base(value) => this.Model.Value = value;

        public JsonVariableBuilder<T> WithId(string name)
        {
            this.Model.Name = name;
            return this;
        }

        public JsonVariableBuilder<T> WithMainName(string name)
        {
            this.Model.MainName = name;
            return this;
        }

        public JsonVariableBuilder<T> WithModule(string name)
        {
            this.Model.ModuleName = name;
            return this;
        }

        protected override WebPage BuildControl() => WebPage.Create(new JsonVariable(), this.Model);
    }
}