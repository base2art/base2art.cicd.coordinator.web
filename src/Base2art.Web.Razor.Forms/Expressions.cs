namespace Base2art.Web.Razor.Forms
{
    using System.ComponentModel;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using Generation;

    public static class Expressions
    {
        public static string ExpressionName(this Expression func)
        {
            var memberInfo = func.GetMemberInfo();
            var displayNameAttr = memberInfo?.GetCustomAttributes<DisplayNameAttribute>()?.ToArray() ?? new DisplayNameAttribute[0];
            return displayNameAttr.FirstOrDefault()?.DisplayName
                   ?? memberInfo?.Name?.HumanizeFromCamelCase()
                   ?? string.Empty;
        }
    }
}