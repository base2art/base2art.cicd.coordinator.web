namespace Base2art.Web.Razor.Forms.Comparison
{
    using System;
    using System.Collections.Generic;

    public static class StandardComparers
    {
        public static IComparer<string> StringComparer { get; } = System.StringComparer.Ordinal;
        public static IComparer<long> Int64Comparer { get; } = new CustomComparer<long>((x, y) => x.CompareTo(y));

        public static IComparer<int> Int32Comparer { get; } = new CustomComparer<int>((x, y) => x.CompareTo(y));
        public static IComparer<short> Int16Comparer { get; } = new CustomComparer<short>((x, y) => x.CompareTo(y));
        public static IComparer<byte> ByteComparer { get; } = new CustomComparer<byte>((x, y) => x.CompareTo(y));

        public static IComparer<Guid> GuidComparer { get; } = new CustomComparer<Guid>((x, y) => x.CompareTo(y));

        public static IComparer<byte[]> ByteArrayComparer { get; } = new CustomComparer<byte[]>(CompareByteArray);

        public static IComparer<Uri> UriComparer { get; }
            = new CustomComparer<Uri>((x, y) => StringComparer.Compare(x?.ToString(), y?.ToString()));

        public static IComparer<Guid?> NullableGuidComparer { get; } =
            new CustomComparer<Guid?>((x, y) => x.GetValueOrDefault().CompareTo(y.GetValueOrDefault()));

        public static IComparer<TEnum> EnumComparer<TEnum>() where TEnum : Enum
        {
            var underlyingType = Enum.GetUnderlyingType(typeof(TEnum));
            return new CustomComparer<TEnum>((x, y) => MapValue(underlyingType, x).CompareTo(MapValue(underlyingType, y)));
        }

        private static string MapValue<TEnum>(Type underlyingType, TEnum @enum)
            // where TEnum : struct
        {
            var val = Convert.ChangeType(@enum, underlyingType);
            return val.ToString();
        }

        private static int CompareByteArray(byte[] arg1, byte[] arg2)
        {
            arg1 = arg1 ?? new byte[0];
            arg2 = arg2 ?? new byte[0];
            return String.Compare(Convert.ToBase64String(arg1), Convert.ToBase64String(arg2), StringComparison.Ordinal);
        }

        public class CustomComparer<T> : IComparer<T>
        {
            private readonly Func<T, T, int> func;

            public CustomComparer(Func<T, T, int> func) => this.func = func;

            public int Compare(T x, T y) => this.func(x, y);
        }
        
        public class EqualityComparer<T> : IEqualityComparer<T>
        {
            private readonly Func<T, T, int> func;
            private readonly Func<T, int> hashCode;

            public EqualityComparer(Func<T, T, int> func, Func<T, int> hashCode)
            {
                this.func = func;
                this.hashCode = hashCode;
            }

            public int Compare(T x, T y) => this.func(x, y);
            public bool Equals(T x, T y) => this.Compare(x, y) == 0;

            public int GetHashCode(T obj) => this.hashCode.Invoke(obj);
        }
    }

    public class Int32Comparer : IComparer<int>
    {
        public int Compare(int x, int y) => throw new System.NotImplementedException();
    }
}