namespace Base2art.Web.Razor.Forms.Controls
{
    using System;

    public interface IControlBuilder<TModel>
    {
        event EventHandler<DataBoundEventArgs<TModel>> PreBuild;  
    }
}