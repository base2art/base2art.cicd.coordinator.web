namespace Base2art.Web.Razor.Forms.Controls
{
    public class DataBoundEventArgs<T>
    {
        public T Model { get; }

        public DataBoundEventArgs(T model)
        {
            this.Model = model;
        }
    }
}