namespace Base2art.Web.Razor.Forms.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class Enums
    {
        public static TResult InvokeCatchNullRef<T, TResult>(this Func<T, TResult> method, T input)
        {
            try
            {
                if (method == null)
                {
                    return default;
                }

                var obj = method.Invoke(input);
                return obj;
            }
            catch (NullReferenceException)
            {
                return default;
            }
            catch (KeyNotFoundException)
            {
                return default;
            }
        }

        public static KeyValuePair<string, string>[] GetOptions<TEnum>()
            where TEnum : Enum
        {
            var underlyingType = Enum.GetUnderlyingType(typeof(TEnum));
            var options = Enum.GetValues(typeof(TEnum))
                              .OfType<TEnum>()
                              .Select(x => new KeyValuePair<string, string>(MapValue(underlyingType, x), x.ToString("G")))
                              .ToArray();

            return options;
        }

        public static KeyValuePair<string, TEnum>[] GetOptionValues<TEnum>()
            where TEnum : Enum => GetOptionValuesUnsafe<TEnum>();

        public static KeyValuePair<string, TEnum>[] GetOptionValuesUnsafe<TEnum>()
            // where TEnum : struct
        {
            var underlyingType = Enum.GetUnderlyingType(typeof(TEnum));
            var options = Enum.GetValues(typeof(TEnum))
                              .OfType<TEnum>()
                              .Select(x => new KeyValuePair<string, TEnum>(MapValue(underlyingType, x), x))
                              .ToArray();

            return options;
        }

        private static string MapValue<TEnum>(Type underlyingType, TEnum @enum)
            // where TEnum : struct
        {
            var val = Convert.ChangeType(@enum, underlyingType);
            return val.ToString();
        }
    }
}