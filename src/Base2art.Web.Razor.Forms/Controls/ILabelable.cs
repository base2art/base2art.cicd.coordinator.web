namespace Base2art.Web.Razor.Forms.Controls
{
    using System;
    using System.Linq.Expressions;

    public interface IValuedBuilder<out TReturn, TInput>
    {
        TReturn For(Expression<Func<TInput, string>> lookup);

        TReturn For(Expression<Func<TInput, byte[]>> lookup);

        TReturn For(Expression<Func<TInput, Uri>> lookup);

        TReturn For(Expression<Func<TInput, Guid?>> lookup);

        TReturn For<TEnum>(Expression<Func<TInput, TEnum>> lookup)
            where TEnum : Enum;

        TReturn WithValue(string data);
    }
}