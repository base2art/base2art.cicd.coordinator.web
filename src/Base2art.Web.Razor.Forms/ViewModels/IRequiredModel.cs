namespace Base2art.Web.Razor.Forms.ViewModels
{
    public interface IRequiredModel
    {
        bool Required { get; set; }
    }
}