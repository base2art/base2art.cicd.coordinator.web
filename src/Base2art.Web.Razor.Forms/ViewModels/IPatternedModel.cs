namespace Base2art.Web.Razor.Forms.ViewModels
{
    using System.Text.RegularExpressions;

    public interface IPatternedModel
    {
        Regex Pattern { get; set; }
    }
}