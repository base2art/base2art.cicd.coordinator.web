namespace Base2art.Web.Razor.Forms.ViewModels.FormInputs
{
    using System.Collections.Generic;

    public class ChooserInputModelBase : ILabelModel, IReadOnlyModel, IRequiredModel, IOptionedModel, INamedAndIdedModel
    {
        public string Label { get; set; }

        // public string Value { get; set; }
        public string Type { get; set; }

        public bool IsReadOnly { get; set; }

        public bool Required { get; set; }

        public KeyValuePair<string, string>[] Options { get; set; } = new KeyValuePair<string, string>[0];
        public int Minimum { get; set; }
        public int Maximum { get; set; }

        public string InputName { get; set; }
        public string Id { get; set; }
    }

    public class ChooserInputModel : ChooserInputModelBase, IValueModel
    {
        public string Value { get; set; }
    }

    public class MultiChooserInputModel : ChooserInputModelBase, IValuesModel
    {
        public string[] Values { get; set; }
    }
}