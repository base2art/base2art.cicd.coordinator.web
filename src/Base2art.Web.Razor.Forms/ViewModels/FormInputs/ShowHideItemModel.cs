namespace Base2art.Web.Razor.Forms.ViewModels.FormInputs
{
    public class ShowHideItemModel
    {
        public string Value { get; set; }
        public string Control { get; set; }
        public bool Show { get; set; }
    }
}