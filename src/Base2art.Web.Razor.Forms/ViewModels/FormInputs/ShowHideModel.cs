namespace Base2art.Web.Razor.Forms.ViewModels.FormInputs
{
    public class ShowHideModel
        : IValueModel, INamedAndIdedModel
    {
        public string Type { get; set; }
        public string Value { get; set; }

        public string InputName { get; set; }
        public string Id { get; set; }

        public ShowHideItemModel[] ShowHides { get; set; } = new ShowHideItemModel[0];
    }
}