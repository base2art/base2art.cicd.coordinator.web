namespace Base2art.Web.Razor.Forms.ViewModels.FormInputs
{
    public class ValidationSummaryModel
    {
        public bool IsValid { get; set; }
        public (string Code, string Message, string Path)[] Errors { get; set; }
    }
}