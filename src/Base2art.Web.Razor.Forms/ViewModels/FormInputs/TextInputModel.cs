namespace Base2art.Web.Razor.Forms.ViewModels.FormInputs
{
    using System.Text.RegularExpressions;

    public class TextInputModel : IValueModel, 
                                  ILabelModel,
                                  IReadOnlyModel,
                                  IRequiredModel,
                                  INamedAndIdedModel,
                                  IRowsModel,
                                  IPlaceholderModel,
                                  IPatternedModel
    {
        public string Label { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public bool IsReadOnly { get; set; }
        public bool Required { get; set; }

        public string Id { get; set; }
        public string InputName { get; set; }

        public int RowCount { get; set; } = 1;
        
        public string Placeholder { get; set; }
        public Regex Pattern { get; set; }
    }
}