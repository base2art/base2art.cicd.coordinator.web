namespace Base2art.Web.Razor.Forms.ViewModels.FormInputs
{
    using System.Collections.Generic;

    public class SubmitModel : IValueModel, ILabelModel, INamedAndIdedModel
    {
        public string Label { get; set; }
        // public string PrimaryKey { get; set; }
        // public string InputName { get; set; }

        public string Type { get; set; }
        public string Value { get; set; }

        public string InputName { get; set; }
        public string Id { get; set; }
        public (string value, string control, bool show)[] ShowHides { get; set; } = new (string, string, bool)[0];
    }
}