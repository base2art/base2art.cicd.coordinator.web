namespace Base2art.Web.Razor.Forms.ViewModels.FormInputs
{
    public class JsonVariableModel
    {
        public object Value { get; set; }
        public string Name { get; set; }
        public string ModuleName { get; set; }
        public string MainName { get; set; }
    }
}