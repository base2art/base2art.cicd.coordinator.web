namespace Base2art.Web.Razor.Forms.ViewModels
{
    using System.Collections.Generic;

    public interface IOptionedModel
    {
        KeyValuePair<string, string>[] Options { get; set; }
        int Minimum { get; set; }
        int Maximum { get; set; }
    }
}