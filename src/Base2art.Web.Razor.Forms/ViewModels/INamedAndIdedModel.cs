namespace Base2art.Web.Razor.Forms.ViewModels
{
    public interface INamedAndIdedModel : INamedModel, IIdedModel
    {
    }
}