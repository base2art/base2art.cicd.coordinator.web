namespace Base2art.Web.Razor.Forms.ViewModels.Specialize
{
    using System.ComponentModel;
    using Base2art.Razor.Api;

    public class HackComponent : Component
    {
        private readonly TemplateRoot helper;
        private readonly string content;
        private readonly string notes;

        public HackComponent(TemplateRoot helper, string intro, string outro, string notes)
        {
            this.helper = helper;
            this.content = outro;
            this.notes = notes;

            this.helper.WriteLiteral(intro.Trim());
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                this.helper.WriteLiteral($"<!-- {this.notes} -->");
                this.helper.WriteLiteral(this.content.Trim());
            }
        }
    }
}