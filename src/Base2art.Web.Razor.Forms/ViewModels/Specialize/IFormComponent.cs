namespace Base2art.Web.Razor.Forms.ViewModels.Specialize
{
    using System;
    using Generation;

    public interface IFormComponent : IDisposable
    {
        string Id { get; }
        
        ChooserInputBuilder<T> ChooserInput<T>(T modelData);
        TextInputBuilder<T> TextInput<T>(T modelData);
        SubmitBuilder<T> Submit<T>(T modelData);
        ValidationSummaryBuilder<T> ValidationSummary<T>(T modelData);
        ShowHideBuilder<T> ShowHide<T>(T modelData);
        JsonVariableBuilder<T> JsonVariable<T>(T value);
    }
}