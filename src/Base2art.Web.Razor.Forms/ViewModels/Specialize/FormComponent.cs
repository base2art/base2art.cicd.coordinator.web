namespace Base2art.Web.Razor.Forms.ViewModels.Specialize
{
    using System.ComponentModel;
    using Base2art.Razor.Api;
    using Generation;

    public class FormComponent : Component, IFormComponent
    {
        private readonly TemplateRoot helper;
        private readonly string content;
        private readonly string notes;

        public FormComponent(string id, TemplateRoot helper, string intro, string outro, string notes)
        {
            this.Id = id;
            this.helper = helper;
            this.content = outro;
            this.notes = notes;

            this.helper.WriteLiteral(intro.Trim());
        }

        public string Id { get; }

        public ChooserInputBuilder<T> ChooserInput<T>(T modelData)
            => new ChooserInputBuilder<T>(modelData);

        public TextInputBuilder<T> TextInput<T>(T modelData)
            => new TextInputBuilder<T>(modelData);

        public SubmitBuilder<T> Submit<T>(T modelData)
            => new SubmitBuilder<T>(modelData);

        public ValidationSummaryBuilder<T> ValidationSummary<T>(T modelData)
            => new ValidationSummaryBuilder<T>(modelData);

        public ShowHideBuilder<T> ShowHide<T>(T modelData)
            => new ShowHideBuilder<T>(modelData);

        public JsonVariableBuilder<T> JsonVariable<T>(T value)
            => new JsonVariableBuilder<T>(value);

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                this.helper.WriteLiteral($"<!-- {this.notes} -->");
                this.helper.WriteLiteral(this.content.Trim());
            }
        }
    }
}