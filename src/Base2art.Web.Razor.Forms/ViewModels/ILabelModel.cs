namespace Base2art.Web.Razor.Forms.ViewModels
{
    public interface ILabelModel
    {
        string Label { get; set; }
    }
}