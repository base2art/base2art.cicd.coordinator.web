namespace Base2art.Web.Razor.Forms.ViewModels
{
    public interface IRowsModel
    {
        int RowCount { get; set; }
    }
}