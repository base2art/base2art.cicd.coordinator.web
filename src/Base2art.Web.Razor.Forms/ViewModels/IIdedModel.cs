namespace Base2art.Web.Razor.Forms.ViewModels
{
    public interface IIdedModel
    {
        string Id { get; set; }
    }
}