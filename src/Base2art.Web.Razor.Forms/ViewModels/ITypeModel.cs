namespace Base2art.Web.Razor.Forms.ViewModels
{
    public interface ITypeModel
    {
        string Type { get; set; }
    }
}