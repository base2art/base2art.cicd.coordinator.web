namespace Base2art.Web.Razor.Forms.ViewModels
{
    public interface IValuesModel
    {
        string[] Values { get; set; }
    }
}