namespace Base2art.Web.Razor.Forms.ViewModels
{
    public interface IReadOnlyModel
    {
        bool IsReadOnly { get; set; }
    }
}