namespace Base2art.Web.Razor.Forms.ViewModels
{
    public interface IValueModel
    {
        string Value { get; set; }
    }
}