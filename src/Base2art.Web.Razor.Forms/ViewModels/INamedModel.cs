namespace Base2art.Web.Razor.Forms.ViewModels
{
    public interface INamedModel
    {
        string InputName { get; set; }
    }
}