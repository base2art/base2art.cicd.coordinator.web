namespace Base2art.Web.Razor.Forms.ViewModels
{
    public interface IPlaceholderModel
    {
        string Placeholder { get; set; }
    }
}