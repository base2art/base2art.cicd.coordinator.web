﻿namespace Base2art.CiCd.Coordinator.Benchmarker
{
    using BenchmarkDotNet.Configs;
    using BenchmarkDotNet.Running;

    class Program
    {
        static void Main(string[] args)
        {
            // BenchmarkRunner.Run<SerDeser>(new DebugBuildConfig());
            BenchmarkRunner.Run<SerDeser2>(new DebugBuildConfig());
            // Console.WriteLine("Hello World!");
        }
    }
}