namespace Base2art.CiCd.Coordinator.Benchmarker
{
    using System;
    using System.Collections.Generic;
    using BenchmarkDotNet.Attributes;
    
    [MemoryDiagnoser()]
    public class SerDeser2
    {
        [Benchmark]
        public void DoIt()
        {
            var service = new DebuggingSerializationService2();
            Dictionary<Guid, byte[]> contentLookup = new Dictionary<Guid, byte[]>();

            var bytes = this.GetByteArray(20_000);

            contentLookup[Guid.NewGuid()] = bytes;

            var content = service.Serialize(contentLookup);
            var contentObj = service.Deserialize<Dictionary<Guid, byte[]>>(content);
            // contentObj.Should().NotBeNull();
        }

        private byte[] GetByteArray(int sizeInKb)
        {
            Random rnd = new Random();
            byte[] b = new byte[sizeInKb * 1024]; // convert kb to byte
            rnd.NextBytes(b);
            return b;
        }
    }
}