namespace Base2art.CiCd.Coordinator.Benchmarker
{
    using System.Text.Json;

    public class DebuggingSerializationService2
    {
        // private readonly System.Text.Json.Serialization. serializer;

        public DebuggingSerializationService2()
        {
            
            // Console.WriteLine(JsonSerializer.Serialize(person));
            // Console.WriteLine(JsonSerializer.Serialize<Person>(person));
            // this.serializer = new CamelCasingSimpleJsonSerializer();
        }

        public string Serialize<T>(T value) => JsonSerializer.Serialize(value);

        public T Deserialize<T>(string value) => JsonSerializer.Deserialize<T>(value);
    }
}