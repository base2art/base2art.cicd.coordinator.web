using System;
using Xunit;

namespace Base2art.CiCd.Coordinator.Features
{
    using System.Linq;
    using Web.Collections;
    using FluentAssertions;

    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var items = new string[20].TakeAbout(10);
            items.Count().Should().Be(10);
            
            items = new string[14].TakeAbout(10);
            items.Count().Should().Be(14);
            
            items = new string[15].TakeAbout(10);
            items.Count().Should().Be(15);
            
            items = new string[16].TakeAbout(10);
            items.Count().Should().Be(10);
        }
    }
}