namespace Base2art.Web.Razor.Forms.Features.FormInputs
{
    using System;
    using System.Text.RegularExpressions;
    using FluentAssertions;
    using Pages;
    using Xunit;
    using Xunit.Abstractions;

    public class ChooserInputFeature
    {
        private readonly ITestOutputHelper testOutputHelper;

        public ChooserInputFeature(ITestOutputHelper testOutputHelper)
        {
            this.testOutputHelper = testOutputHelper;
        }

        [Fact]
        public async void LoadInputBool()
        {
            var person = new Person {Name = "SjY", IsAwesome = true};

            IWebPage holder = new HackTemplateRoot(async x =>
            {
                using (var form = x.Form())
                {
                    x.WriteLiteral(await form.ChooserInput(person)
                                             .For(y => y.IsAwesome)
                                             .WithLabel("\"Hello!\"")
                                             .Build());
                }
            });

            var content = (await holder.ExecuteAsync());
            var cleanContent = this.Clean(content);

            cleanContent.Should().Contain("&quot;Hello!&quot;");
            cleanContent.Should().Contain(@"<label class=""form-check-label"" for=""field_0"">Is Awesome</label>");
            cleanContent.Should().Contain(@"<input class=""form-check-input""
                   style=""min-width: auto;""
                   type=""checkbox""
                   value=""True""
                   checked=""checked""
                   name=""IsAwesome""
                   id=""field_0""");

            testOutputHelper.WriteLine(cleanContent);
        }
        
        private string Clean(string executeAsync)
        {
            executeAsync = executeAsync.Substring(84);
            executeAsync = executeAsync.Substring(0, executeAsync.Length - 38);

            executeAsync = Regex.Replace(executeAsync, @"field[a-f0-9]*_", "field_");
            executeAsync = Regex.Replace(executeAsync, @"'form[a-f0-9]{32}'", "'form'");
            return executeAsync;
        }
    }
}