namespace Base2art.Web.Razor.Forms.Features.FormInputs
{
    using System;
    using Base2art.Razor.Api;
    using Base2art.Razor.Api.Text;
    using ViewModels.Specialize;

    public static class Controls
    {
// #if DEBUG
//         public static IDisposable Card(this IRazorPage helper) => null;
//         public static IFormComponent Form(this IRazorPage helper) => null;
//         public static IDisposable Fieldset(this IRazorPage helper, string text) => null;
//         public static IDisposable Header(this IRazorPage helper) => null;
//         public static IEncodedString ListPageHeader(this IRazorPage helper, string title, string urlPath = null) => null;
// #endif

        public static IEncodedString ListPageHeader(this TemplateRoot helper, string title, string urlPath = null)
        {
            if (string.IsNullOrWhiteSpace(urlPath))
            {
                var intro1 = $@"
<div style='display: grid;grid-template-columns:  auto 5em;'>
    <h3 style='grid-column: 1;'>{Clean(title)}</h3>
</div>
";
                return new RawStringFactory.RawString(intro1);
            }

            var intro = $@"
<div style='display: grid;grid-template-columns:  auto 8em;'>
    <h3 style='grid-column: 1;'>{Clean(title)}</h3>
    <h3 style='grid-column: 2;text-align: right;'>
        <a href='{Uri.EscapeUriString(urlPath)}'>Add <i class='fas fa-plus-circle'></i></a>
    </h3>
</div>
";
            return new RawStringFactory.RawString(intro);
        }

        public static IDisposable Card(this TemplateRoot helper)
        {
            var intro = @"
<div class='card full-card'>
    <div class='card-body'>
        <div class='card-container' style='display: block'>
            <div class='full-content'>";
            var outro = @"
            </div>
        </div>
    </div>
</div>
";

            return new HackComponent(helper, intro, outro, nameof(Card));
        }

        public static IFormComponent Form(this TemplateRoot helper)
        {
            string id = $"form{Guid.NewGuid():N}";
            var intro = $@"
<form id='{id}' method='post'>
  <!-- form-start -->";
            var outro = @"
  <!-- form-end -->
</form>
";
            return new FormComponent(id, helper, intro, outro, nameof(Form));
        }

        public static IDisposable Fieldset(this TemplateRoot helper, string text)
        {
            var intro = @"<fieldset>";
            helper.WriteLiteral(intro.Trim());
            helper.WriteLiteral("<legend>");
            helper.Write(text);
            helper.WriteLiteral("</legend>");

            var outro = @"</fieldset>";
            return new HackComponent(helper, "", outro, nameof(Form));
        }

//         public static PagePanelHolder PagePanel(this TemplateRoot helper)
//         {
//             var intro = @"
// <div style='display: grid; grid-template-rows: 4em auto;'>";
//             var outro = @"
// </div>
// ";
//             helper.WriteLiteral(intro.Trim());
//             return new PagePanelHolder(helper, outro);
//         }

        private static string Clean(string title)
            => title?.Replace("&", "&amp;")
                    ?.Replace("<", "&lt;")
                    ?.Replace(">", "&gt;")
                    ?.Replace("\"", "&quot;")
                    ?.Replace("'", "&apos;");
    }
}