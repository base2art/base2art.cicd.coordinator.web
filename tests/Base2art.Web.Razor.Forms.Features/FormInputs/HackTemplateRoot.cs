namespace Base2art.Web.Razor.Forms.Features.FormInputs
{
    using System;
    using System.Dynamic;
    using System.IO;
    using System.Threading.Tasks;
    using Base2art.Razor.Api;
    using Pages;

    public class HackTemplateRoot : TemplateRoot, IWebPage
    {
        private readonly Func<TemplateRoot, Task> item;

        public HackTemplateRoot(Func<TemplateRoot, Task> item)
        {
            this.item = item;
        }

        public override async global::System.Threading.Tasks.Task ExecuteAsync()
        {
            await this.item(this);
        }

        async Task<string> IWebPage.ExecuteAsync()
        {
            string str;
            await using (StringWriter stringWriter = new StringWriter())
            {
                ExecuteContext executeContext = new ExecuteContext();
                this.viewBag = new ExpandoObject();
                await this.Run(executeContext, stringWriter);
                await stringWriter.FlushAsync();
                str = stringWriter.GetStringBuilder().ToString();
            }

            return str;
        }
    }
}