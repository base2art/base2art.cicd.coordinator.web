namespace Base2art.Web.Razor.Forms.Features.FormInputs
{
    using System;
    using FluentAssertions;
    using Forms.FormInputs;
    using Pages;
    using Xunit;

    public class TextBoxInputFeature
    {
        [Fact]
        public async void LoadInputRequests()
        {
            var person = new Person {Name = "SjY"};

            IWebPage holder = new HackTemplateRoot(async x =>
            {
                using (var form = x.Form())
                {
                    x.WriteLiteral(await form.TextInput(person)
                                             .For(y => y.Name)
                                             .Build());
                }
            });

            var content = (await holder.ExecuteAsync());
            var cleanContent = this.Clean(content);
            cleanContent.Should().Contain(@"<input type='text'
               value=""SjY""
               name=""Name""");
            // var form = new 
        }

        [Fact]
        public async void LoadInputDateTime()
        {
            var person = new Person {Name = "SjY", BirthDate = new DateTime(1999, 07, 14)};

            IWebPage holder = new HackTemplateRoot(async x =>
            {
                using (var form = x.Form())
                {
                    x.WriteLiteral(await form.TextInput(person)
                                             .ForDate(y => y.BirthDate)
                                             .Build());
                }
            });

            var content = (await holder.ExecuteAsync());
            var cleanContent = this.Clean(content);
            cleanContent.Should().Contain(@"<input type='date'
               value=""1999-07-14""
               name=""BirthDate""");
            // var form = new 
        }

        [Fact]
        public async void LoadInputDateTimeLocal()
        {
            var person = new Person {Name = "SjY", DentistAppointment = new DateTimeOffset(1999, 07, 14, 13, 45, 21, TimeSpan.Zero)};

            IWebPage holder = new HackTemplateRoot(async x =>
            {
                using (var form = x.Form())
                {
                    x.WriteLiteral(await form.TextInput(person)
                                             .ForLocalizedDateTime(y => y.DentistAppointment)
                                             .Build());
                }
            });

            var content = (await holder.ExecuteAsync());
            var cleanContent = this.Clean(content);
            // yyyy-MM-ddThh:mm
            cleanContent.Should().Contain(@"<input type='datetime-local'
               value=""1999-07-14T13:45""
               name=""DentistAppointment""");
            // var form = new 
        }

        [Fact]
        public async void LoadInputDateTimeNull()
        {
            var person = new Person {Name = "SjY", BirthDate = null};

            IWebPage holder = new HackTemplateRoot(async x =>
            {
                using (var form = x.Form())
                {
                    x.WriteLiteral(await form.TextInput(person)
                                             .ForDate(y => y.BirthDate)
                                             .Build());
                }
            });

            var content = (await holder.ExecuteAsync());
            var cleanContent = this.Clean(content);
            cleanContent.Should().Contain(@"<input type='date'
               name=""BirthDate""");
            // var form = new 
        }

        [Fact]
        public async void LoadInputTimespan()
        {
            var ts = new TimeSpan(1, 23, 54, 12, 245);
            var person = new Person {Name = "SjY", LastLogin = ts};

            IWebPage holder = new HackTemplateRoot(async x =>
            {
                using (var form = x.Form())
                {
                    x.WriteLiteral(await form.TextInput(person)
                                             .ForTime(y => y.LastLogin)
                                             .Build());
                }
            });

            var content = await holder.ExecuteAsync();
            this.Clean(content).Should().Contain(@"<input type='time'
               value=""23:54:12""
               name=""LastLogin""");
            // var form = new 
        }

        private string Clean(string executeAsync)
        {
            executeAsync = executeAsync.Substring(84);
            return executeAsync.Substring(0, executeAsync.Length - 38);
        }
    }
}