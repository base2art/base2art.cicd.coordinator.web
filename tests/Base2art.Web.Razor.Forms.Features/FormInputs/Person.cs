namespace Base2art.Web.Razor.Forms.Features.FormInputs
{
    using System;

    public class Person
    {
        public string Name { get; set; }
        public DateTime? BirthDate { get; set; }
        public TimeSpan? LastLogin { get; set; }
        public DateTimeOffset DentistAppointment { get; set; }
        public bool IsAwesome { get; set; }
    }
}