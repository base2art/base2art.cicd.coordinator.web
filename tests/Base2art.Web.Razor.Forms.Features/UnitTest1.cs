namespace Base2art.Web.Razor.Forms.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using FluentAssertions;
    using Generation;
    using WebClient.NetHttp;
    using Xunit;

    public class Tests
    {
        [Fact]
        public void LoadNetRequests()
        {
            var @null = new Base2art.WebClient.NetRequests.SimpleWebClient();
            @null.DefaultTimeout = TimeSpan.FromDays(1);
            var @null1 = new Base2art.WebClient.NetHttp.SimpleHttpWebClient();
            @null1.DefaultTimeout = TimeSpan.FromDays(1);
        }
        [Fact]
        public void CommonPassing()
        {
            // SimpleHttpWebClient
            this.Expr<Holder>(x => x.Name).GetAccessString().Should().Be("Name");
            this.Expr<Holder>(x => x.Name).GetMemberInfo().Name.Should().Be("Name");
            this.Expr<Holder>(x => x.Versioning.Strategy).GetMemberInfo().Should().BeNull();
            this.Expr<Holder>(x => x.Versioning.Strategy).GetAccessString().Should().Be("Versioning.Strategy");
            this.Expr<Holder>(x => x.Versioning.Data["startingVersion"]).GetAccessString().Should().Be("Versioning.Data.startingVersion");
            this.Expr<Holder>(x => x.BuildInstructions[0].ExecutableFile).GetAccessString().Should().Be("BuildInstructions[0].ExecutableFile");
            // this.Expr<Holder>(x => x.Versioning.Strategy).GetMemberInfo().Name.Should().Be("Strategy");
        }

        [Fact]
        public void Test1()
        {
            // x.BuildInstructions[0].ExecutableFile
            this.Expr<Holder>(x => x.BuildInstructionArray[0].ExecutableFile).GetAccessString().Should().Be("BuildInstructionArray[0].ExecutableFile");
            //
            // var expr = this.Expr<Holder>(x => x.Versioning.Strategy);
            // this.Expr<Holder>(x => x.Versioning.Strategy).GetMemberInfo().Name.Should().Be("Strategy");

            // expr.GetMemberName();
            // expr.GetMemberName().Should().Be("Versioning.Strategy");
        }

        private Expression Expr<T>(Expression<Func<Holder, string>> func)
        {
            return func;
        }

        public class Holder
        {
            public StrategyHolder Versioning { get; set; }
            public string Name { get; set; }
            public List<BuildInstruction> BuildInstructions { get; set; }
            public BuildInstruction[] BuildInstructionArray { get; set; }
        }

        public class StrategyHolder
        {
            public string Strategy { get; set; }
            public Dictionary<string, string> Data { get; set; }
        }

        public class BuildInstruction
        {
            public string ExecutableFile { get; set; }
        }
    }
}