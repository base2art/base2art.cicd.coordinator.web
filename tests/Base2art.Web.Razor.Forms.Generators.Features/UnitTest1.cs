using System;
using Xunit;

namespace Base2art.Web.Razor.Forms.Generators.Features
{
    using FluentAssertions;
    using Generation;

    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            "HelloWorld".HumanizeFromCamelCase().Should().Be("Hello World");
        }
    }
}